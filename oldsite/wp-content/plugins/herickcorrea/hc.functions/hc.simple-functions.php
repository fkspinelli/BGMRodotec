<?php

//IDENTIFICA A PÁGINA

function urlPage(){
	$urlExp = explode('/',$_SERVER['REQUEST_URI']);
	$total = count($urlExp) - 2;
	$url = $urlExp[$total];
	return $url;
}

//IDENTIFICA A PÁGINA SE ELA FOR CUSTOM POST

function urlPage2(){
	$urlExp = explode('/',$_SERVER['REQUEST_URI']);
	$total = count($urlExp) - 3;
	$url = $urlExp[$total];
	return $url;
}

//ABREVIA TEXTO

function abreviarTexto($txt,$tam) {
	if (strlen($txt) > $tam) {
		$rtexto = substr($txt,0,$tam)."...";
		return $rtexto;
	}
	else {
		return $txt;
	}
	
}

function texto_limite($title,$maximo) {
	if ( strlen($title) > $maximo ) {
		$continua = '...';
	}
	$title = mb_substr( $title, 0, $maximo, 'UTF-8' );
	return $title.$continua;
}

//TRADUZ STRING WPML

function translation($id,$idioma){
	foreach (get_field('traducao', 'option') as $string){
		if($string['id'] == $id){
			echo $string[$idioma];
		}
	}
}

function get_translation($id,$idioma){
	foreach (get_field('traducao', 'option') as $string){
		if($string['id'] == $id){
			return $string[$idioma];
		}
	}
}


//MONTA DATA CERTA

function dataShort($data){

	$dataExp = explode('-',$data);
	
	$dia = $dataExp[2];
	$ano = $dataExp[0];
	
	switch($dataExp[1]){
		case "1": $mes = 'Jan'; break;
		case "2": $mes = 'Fev'; break;
		case "3": $mes = 'Mar'; break;
		case "4": $mes = 'Abr'; break;
		case "5": $mes = 'Mai'; break;
		case "6": $mes = 'Jun'; break;
		case "7": $mes = 'Jul'; break;
		case "8": $mes = 'Ago'; break;
		case "9": $mes = 'Set'; break;
		case "10": $mes = 'Out'; break;
		case "11": $mes = 'Nov'; break;
		case "12": $mes = 'Dez'; break;
	}
	
	return $dia.'.'.$mes.'.'.$ano;
	
}

//CRIA TIPOS DE POSTS PERSONALIZADOS

function custom_post_modulos() {
	$labels = array(
		'name'                => _x( 'Módulos', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Módulo', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Módulos', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Módulos:', 'text_domain' ),
		'all_items'           => __( 'Todos os Módulos', 'text_domain' ),
		'view_item'           => __( 'Ver Módulos', 'text_domain' ),
		'add_new_item'        => __( 'Adicionar Novo Módulo', 'text_domain' ),
		'add_new'             => __( 'Adicionar Novo', 'text_domain' ),
		'edit_item'           => __( 'Editar Módulo', 'text_domain' ),
		'update_item'         => __( 'Atualizar Módulo', 'text_domain' ),
		'search_items'        => __( 'Procurar Módulos', 'text_domain' ),
		'not_found'           => __( 'Módulos não encontrados', 'text_domain' ),
		'not_found_in_trash'  => __( 'Nenhum módulo enviada para lixeira', 'text_domain' ),
	);

	$rewrite = array(
		'slug'                => 'modulos',
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => true,
	);

	$args = array(
		'label'               => __( 'Módulos', 'text_domain' ),
		'description'         => __( 'Módulos do Software', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( ),
		'taxonomies'          => array( 'tipos' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 6,
		'menu_icon'           => '',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'post',
	);

	register_post_type( 'modulos', $args );
}

// Hook into the 'init' action
add_action( 'init', 'custom_post_modulos', 0 );

// ASSINATURA

function wp_signature(){
	echo '
		<div class="esconde">
			<p>Site desenvolvido pelo webdsigner <a href="http://www.herickcorrea.com.br" title="Web Designer Herick Correa, Criação de sites e Wordpress" target="_blank">Herick Correa</a></p>
		</div>
	';
}

?>