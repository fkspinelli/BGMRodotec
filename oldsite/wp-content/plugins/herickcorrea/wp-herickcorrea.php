<?php
/* 
Plugin Name: Funções Herick Correa
Plugin URI: http://www.sabecomofazer.com.br
Description: Plugins com funções criadas por Herick Correa para auxiliar em projetos. Mas para administrar as funções, é preciso mexer nelas manualmente, pois estes arquivos são somente um conjunto de funções. Conheça meus trabalhos em www.herickcorrea.com.br.
Version: 1.0
Author: Herick Correa
Author URI: http://www.herickcorrea.com.br
License: GLP

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

Contact Herick Correa at http://www.herickcorrea.com.br

*/

class hc{
	
	public function ativar(){
		add_option('hc', '');
	}
	
	public function desativar(){
		delete_option('hc');
	}
	
}

$pathPlugin = substr(strrchr(dirname(__FILE__),DIRECTORY_SEPARATOR),1).DIRECTORY_SEPARATOR.basename(__FILE__);

// Função ativar
register_activation_hook($pathPlugin, array('hc','ativar'));
 
// Função desativar
register_deactivation_hook($pathPlugin, array('hc','desativar'));
 

/* ---------------------------- CHAMA ARQUIVOS COM AS FUNÇÕES DESEJADAS ---------------------------- */

include('hc.functions/hc.simple-functions.php');

?>