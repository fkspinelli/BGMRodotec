<section id="inteligence">
	<div id="headerTxt">
		<div class="trava">
			<article>
				<header>
					<h1><?php the_field('titulo'); ?></h1>
					<h2><?php the_field('subtitulo'); ?></h2>
				</header>
				<?php the_field('texto'); ?>
			</article>
			<figure>
				<img src="<?php the_field('imagem_de_fundo'); ?>" alt="#">
			</figure>
			<div class="clear"></div>
		</div>
	</div>
	<div class="content trava">
		<article class="beneficios">
			<h1><?php the_field('titulo_complemento'); ?></h1>
			<?php the_field('texto_complemento'); ?>
		</article>
		<article class="contato">
			<h1>Tome uma decisão correta e leve o Globus Intelligence para a sua empresa.</h1>
			<p>Com ele, você tomará decisões mais rápidas e acertadas.</p>
			<a href="<?php echo esc_url( home_url( '/contato' ) ); ?>" title="Entre em contato">Entre em contato</a>
		</article>
		<div class="clear"></div>
	</div>
</section>