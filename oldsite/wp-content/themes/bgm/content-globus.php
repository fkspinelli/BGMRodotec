<section id="globus">
	<div id="headerTxt" data-tipo="1">
		<article>
			<header>
				<h1><?php the_field('titulo'); ?></h1>
				<h2><?php the_field('subtitulo'); ?></h2>
			</header>
			<?php the_field('texto'); ?>
		</article>
		<figure>
			<img src="<?php the_field('imagem_de_fundo'); ?>" alt="#">
		</figure>
	</div>
	<div class="bg">
		<div class="trava">
			<article class="missao">
				<h1>Sempre atenta às inovações, <br />a BgmRodotec investe constantemente <br />na evolução do software Globus <br />incorporando:</h1>
				<?php the_field('texto_de_missão'); ?>
				<ul>
                    <li>novos recursos tecnológicos;</li>
                    <li>experiência de especialistas do segmento;</li>
                    <li>demandas dos clientes a partir de uma ampla </li>
                    <li>comunidade de mais de 25 mil usuários;</li>
                    <li>boas práticas amplamente testadas pelo mercado.</li>
                </ul>
			</article>
			<article class="visao">
				<h1>Benefícios Globus:</h1>
				<?php the_field('texto_de_visao'); ?>
				<ul>
                    <li>Melhora dos processos e da gestão da empresa.</li>
                    <li>Redução dos custos operacionais.</li>
                    <li>Eliminação de erros na emissão de documentos.</li>
                    <li>Eliminação do retrabalho e aumento da produtividade.</li>
                    <li>Padronização da operação e dos indicadores de gestão.</li>
                    <li>Apoio para o crescimento da empresa.</li>
                    <li>Redução de riscos dos acionistas e dos administradores.</li>
                </ul>
			</article>
		</div>
	</div>
	<article class="suporte">
		<h1><?php the_field('titulo_suporte_tecnico'); ?></h1>
		<p>
			<?php the_field('texto_suporte_tecnico'); ?>
		</p>
	</article>
	<article class="suporte">
		<h1><?php the_field('titulo_treinamentos'); ?></h1>
		<p>
			<?php the_field('texto_treinamento'); ?>
		</p>
	</article>
	<article class="suporte noborder">
		<h1><?php the_field('titulo_globus_report'); ?></h1>
		<p>
			<?php the_field('texto_globus_report'); ?>
		</p>
	</article>
	<hr>
	<section id="spots">
		<div class="trava">
			<article class="passageiros">
				<div class="content">
					<header>
						<h1>conheça as soluções e os benefícios para</h1>
						<h2>passageiros</h2>
					</header>
                    <figure>
                    <a href="#" title="Ver mais" class="open">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/bg_passageiros_branco.png" alt="#">
                    </a>
                    </figure>
				</div>
				<aside>
					<a href="#" title="Ver mais" class="open">
						<img src="<?php echo get_template_directory_uri(); ?>/images/bot_seta_dir_branco_01.jpg" alt="open">
					</a>
					<a href="#" title="Ver mais" class="close">
						<img src="<?php echo get_template_directory_uri(); ?>/images/bot_seta_esq_02.jpg" alt="open">
					</a>
					<?php //wp_nav_menu( array( 'theme_location' => 'secundary', 'blc_passageiros' => 'nav-menu' ) ); ?>
                    <?php wp_nav_menu( array( 'theme_location' => 'secundary', 'menu' => 'Passageiros-home' ) ); ?>
				</aside>
			</article>
			<hr>
			<article class="logistica">
				<aside>
					<a href="#" title="Ver mais" class="open">
						<img src="<?php echo get_template_directory_uri(); ?>/images/bot_seta_esq_branco_01.jpg" alt="open">
					</a>
					<a href="#" title="Ver mais" class="close">
						<img src="<?php echo get_template_directory_uri(); ?>/images/bot_seta_dir_02.jpg" alt="open">
					</a>
					<?php //wp_nav_menu( array( 'theme_location' => 'third', 'blc_logistica' => 'nav-menu' ) ); ?>
                    <?php wp_nav_menu( array( 'theme_location' => 'secundary', 'menu' => 'logistica-home' ) ); ?>
				</aside>
				<div class="content">
					<header>
						<h1>conheça as soluções e os benefícios para</h1>
						<h2>log&iacute;stica</h2>
					</header>
					<figure>
                    <a href="#" title="Ver mais" class="open">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/bg_logistica_branco.png" alt="#">
                    </a>
                    </figure>
				</div>
			</article>
		</div>
	</section>
</section>