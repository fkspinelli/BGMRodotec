<section id="imprensa">
	<div id="headerTxt">
		<article class="accordeon">
			<header>
				<h1>BgmRodotec <span>imprensa</span></h1>
				<p>Bem-vindo à sala de imprensa da BgmRodotec. Baixe fotos, press release e outros arquivos da BgmRodotec.</p>
			</header>
			<div>
				<h2>ÚLTIMOS RELEASES</h2>
				<ul>
					<?php
						foreach(get_field('ultimos_releases') as $release){
					?>
					<li>
						<a href="<?php echo $release['pdf']; ?>" title="Abrir" class="open" target="_blank"><?php echo $release['titulo']; ?></a>
						<time datetime="2013-12-12">publicado em <?php echo dataShort($release['data']); ?></time>
					</li>
					<?php }	?>
				</ul>
		</article>
	</div>	
	<article class="galeria">
		<div>
			<h2>galeria de imagens</h2>
			<ul>
				<?php
					foreach(get_field('galeria_de_imagens') as $galeria){
				?>
				<li>
					<figure>
						<a href="<?php echo $galeria['arquivo_da_galeria']; ?>" title="Download" target="_blank"><img src="<?php echo $galeria['imagem']; ?>" alt="<?php echo $galeria['titulo_da_galeria']; ?>"></a>
						<figcaption>
							<p>
								<span><?php echo $galeria['titulo_da_galeria']; ?></span>
								<a href="<?php echo $galeria['arquivo_da_galeria']; ?>" title="Download" target="_blank">faça o download em alta resolução. <?php echo $galeria['tamanho_do_arquvio']; ?></a>
							</p>
						</figcaption>
					</figure>
				</li>
				<?php }	?>
			</ul>
		</div>
	</article>
</section>