<section id="webdoor" data-tipo="1">
	<h3 class="esconde">Destaques</h3>
	<ul>
		<?php
			foreach (get_field('webdoor') as $slideshow){
				$posts = $slideshow['link'];
				foreach( $posts as $post):
					setup_postdata($post);
		?>
		<li>
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
				<img src="<?php echo $slideshow['imagem']; ?>" alt="<?php echo $titulo; ?>">
			</a>
		</li>
		<?php
				endforeach;
					wp_reset_postdata();
			}
		?>
	</ul>
	<div class="trava">
		<div class="controles">
			<a href="#" title="Anterior" class="anterior"><span>Anterior</span></a>
			<div class="count">
				<span class="atual"></span>/<span class="total"></span>
			</div>
			<a href="#" title="Próximo" class="proximo"><span>Anterior</span></a>
		</div>
	</div>
</section>
<hr>
<section id="spots">
	<div class="trava">
		<article class="passageiros">
			<div class="content">
				<header>
					<h1>conheça as soluções e os benefícios para</h1>
					<h2>passageiros</h2>
				</header>
				<figure>
                <a href="#" title="Ver mais" class="open">
                	<img src="<?php echo get_template_directory_uri(); ?>/images/bg_passageiros.png" alt="#">
                </a>
                </figure>
			</div>
			<aside>
				<a href="#" title="Ver mais" class="open">
					<img src="<?php echo get_template_directory_uri(); ?>/images/bot_seta_dir_01.jpg" alt="open">
				</a>
				<a href="#" title="Fechar - bt_close" class="close">
					<img src="<?php echo get_template_directory_uri(); ?>/images/bot_seta_esq_02.jpg" alt="open">
				</a>
				<?php //wp_nav_menu( array( 'theme_location' => 'secundary', 'blc_passageiros' => 'nav-menu' ) ); ?>
                <?php wp_nav_menu( array( 'theme_location' => 'secundary', 'menu' => 'Passageiros-home' ) ); ?>
			</aside>
		</article>
		<hr>
		<article class="logistica">
			<aside>
				<a href="#" title="Ver mais - bt_open" class="open">
					<img src="<?php echo get_template_directory_uri(); ?>/images/bot_seta_esq_01.jpg" alt="open">
				</a>
				<a href="#" title="Ver mais - bt_close" class="close">
					<img src="<?php echo get_template_directory_uri(); ?>/images/bot_seta_dir_02.jpg" alt="open">
				</a>
				<?php //wp_nav_menu( array( 'theme_location' => 'third', 'blc_logistica' => 'nav-menu' ) ); ?>
                <?php wp_nav_menu( array( 'theme_location' => 'secundary', 'menu' => 'logistica-home' ) ); ?>
			</aside>
			<div class="content">
				<header>
					<h1>conheça as soluções e os benefícios para</h1>
					<h2>log&iacute;stica</h2>
				</header>
				<figure>
                <a href="#" title="Ver mais" class="open">
                	<img src="<?php echo get_template_directory_uri(); ?>/images/bg_logistica.png" alt="#">
                </a>
                </figure>
			</div>
		</article>
	</div>
</section>
<hr>
<section id="oportunidades">
	<div class="trava">
		<h1>descubra novas oportunidades</h1>
		<hr>
		<!-- <article class="globus">
			<header>
				<h1>SOFTWARE</h1>
				<h2>GLOBUS</h2>
			</header>
			<p>
				<?php the_field('descrição_software_globus', 'option'); ?>
			</p>
			<a href="<?php echo esc_url( home_url( '/globus' ) ); ?>" title="Clique aqui para saber mais">Clique aqui para saber mais</a>
		</article>
		<hr>
		<aside>
			<article class="inteligence">
				<header>
					<h1>SOFTWARE</h1>
					<h2>globus intelligence</h2>
				</header>
				<p>
					<?php the_field('short_descricao_globus_inteligence', 'option'); ?>
				</p>
				<a href="<?php echo esc_url( home_url( '/globus-inteligence' ) ); ?>" title="Conheça mais aqui">Conheça mais aqui</a>
			</article>
			<hr>
			<article class="inteligence">
				<header>
					<h1>BgmRodotec</h1>
					<h2>consultoria</h2>
				</header>
				<p>
					<?php the_field('descricao_curta_de_consultoria', 'option'); ?>
				</p>
				<a href="<?php echo esc_url( home_url( '/consultoria' ) ); ?>" title="Conheça mais aqui">Conheça mais aqui</a>
			</article>
		</aside>
		<div class="clear"></div> -->

		<div style="float: left; width: 475px; ">
			<article class="globus" style="float: none; padding: 0 0 30px 0;">
				<?php
				if(get_field('video_home')){
					echo convertYoutubeUrlToEmbed(get_field('video_home'), 490, 340);
				};
				?>
			</article>
		<hr>
			<article class="inteligence">
				<header>
					<h1>SOFTWARE</h1>
					<h2>globus intelligence</h2>
				</header>
				<p>
					<?php the_field('short_descricao_globus_inteligence', 'option'); ?>
				</p>
				<a href="<?php echo esc_url( home_url( '/globus-inteligence' ) ); ?>" title="Conheça mais aqui">Conheça mais aqui</a>
			</article>
		</div>
		<aside>
			<article class="globus" style="float: none; padding: 0 0 30px 0;">
				<header>
					<h1>SOFTWARE</h1>
					<h2>GLOBUS</h2>
				</header>
				<p>
					<?php the_field('descrição_software_globus', 'option'); ?>
				</p>
				<a href="<?php echo esc_url( home_url( '/globus' ) ); ?>" title="Clique aqui para saber mais">Clique aqui para saber mais</a>
			</article>
			<article class="consultoria">
				<header>
					<h1>BgmRodotec</h1>
					<h2>consultoria</h2>
				</header>
				<p>
					<?php the_field('descricao_curta_de_consultoria', 'option'); ?>
				</p>
				<a href="<?php echo esc_url( home_url( '/consultoria' ) ); ?>" title="Conheça mais aqui">Conheça mais aqui</a>
			</article>
		</aside>
		<div class="clear"></div>

	</div>
</section>
<hr>
<section id="contatos">
	<div class="trava">
		<h1 class="esconde">Contatos</h1>
		<div class="duvidas">
			<p>Tire suas dúvidas pelo telefone <span><?php the_field('telefone', 'option'); ?></span></p>
		</div>
		<div class="email">
			<p>Se preferir, envie email por aqui</p>
			<a href="<?php echo esc_url( home_url( '/contato' ) ); ?>" title="ENVIAR CONTATO">ENVIAR CONTATO</a>
		</div>
	</div>
</section>
<hr>
<section id="parts">
	<div class="trava">
		<article class="inteligence">
			<div>
				<h1>GLOBUS PARTS</h1>
				<p>
					<?php the_field('descricao_globus_parts', 'option'); ?>
				</p>
				<a href="<?php the_field('link_globus_parts', 'option'); ?>" title="Compre e Venda aqui" target="_blank">Compre e Venda aqui</a>
			</div>
			<figure><img src="<?php the_field('imagem_globus_parts', 'option'); ?>" alt="GLOBUS PARTS"></figure>
		</article>
		<hr>
		<div class="slide">
			<h1>Depoimentos</h1>
			<div class="scroll">
                <ul>
                    <?php
						$i = 1;
						$args = array(
							'p'      => 50,
							'post_type' => 'page',
						);
						query_posts( $args );
							while ( have_posts() ) : the_post();
								foreach(get_field('depoimentos') as $depoimento){
					?>
					<li>
                        <a href="#" title="<?php echo $depoimento['titulo']; ?>" data-id="<?php echo $i; ?>">
							<img src="<?php echo $depoimento['logo_da_empresa']; ?>" alt="<?php echo $depoimento['subtitulo']; ?>">
						</a>
                        <div class="esconde">
                            <p>
                                "<?php echo $depoimento['texto']; ?>"
                            </p>
                            <p>
                                <strong><?php echo $depoimento['titulo']; ?></strong>
                                <?php echo $depoimento['cargo_profissional']; ?>
                            </p>
                        </div>
                    </li>
					<?php
									$i++;
								}
							endwhile;
						wp_reset_query();
					?>
                </ul>
            </div>
			<div class="controles">
				<a href="#" title="Anterior" class="anterior"><span>Anterior</span></a>
				<a href="#" title="Próximo" class="proximo"><span>Próximo</span></a>
			</div>
			<div class="tooltip"><div></div></div>
			<div class="vermais">
				<a href="<?php echo esc_url( home_url( '/depoimentos' ) ); ?>" title="Ver todos os depoimentos">Ver todos os depoimentos</a>
			</div>
		</div>
	</div>
</section>