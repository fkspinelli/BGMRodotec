<?php $pagina = get_the_title(); ?>
<section id="solucoes">
	<div id="headerTxt">
		<div class="trava">
			<article>
				<header>
					<hgroup>
						<h1><?php the_field('categoria'); ?>/</h1>
						<h2><?php the_title(); ?></h2>
					</hgroup>
					<div class="select">
						<a href="javascript:void(0)" title="selecione outra categoria" class="open seta">selecione outra categoria</a>
						<?php
							if(get_field('categoria') == "Passageiros"){
						?>
							<nav>
								<?php wp_nav_menu( array( 'theme_location' => 'secundary', 'blc_passageiros' => 'nav-menu' ) ); ?>
							</nav>
						<?php } else { ?>
							<nav>
								<?php wp_nav_menu( array( 'theme_location' => 'third', 'blc_logistica' => 'nav-menu' ) ); ?>
							</nav>
						<?php } ?>
					</div>
				</header>
				<figure>
					<img src="<?php the_field('imagem'); ?>" alt="#">
				</figure>
				<div class="content">
					<h3><?php the_field('titulo'); ?></h3>
					<p>
						<?php the_field('texto'); ?>
					</p>
					<a href="<?php echo esc_url( home_url( '/globus' ) ); ?>" title="Tudo sobre o Globus">Tudo sobre o Globus</a>
				</div>
			</article>
			<div class="clear"></div>
		</div>
	</div>
	<div class="modulos">
		<article>
			<div class="bloco">
				<h1>módulos específicos para <span><?php echo $pagina; ?></span></h1>
				<ul>
					<?php
						$args = array(
							'post_type' => 'modulos',
							'post_status' => 'publish',
							'post__in' => get_field('modulos')
						);
						query_posts($args);
						while ( have_posts() ) : the_post();
					?>
					<li>
						<div class="circulo">
							<span><?php the_title(); ?></span>
							<img src="<?php echo get_template_directory_uri(); ?>/images/img_seta_circulo.png" alt="#">
						</div>
						<div class="esconde">
							<div>
								<strong><?php echo $pagina; ?> /<?php the_title(); ?></strong>
								<?php the_field('descricao'); ?>
							</div>
						</div>
					</li>
					<?php
						endwhile;
						wp_reset_query();
					?>
				</ul>
				<div class="tooltip"><div></div></div>
			</div>
			<div class="slide">
				<h1><span>Outros Módulos Adequados para <?php echo $pagina; ?></span></h1>
				<div class="scroll">
					<ul>
						<?php
							$args = array(
								'post_type' => 'modulos',
								'post_status' => 'publish',
								'post__in' => get_field('outros_modulos'),
								'posts_per_page' => -1,
								);
							query_posts($args);

							while ( have_posts() ) : the_post();
						?>
						<li>
							<div class="circulo">
								<span><?php the_title(); ?></span>
							</div>
							<div class="esconde">
								<div class="descricao">
									<strong><?php the_title(); ?></strong>
									<?php the_field('descricao'); ?>
								</div>
							</div>
						</li>
						<?php
							endwhile;
							wp_reset_query();
						?>
					</ul>
				</div>
				<div class="controles">
					<a href="#" title="Anterior" class="anterior"><span>Anterior</span></a>
					<a href="#" title="Próximo" class="proximo"><span>Próximo</span></a>
				</div>
				<div class="tooltip"><div></div></div>
			</div>
		</article>
	</div>
	<div class="complemento">
		<article class="inteligence">
			<header>
				<h1>SOFTWARE</h1>
				<h2>globus intelligence</h2>
			</header>
			<p>
				<?php the_field('long_descricao_globus_inteligence', 'option'); ?>
			</p>
			<a href="<?php echo esc_url( home_url( '/globus-inteligence' ) ); ?>" title="Conheça mais aqui">Conheça mais aqui</a>
		</article>
		<hr>
		<article class="consultoria">
			<header>
				<h1>BgmRodotec</h1>
				<h2>consultoria</h2>
			</header>
			<p>
				<?php the_field('descricao_longa_de_consultoria', 'option'); ?>
			</p>
			<a href="<?php echo esc_url( home_url( '/consultoria' ) ); ?>" title="Conheça mais aqui">Conheça mais aqui</a>
		</article>
		<article class="globus">
			<div>
				<h1>GLOBUS PARTS</h1>
				<p>
					<?php the_field('descricao_globus_parts', 'option'); ?>
				</p>
				<a href="<?php the_field('link_globus_parts', 'option'); ?>" title="Compre e Venda aqui" target="_blank">Compre e Venda aqui</a>
			</div>
			<figure><img src="<?php the_field('imagem_globus_parts', 'option'); ?>" alt="GLOBUS PARTS"></figure>
		</article>
		<div class="clear"></div>
	</div>
</section>