<?php get_header(); ?>
<?php $category = get_the_category(); ?>
<section id="noticiaOpen">
	<header>
		<h1>BgmRodotec <span><?php echo $category[0]->name;  ?></span></h1>
	</header>
	<div class="news">
		<article>
			<div class="trava">
				<div class="content">
					<figure>
						<img src="<?php the_field('imagem_de_destaque'); ?>" alt="#">
						<figcaption><?php the_field('legenda_da_imagem'); ?></figcaption>
					</figure>
					<header>
						<h2><?php the_title(); ?></h2>
						<time datetime="<?php echo get_the_date(); ?>"><?php echo get_the_date(); ?></time>
					</header>
					<div class="text">
						<?php the_field('texto'); ?>
					</div>
					<div class="share">
						<div class="fb-like" data-href="<?php echo get_permalink(); ?>" data-width="200" data-height="The pixel height of the plugin" data-colorscheme="light" data-layout="button_count" data-action="like" data-show-faces="true" data-send="true"></div>
						<div class="addthis_toolbox addthis_default_style addthis_32x32_style">
							<a class="addthis_button_email"><img src="<?php echo get_template_directory_uri(); ?>/images/bot_email.jpg" alt="#"></a>
							<a class="addthis_button_print"><img src="<?php echo get_template_directory_uri(); ?>/images/bot_print.jpg" alt="#"></a>
						</div>
						<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
						<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=herickcorrea"></script>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</article>
		<?php
			$args = array(
				'cat'           => $category[0]->term_id,
				'post__not_in' => array(get_the_ID()),
				'posts_per_page'    => '2',
			);
			query_posts($args);
			while ( have_posts() ) : the_post();
		?>
		<article>
			<div class="trava">
				<div class="content">
					<figure>
						<img src="<?php the_field('imagem_de_destaque'); ?>" alt="#">
						<figcaption><?php the_field('legenda_da_imagem'); ?></figcaption>
					</figure>
					<header>
						<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
						<time datetime="<?php echo get_the_date(); ?>"><?php echo get_the_date(); ?></time>
					</header>
					<div class="text">
						<p>
							<?php the_field('resumo_do_texto'); ?>
						</p>
					</div>
					<a href="<?php the_permalink(); ?>" title="leia o conteúdo completo">leia o conteúdo completo</a>
				</div>
			</div>
		</article>
		<?php
			endwhile;
			wp_reset_query();
		?>
	</div>
</section>
<?php get_footer(); ?>