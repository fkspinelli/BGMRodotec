<section id="publicacoes">
	<div id="headerTxt">
		<article class="accordeon">
			<header>
				<h1>BgmRodotec <span>Publicações</span></h1>
				<p>Bem-vindo à sala de publicações da BgmRodotec. Baixe jornais, boletins, revistas e outros arquivos da BgmRodotec.</p>
			</header>
			<div>
				<h2>ÚLTIMAS PUBLICAÇÕES</h2>
				<?php
					$blc = 1;
					$i = 1;
					$contador_geral = 1;
					$num_registros = 99;
					$arr = array_reverse(get_field('ultimos_releases'));
					//foreach(get_field('ultimos_releases') as $release){
					foreach($arr as $release){
						if ($i == 1) {
							echo '<ul class="paginacao" data-bloco="'.$blc.'">';
						}
				?>
					<li>
						<a href="<?php echo $release['pdf']; ?>" title="Abrir" class="open" target="_blank"><?php echo $release['titulo']; ?></a>
						<time>publicado em <?php echo dataShort($release['data']); ?></time>
					</li>
				<?php
						if ($i == $num_registros) {
							echo "</ul>";
							$i = 1;
							$blc++;
						} 
						elseif  ($total == $contador_geral && $i < $num_registros) {
							echo "</ul>";
						}
						elseif ($total != $contador_geral && $i < $num_registros) {
							$i++;	
						}
						
						$contador_geral++;
					}
				?>
			</div>
			<div class="more">
				<a href="#" title="Mais publicações" class="loadMore" data-bloco="2">Mais publicações</a>
			</div>
		</article>
	</div>
</section>