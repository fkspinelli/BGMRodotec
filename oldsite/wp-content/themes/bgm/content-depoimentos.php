<section id="depoimentos">
	<header>
		<h1>BgmRodotec <span>depoimentos</span></h1>
	</header>
	<div class="news">
		<?php
			$blc = 1;
			$i = 1;
			$contador_geral = 1;
			$num_registros = 5;
			$depoimentos = get_field('depoimentos');
			$result = array_reverse($depoimentos);
			foreach($result as $depoimentos){
				if ($i == 1) {
					echo '<div class="paginacao" data-bloco="'.$blc.'">';
				}
		?>
		<article>
			<div class="trava">
				<div class="content">
					<figure>
                        <img src="<?php echo $depoimentos['logo_da_empresa']; ?>" alt="#">
                    </figure>
                    <div class="text">
						<h2><?php echo $depoimentos['subtitulo']; ?></h2>
						<p><?php echo $depoimentos['texto']; ?></p>
						<p class="assina">
							<span><?php echo $depoimentos['titulo']; ?></span>
							<?php echo $depoimentos['cargo_profissional']; ?>
						</p>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</article>
		<?php
				if ($i == $num_registros) {
					echo "</div>";
					$i = 1;
					$blc++;
				} 
				elseif  ($total == $contador_geral && $i < $num_registros) {
					echo "</div>";
				}
				elseif ($total != $contador_geral && $i < $num_registros) {
					$i++;	
				}
				
				$contador_geral++;
			}
		?>
	</div>
	<div class="more">
		<a href="#" title="Mais Depoimentos" class="loadMore" data-bloco="2">Mais Depoimentos</a>
	</div>
</section>