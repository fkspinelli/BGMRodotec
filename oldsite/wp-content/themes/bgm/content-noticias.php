<article>
	<div class="trava">
		<div class="content">
			<figure>
				<a href="<?php the_permalink(); ?>"><img src="<?php the_field('imagem_de_destaque'); ?>" alt="#"></a>
				<figcaption><?php the_field('legenda_da_imagem'); ?></figcaption>
			</figure>
			<header>
				<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
				<time datetime=""><?php echo get_the_date(); ?></time>
			</header>
			<div class="text">
				<p>
					<?php the_field('resumo_do_texto'); ?>
				</p>
			</div>
			<a href="<?php the_permalink(); ?>" title="leia o conteúdo completo">leia o conteúdo completo</a>
		</div>
	</div>
</article>