<?php get_header(); ?>
<?php
	$category = get_the_category();
	if(urlPage() == 1 || urlPage() == "noticias" || $category[0]){
		$section = "noticiasPag1";
	} else {
		$section = "noticiasPag2";
	}
?>
<section id="<?php echo $section; ?>">
	<header>
		<h1>BgmRodotec <span><?php printf( __( '%s', 'twentythirteen' ), single_cat_title( '', false ) ); ?></span></h1>
	</header>
	<div class="news">
		<?php if ( have_posts() ) : ?>
		<?php $i = 1; ?>
		<?php while ( have_posts() ) : the_post(); ?>
			<?php
				if ($i == 2) {
					echo '<div class="cinza"><div class="wrap">';
				}

				get_template_part( 'content', 'noticias' );

				if ($i == 3) {
					echo '</div><div class="clear"></div></div>';
				}
				$i++;
			?>
		<?php endwhile; ?>
		<?php else: ?>
			<article>
				<div class="trava">
					<div class="content">
						<div class="text" style="text-align: center;">
							<p>
								Não há publicações para esta categoria.
							</p>
						</div>
					</div>
				</div>
			</article>
		<?php endif; ?>
	</div>
	<div class="more">
		<?php twentythirteen_paging_nav(); ?>
	</div>
</section>
<?php get_footer(); ?>