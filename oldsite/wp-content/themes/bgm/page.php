<?php get_header(); ?>
<?php
	while ( have_posts() ) : the_post();
		if(urlPage2() != "globus"){
			switch(urlPage()){
				default:
					get_template_part( 'content', 'home' );
				break;
				case "empresa":
					get_template_part( 'content', 'empresa' );
				break;
				case "globus":
					get_template_part( 'content', 'globus' );
				break;
				case "globus-inteligence":
					get_template_part( 'content', 'inteligence' );
				break;
				case "globus-cloud":
					get_template_part( 'content', 'cloud' );
				break;
				case "consultoria":
					get_template_part( 'content', 'consultoria' );
				break;
				case "depoimentos":
					get_template_part( 'content', 'depoimentos' );
				break;
				case "imprensa":
					get_template_part( 'content', 'imprensa' );
				break;
				case "publicacoes":
					get_template_part( 'content', 'publicacoes' );
				break;
				case "contato":
					get_template_part( 'content', 'contato' );
				break;
				case "contato-teste":
					get_template_part( 'content', 'contato2' );
				break;
				case "mysuite":
					get_template_part( 'content', 'mysuite' );
				break;
				case "tickets-clientes":
					get_template_part( 'content', 'tickets-clientes' );
				break;
			}
		} else {
			get_template_part( 'content', 'globus_children' );
		}
	endwhile;
?>
<?php get_footer(); ?>