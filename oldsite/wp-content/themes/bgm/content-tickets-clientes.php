<section id="mysuite">
	<div id="headerTxt" data-tipo="1" style="background: #e1e0df;">
		<article>
			<aside>
				<h1><?php the_title(); ?></h1>
				<p class="content"><?php the_content(); ?></p>
				<a target="_blank" href="<?php the_field('page_mysuite_link'); ?>" class="btn-red new-ticket">CLIQUE AQUI <br> <span>para abrir um novo ticket</span></a>
			</aside>
			<div id="main-video">
				<?php the_field('page_mysuite_youtube_video'); ?>
			</div>
		</article>
	</div>
</section>