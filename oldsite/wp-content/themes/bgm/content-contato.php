<section id="contato">
	<div id="headerTxt">
		<article class="accordeon">
			<header>
				<h1>BgmRodotec <span>CONTATO</span></h1>
				<p>Entre em contato com a BgmRodotec ou nos envie o seu currículo escolhendo a opção abaixo.</p>
				<p>Caso você queira entrar em contato com o suporte técnico <a class="gray-underline" href="http://bgmrodotec.com.br/tickets/" target="_blank">clique aqui.</a></p>
			</header>
		<script>
		$(document).ready( function() {
		  $('#falseinput').click(function(){
			$("#arquivo").click();
		  });
		});
		</script>

		<script type="text/javascript" language="javascript">
		$(document).ready(function(){
			$("#assuntos a").click(function(){

				if ($("#assunto").val() == "Trabalhe Conosco" ) {

					$("#falseinput").slideDown("fast"); //Slide Down Effect

				} else {

					$("#falseinput").slideUp("fast");    //Slide Up Effect

				}
			});

		});
		</script>
            <form action="<?php echo get_template_directory_uri(); ?>/envia.php" onsubmit="return checa_formulario(this)" method="post" enctype="multipart/form-data" class="formDefaut" name="formulario" id="formulario">
				<fieldset class="first">
					<div class="select">
						<span class="wpcf7-form-control-wrap text-636">
							<input type="text" name="assunto" id="assunto" value="Fale Conosco" size="40" class="wpcf7-form-control wpcf7-text" />
						</span>
						<a href="#" class="open down" id="assuntos"><span>Open</span></a>
						<div id="assuntos">
							<ul>
								<li><a href="#" data-value="Fale Conosco">Fale Conosco</a></li>
								<li><a href="#" data-value="Trabalhe Conosco">Trabalhe Conosco</a></li>
							</ul>
						</div>
					</div>
					<p>
						<span class="wpcf7-form-control-wrap nome">
							<input type="text" name="nome" value="Escreva seu Nome" size="40" class="wpcf7-form-control wpcf7-text" />
						</span>
					</p>
					<p>
						<span class="wpcf7-form-control-wrap email-895">
							<input type="email" name="email" value="Escreva seu E-mail" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-email" />
						</span>
					</p>
				</fieldset>
				<fieldset class="second">
					<p>
						<span class="wpcf7-form-control-wrap textmensagem">
							<textarea name="mensagem" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea">Escreva sua Mensagem</textarea>
						</span>
					</p>
                    <span class="wpcf7-form-control-wrap anexo">
                    <input id="arquivo" name="arquivo" class="wpcf7-form-control wpcf7-file hide" type="file" style="display:none">
                    </span>
					<p>
						<a id="falseinput" href="#" style="display: none;">Anexar currículo</a>
                        <input type="submit" value="Enviar" class="wpcf7-form-control wpcf7-submit" />
					</p>
				</fieldset>
				<div class="clear"></div>
			</form>
			<header>
                <?php the_field('unidades_parceiros'); ?>
			</header>
		</article>
	<!-- Google Code for Contato Conversion Page -->
	<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 1064836977;
	var google_conversion_language = "pt";
	var google_conversion_format = "2";
	var google_conversion_color = "ffffff";
	var google_conversion_label = "0NT1CMOr-gcQ8b7g-wM";
	var google_remarketing_only = false;
	/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
	<div style="display:inline;">
	<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1064836977/?label=0NT1CMOr-gcQ8b7g-wM&amp;guid=ON&amp;script=0"/>
	</div>
	</noscript>
	</div>	
</section>