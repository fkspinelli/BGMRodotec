<section id="empresa">
	<div id="headerTxt" data-tipo="1">
		<article>
			<header>
				<h1><?php the_field('titulo'); ?></h1>
				<h2><?php the_field('subtitulo'); ?></h2>
			</header>
			<?php the_field('texto'); ?>
		</article>
		<figure>
			<img src="<?php the_field('imagem_de_fundo'); ?>" alt="#">
		</figure>
	</div>
	<div class="bg">
		<div class="trava">
			<article class="missao">
				<h1>nossa <span>missão</span></h1>
				<?php the_field('texto_de_missão'); ?>
			</article>
			<article class="visao">
				<h1>nossa <span>visão</span></h1>
				<?php the_field('texto_de_visao'); ?>
			</article>
			<article class="valores">
				<h1>nossos <span>valores</span></h1>
				<?php the_field('texto_de_valores'); ?>
			</article>
		</div>
	</div>
	<article class="relacionamentos">
		<h1><?php the_field('titulo_de_relacionamentos'); ?></h1>
		<p><?php the_field('texto_de_relacionamentos'); ?></p>
	</article>
	<article class="metodologia">
		<h1><?php the_field('titulo_de_metodologia'); ?></h1>
		<p><?php the_field('texto_de_metodologia'); ?></p>
	</article>
	<article class="solucoes">
		<figure>
			<img src="<?php the_field('imagem_de_solucoes'); ?>" alt="#" usemap="#Map">
			<map name="Map">
				<area shape="rect" coords="64,30,249,148" href="<?php echo esc_url( home_url( '/globus-consultoria' ) ); ?>" alt="Consultoria">
				<area shape="rect" coords="11,153,253,270" href="<?php the_field('link_globus_parts', 'option'); ?>" alt="Globus Parts" target="_blank">
				<area shape="rect" coords="255,32,457,147" href="<?php echo esc_url( home_url( '/globus' ) ); ?>" alt="Globus">
				<area shape="rect" coords="259,153,477,292" href="<?php echo esc_url( home_url( '/globus-inteligence' ) ); ?>" alt="Globus Intelligence">
			</map>
		</figure>
		<div class="content">
			<header>
				<h1><?php the_field('titulo_de_solucoes'); ?></h1>
				<h2><?php the_field('subtitulo_de_solucoes'); ?></h2>
			</header>
			<p><?php the_field('texto_de_solucoes'); ?></p>
		</div>
		<div class="clear"></div>
	</article>
	<article class="videos">
		<h1>ASSISTA AOS NOSSOS VÍDEOS</h1>
		<br>
		<?php
			$urlsYoutube = preg_split('/$/m', get_field('videos_empresa'));
			if($urlsYoutube){
				foreach ($urlsYoutube as $idx => $url) {
					if($idx % 2 == 0){
						?>
						<div style="float: left; width: 390px; margin-bottom: 40px;">
							<?php
							if($url){
								echo convertYoutubeUrlToEmbed($url, 420, 315);
							}
							?>
						</div>
						<?php
					}else{
						?>
						<div style="float: right; width: 360px; margin-bottom: 40px;">
							<?php
							if($url){
								echo convertYoutubeUrlToEmbed($url, 420, 315);
							}
							?>
						</div>
						<div class="clear"></div>
						<?php
					}
				}
			};
		?>
		<div class="clear"></div>
	</article>


</section>