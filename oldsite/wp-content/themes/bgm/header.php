<?php

/**

 * The Header for our theme.

 *

 * Displays all of the <head> section and everything up till <div id="main">

 *

 * @package WordPress

 * @subpackage Twenty_Thirteen

 * @since Twenty Thirteen 1.0

 */

?><!DOCTYPE html>

<!--[if IE 7]>

<html class="ie ie7" <?php language_attributes(); ?>>

<![endif]-->

<!--[if IE 8]>

<html class="ie ie8" <?php language_attributes(); ?>>

<![endif]-->

<!--[if !(IE 7) | !(IE 8)  ]><!-->

<html <?php language_attributes(); ?>>

<!--<![endif]-->

<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width">
<meta name="google-site-verification" content="kGAtw0kyzDf3XilCMOKhyAq_Ai0nio0zdETFv2ygsuo" />

<title><?php wp_title( '|', true, 'right' ); ?></title>

<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" />

<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/style.css" media="all">

<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/js/fancybox/jquery.fancybox.css" media="all">

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.10.2.min.js"></script>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.color.js"></script>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/fancybox/jquery.fancybox.js"></script>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/hc.js?t=1407781133795"></script>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/hc.geral.js?t=7"></script>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/hc.slides.js?t=1"></script>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/moment.min.js"></script>

<link rel="profile" href="http://gmpg.org/xfn/11">

<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<script type="text/javascript">


$(function(){



	hc.path = '<?php //echo get_template_directory_uri(); ?>';



	hc.init([

		hc.webdoor,

		hc.spots,

		hc.slides,

		hc.parts,

		hc.categorias,

		hc.modulos,

		hc.depoimentos,

		hc.publicacoes,

		hc.form

	]);



});



(function(d, s, id) {

	var js, fjs = d.getElementsByTagName(s)[0];

  	if (d.getElementById(id)) return;

	js = d.createElement(s); js.id = id;

	js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=629723237056017";

	fjs.parentNode.insertBefore(js, fjs);

}(document, 'script', 'facebook-jssdk'));



</script>
<!-- wp_head(); -->
<?php wp_head(); ?>
<style>

html { margin-top: 0px !important; }
* html body { margin-top: 0px !important; }

</style>

<!-- /wp_head(); -->
</head>



<body <?php body_class(); ?>>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-772002-1', 'auto');
  ga('send', 'pageview');

</script>

<div id="fb-root"></div>

<header>
	<nav class="adm">

			<h3 class="esconde">Menu Administrativo</h3>

			<div>
				<ul>

					<?php /*?><li><a href="<?php the_field('link_area_do_cliente', 'option'); ?>" target="_blank" title="Área do cliente">área do cliente</a></li><?php */?>

					<!-- <li><a href="http://bgmrodotec.com.br/tickets/" target="_blank" title="Globus Parts">suporte técnico</a></li> -->
                    <li><a href="#" title="Área do cliente">Área do cliente</a>
                    	<ul class="sub-menu">
                    		<li><a href="/tickets-clientes">Tickets</a></li>
                    		<li><a href="http://bgmrodotec.com.br/pseonline/" target="_blank">PSE Online</a></li>
                    	</ul>
                    </li>

					<li><a href="<?php the_field('link_globus_parts', 'option'); ?>" target="_blank" title="Globus Parts">globus parts</a></li>

					<!-- <li><a href="http://bgmrodotec.com.br/tickets/" target="_blank" title="Globus Parts">suporte técnico</a></li> -->

					<!-- <li><?php the_field('telefone', 'option'); ?></li> -->
					<form class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get" role="search">
						<input class="search-field" type="text" title="Pesquisar por:" name="s" value="">
						<input class="search-submit" type="submit" value="Pesquisar">
					</form>
				</ul>
			</div>

		</nav>

		<hr>
	<div class="trava">

		<hgroup>

			<h1>

				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">

					<img src="<?php the_field('logo_do_topo', 'option'); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">

				</a>

			</h1>

			<h2 class="esconde"><?php bloginfo( 'description' ); ?></h2>

		</hgroup>

		<hr>

		<nav class="menu">

			<h3 class="esconde">Menu Principal</h3>

			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>

			<?php //get_search_form(); ?>

		</nav>

		<hr>

	</div>

</header>

<hr>