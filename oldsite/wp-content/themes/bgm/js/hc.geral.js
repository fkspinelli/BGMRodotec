hc.spots = function(){
	
	function spots(objeto){
		
		var imagem = objeto.find('figure');
		var aside = objeto.find('aside');
		var conteudo = aside.find('div');
		var bt_open = aside.find('a.open');
		var bt_open2 = imagem.find('a.open');
		var bt_close = aside.find('a.close');
		var bt_close2 = imagem.find('a.close');
		
		var status = 0;
		
		function zeramento(){
			$('section#spots article figure').stop().animate({backgroundColor:'#4e5156'},200);	
			$('section#spots article aside').stop().animate({width:65},200);
			$('section#spots article aside div').stop().animate({opacity:0},200);	
			$('section#spots article aside a.close').stop().animate({opacity:0},200,function(){
				$(this).css({'display':'none'});	
			});
			$('section#spots article aside a.open').css({'display':'block'}).stop().animate({opacity:1},200);
		}
		
		bt_open.click(function(){
			zeramento();
			bt_open.stop().animate({opacity:0},200,function(){
				$(this).css({'display':'none'});	
			});
			bt_close.css({'display':'block'}).stop().animate({opacity:1},200);
			imagem.stop().animate({backgroundColor:'#eb4331'},200);	
			aside.stop().animate({width:180},200,function(){
				conteudo.stop().animate({opacity:1},200)
			});	
			return false;
		});
		
		bt_open2.click(function(){
			zeramento();
			bt_open.stop().animate({opacity:0},200,function(){
				$(this).css({'display':'none'});	
			});
			bt_close.css({'display':'block'}).stop().animate({opacity:1},200);
			imagem.stop().animate({backgroundColor:'#eb4331'},200);	
			aside.stop().animate({width:180},200,function(){
				conteudo.stop().animate({opacity:1},200)
			});	
			return false;
		});
		
		bt_close.click(function(){
			zeramento();
			return false;
		});
		
		bt_close2.click(function(){
			zeramento();
			return false;
		});
		
	}
	
	$('section#spots article').each(function(){
		spots($(this));	
	});
	
	
}

hc.parts = function(){
	
	function tooltip(content,local){
		var html = content;
		var box = local;
		var wrap = local.parent();
		
		box.html(html);
		var altura = box.innerHeight()
		
		wrap.stop().animate({height:altura},200);
	}
	
	function modulosSlide(objeto){
		var li = objeto;
		var circulo = li.find('div.circulo');
		var content = li.find('div.esconde').html();
		var wrap = objeto.parent().parent().parent().find('div.tooltip');
		var local = objeto.parent().parent().parent().find('div.tooltip div');
		
		li.mouseover(function(){
			circulo.stop().animate({color:'#eb4331'},200);
			tooltip(content,local)
		}).mouseout(function(){
			circulo.stop().animate({color:'#4e5156'},200);
			wrap.stop().animate({height:0},200);
			local.html('');
		});
	}
	
	$('section#parts div.slide div.scroll ul li').each(function(){
		modulosSlide($(this));	
	});
	
	
}

hc.categorias = function(){
	
	/* SELECT CATEGORIA */
	
	var menuSelect = $('div#headerTxt div.select');
	var a_menuSelect = menuSelect.find('a.seta');
	var nav_menuSelect = menuSelect.find('nav');
	var ul_menuSelect = nav_menuSelect.find('ul');
	
	var staturSelect = 0;
	
	a_menuSelect.click(function(){
		if(staturSelect == 0){
			var altura = ul_menuSelect.height();
			nav_menuSelect.stop().animate({height:altura},300);
			a_menuSelect.removeClass('open').addClass('close');
			staturSelect = 1;
		} else {
			nav_menuSelect.stop().animate({height:0},300);
			a_menuSelect.removeClass('close').addClass('open');
			staturSelect = 0;
		}
		return false;
	});
	
}

hc.modulos = function(){
	
	/* MÓDULOS */
	
	function tooltip(content,local){
		var html = content;
		var box = local;
		var wrap = local.parent();
		
		box.html(html);
		var altura = box.height()
		
		wrap.stop().animate({height:altura},200);
	}
	
	function modulosCirculo(objeto){
		
		var li = objeto;
		var circulo = li.find('div.circulo');
		var circulo_img = circulo.find('img');
		var content = li.find('div.esconde').html();
		var wrap = objeto.parent().parent().find('div.tooltip');
		var local = objeto.parent().parent().find('div.tooltip div');
		
		li.mouseover(function(){
			circulo.stop().animate({backgroundColor:'#eb4331', color:'#ffffff',borderColor:'#f8f7f5'},200);
			circulo_img.stop().animate({opacity:0},200);
			tooltip(content,local)
		}).mouseout(function(){
			circulo.stop().animate({backgroundColor:'#f8f7f5', color:'#4e5156',borderColor:'#4e5156'},200);
			circulo_img.stop().animate({opacity:1},200);
			wrap.stop().animate({height:0},200);
			local.html('');
		});
		
	}
	
	$('section#solucoes div.modulos article div.bloco ul li').each(function(){
		modulosCirculo($(this));	
	});
	
	function modulosSlide(objeto){
		var li = objeto;
		var circulo = li.find('div.circulo');
		var content = li.find('div.esconde').html();
		var wrap = objeto.parent().parent().parent().find('div.tooltip');
		var local = objeto.parent().parent().parent().find('div.tooltip div');
		
		li.mouseover(function(){
			circulo.stop().animate({color:'#eb4331'},200);
			tooltip(content,local)
		}).mouseout(function(){
			circulo.stop().animate({color:'#4e5156'},200);
			wrap.stop().animate({height:0},200);
			local.html('');
		});
	}
	
	$('section#solucoes div.modulos article div.scroll ul li').each(function(){
		modulosSlide($(this));	
	});
	
}

hc.accordeon = function(){
	
	/* ACCORDEON IMPRENSA */
	
	var listaLink = $('section#imprensa div#headerTxt article > div ul li');
	var allContent = $('section#imprensa div#headerTxt article > div ul li div.bloco');
	
	function accordeon(objeto){
		
		var linkAccordeon = objeto.find('> a');
		var animaAccordeon = objeto.find('div.bloco');
		var contentAccordeon = objeto.find('div.content');
		
		linkAccordeon.click(function(){
			
			var altura = contentAccordeon.height();
			var status = objeto.attr('data-status');
			
			if(status == 'off'){
				allContent.stop().animate({height:0},200);
				animaAccordeon.stop().animate({height:altura},300);
				listaLink.attr('data-status','off');
				objeto.attr('data-status','on');
			} else {
				allContent.stop().animate({height:0},200);
				listaLink.attr('data-status','off');
			}
			return false;
		});
		
	}
	
	listaLink.each(function(){
		$(this).attr('data-status','off')
		accordeon($(this));	
	})
	
	
}

hc.depoimentos = function(){
	
	var clickMore = $('section#depoimentos div.more a');
	
	clickMore.click(function(){
		var bloco = $(this).attr('data-bloco');
		var objeto = $('section#depoimentos div.news div[data-bloco="'+bloco+'"] article');
		objeto.css({'display':'block'}).stop().animate({opacity:1},300);
		
		var newBloco = parseInt(bloco) + 1;
		$(this).attr('data-bloco',newBloco);
		
		return false;
	});
	
	
}

hc.form = function(){
	
	/* BLUR FORMS */
	
	$('form input[type="text"], form input[type="email"], form textarea').each(function(){
		var valor = $(this).val();
		$(this).focus(function(){
			if (this.value == valor) {
				this.value = '';
			}
		}).blur(function(){
			if (this.value == '') {
				this.value = valor;
			}
		});
	});
	
	/* SELECT FORM */
	
	var contato = $('form.wpcf7-form, form.formDefaut');
	
	var staturSelect = 0;
	
	function selectForm(objeto){
		
		var divSelect = objeto.find('div.select');
		var inputSelect = divSelect.find('input');
		var inputSeta = divSelect.find('a.open');
		var blocoValues = divSelect.find('div');
		var listaValues = blocoValues.find('ul');
		var newValue = listaValues.find('a');
		
		inputSeta.click(function(){
			altura = listaValues.height();
			if(staturSelect == 0){
				blocoValues.stop().animate({height:altura},300);
				inputSeta.removeClass('down').addClass('up');
				staturSelect = 1
			} else {
				blocoValues.stop().animate({height:0},300);
				inputSeta.removeClass('up').addClass('down');
				staturSelect = 0;
			}
			return false;
		});
		
		newValue.click(function(){
			var value = $(this).attr('data-value');
			inputSelect.val(value);
			blocoValues.stop().animate({height:0},300);
			inputSeta.removeClass('up').addClass('down');
			staturSelect = 0;
			return false;
		});
		
	}
	
	selectForm(contato);
	
	/* ALERTAS */
	
	$(".sucesso").fancybox({
		padding		 : 0,
		width        : 400,
		height       : 200,
		title        : false,
		closeClick   : true,
		type         : 'inline'
	});
	
	$(".erro_captcha").fancybox({
		padding		 : 0,
		width        : 400,
		height       : 200,
		title        : false,
		closeClick   : true,
		type         : 'inline'
	});

};

$(document).ready(function(){
	$.googleAnalyticsEventTracking(
	{
		// Does the contact Form was submitted? With subject was selected?
	    targetSelector: '#formulario',
	    events: {
	        eventType: 'submit',
	        gaEventData: {
	            category: 'Contato',
	            action: 'Formulário',
	            label: function($target) {
	                return $target.find('#assunto').val();
	            }
	        }
	    }
	},{
		// Which Outbound Links were clicked
		targetSelector: 'a:not([href*=bgmrodotec]):not([href^="/"]):not([href="#"]):not([href="javascript:void(0)"])',
		events: {
			eventType: 'click',
			gaEventData: {
				category: 'Outbound Link',
				action: function($target) {
					return $target.text();
				},
				label: function($target) {
					return $target.attr('href');
				}
			}
		}
	},{
		// Is the "modules" slides used?
		targetSelector: '.modulos .controles a',
		events: {
			eventType: 'click',
			gaEventData: {
				category: 'Módulos',
				action: 'Controle',
				label: function($target) {
					return $target.attr('Title');
				},
				nonInteraction: 1
			}
		}
	},{
		// Solutions Menu was opened?
		targetSelector: '#spots a[title="Ver mais"]',
		events: {
			eventType: 'click',
			gaEventData: {
				category: 'Soluções Spot',
				action: 'Abrir Menu',
				label: function($target) {
					return $target.parents('article').attr('class');
				},
				nonInteraction: 1
			}
		}
	},{
		// Which menu is most used? Header or Footer?
		targetSelector: '.nav-menu a',
		events: {
			eventType: 'click',
			gaEventData: {
				category: 'Main Nav',
				action: function($target) {
					return $target.parents('header, footer').prop("tagName");
				},
				label: function($target) {
					return $target.attr('href');
				},
				nonInteraction: 1
			}
		}
	},{
		// Does the main banner CTA was clicked?
		targetSelector: '#webdoor a[href!=#]',
		events: {
			eventType: 'click',
			gaEventData: {
				category: 'Main Banner',
				action: 'Slide CTA',
				label: function($target) {
					return $target.parents('ul').find('[class=ativo]').data('posicao');
				},
				nonInteraction: 1
			}
		}
	},{
		// Which iCal link is most clicked
		targetSelector: '.tribe-events-cal-links a',
		events: {
			eventType: 'click',
			gaEventData: {
				category: 'Treinamento',
				action: 'iCal Button',
				label: function($target) {
					return $target.text();
				}
			}
		}
	});
});