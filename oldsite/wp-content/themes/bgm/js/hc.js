var hc = {
	init: function(imports)
	{
		for (var i in imports)
		{
			if (imports[i]) {
				imports[i].apply(hc);
			}
		}
	}

};

/*!
 * jQuery toggleTarget Plugin
 *
 * Toggles an element's visibility defined by a HTML attribute
 * With some of the effects options (fade, slide, none)
 *
 * It's possible to specify the root element of the target by config param or by HTML attribute (This one will take precedence)
 *
 * Author: Diego ZoracKy | @DiegoZoracKy | http://diegozoracky.com
 */
$.fn.toggleTarget = function(opt) {
	var sets = {
		classOn: 'selected',
		effect: 'fade',
		transitionSpeed: 'slow',
		innerTargetToClose: null,
		rootEl: null,
		centerOnPage: true,
		fixedPosition: true,
		overlay: false,
		overlayBackground: '#666',
		overlayOpacity: 0.5,
		closeOnOverlayClick: true,
		closeElement: {
			element: '<a href="#"><img src="/wp-content/themes/bgm/images/pop-up-close.png" border="0"></a>',
			css: {
				position: 'absolute',
				width: '22px',
				height: '21px',
				right: '-20px',
				top: '-5px',
				border: 'none'
			}
		},
		beforeToggle: function(e, $target) {},
		onToggle: function(e, $target) {},
		afterToggle: function(e, $target) {},
		onClose: function(e, $target) {}
	};

	var pluginInfo = {
		name: 'toggleTarget',
		abbr: 'tglt'
	};

	var centerTarget = function($target, sets) {
		var scrollX = (sets.fixedPosition) ? 0 : $('body').scrollLeft();
		var scrollY = (sets.fixedPosition) ? 0 : $('body').scrollTop();
		$target.css({
			// left: (($(window).width() / 2) - ($target.width() / 2)) + scrollX,
			top: (($(window).height() / 2) - ($target.height() / 2)) + scrollY
		});
	};

	var setCenterTarget = function($target, sets) {

		var targetCenteredZindex = 8000;

		$target.css('visibility', 'hidden');
		$target.show();
		sets.onToggle();
		var positionElement = (sets.fixedPosition) ? 'fixed' : 'absolute';
		var scrollX = (sets.fixedPosition) ? 0 : $('body').scrollLeft();
		var scrollY = (sets.fixedPosition) ? 0 : $('body').scrollTop();

		$target.appendTo('body');
		$target.css({
			position: positionElement,
			// left: (($(window).width() / 2) - ($target.width() / 2)) + scrollX,
			top: (($(window).height() / 2) - ($target.height() / 2)) + scrollY,
			'z-index': targetCenteredZindex
		});

		$target.hide();
		$target.css('visibility', 'visible');

		if ($target.not(':visible')) {
			$(window).on('resize.toggleTarget', function() {
				centerTarget($target, sets);
			});
			$(window).trigger('resize.toggleTarget');
		} else {
			$(window).off('resize.toggleTarget');
		}
	};

	if (opt) opt = $.extend(true, {}, sets, opt);

	return this.each(function() {

		var $main = $(this);
		var targetSelector = $main.data('tglt-target') || opt.target;

		var effect = $main.data('tglt-effect') || opt.effect;
		var rootEl = $main.data('tglt-root-el') || opt.rootEl;
		var centerOnPage = $main.data('tglt-center-on-page') || opt.centerOnPage;
		var $originalParent = $main.parent();
		var overlay = $main.data('tglt-overlay') || opt.overlay;
		var $overlay = $('<div id="tglt-overlay"></div>');

		var overlayZindex = 7000;

		var $target;
		if (rootEl) {
			$target = $main.parents(rootEl).find(targetSelector);
		} else {
			$target = $(targetSelector);
		}

		$target.data('toggleTarget', {
			linkedElement: $main
		});

		var cssReset = {
			position: '',
			width: '',
			height: '',
			left: '',
			top: '',
			'z-index': ''
		};

		var cssOverlay = {
			position: 'fixed',
			width: '100%',
			height: '100%',
			top: 0,
			left: 0,
			background: opt.overlayBackground,
			opacity: opt.overlayOpacity,
			'z-index': overlayZindex,
			display: 'none',
			'-webkit-transform': 'translate3d(0, 0, 0)',
			'-webkit-perspective': 1000,
			'-webkit-backface-visibility': 'hidden'
		};

		$main.on('click.toggleTarget', function(e) {
			e.preventDefault();

			if (overlay) {
				$overlay.css(cssOverlay).appendTo('body');
				$overlay.stop().fadeToggle(opt.transitionSpeed);
			}

			if (centerOnPage) {
				setCenterTarget($target, opt);
			}

			switch (effect) {
				case 'fade':
					if (opt.beforeToggle(e, $target) === false) {
						return;
					}
					$target.stop().fadeToggle(opt.transitionSpeed, function() {
						opt.afterToggle(e, $target);
					});
					opt.onToggle(e, $target);
					break;
				case 'slide':
					if (opt.beforeToggle(e, $target) === false) {
						return;
					}
					$target.stop().slideToggle(opt.transitionSpeed, function() {
						opt.afterToggle(e, $target);
					});
					opt.onToggle(e, $target);
					break;
				default:
					if (opt.beforeToggle(e, $target) === false) {
						return;
					}
					$target.toggle();
					opt.afterToggle(e, $target);
					opt.onToggle(e, $target);
			}
		});

		if (opt.innerTargetToClose || opt.closeOnOverlayClick) {
			var $elClosesIt = $overlay.add( $target.find(opt.innerTargetToClose ) );

			if(opt.closeElement){
				var $closeElement = $(opt.closeElement.element).css(opt.closeElement.css);
				$closeElement.prependTo($target);
				$closeElement.css({width: '22px',height: '21px'});
				$elClosesIt = $elClosesIt.add($closeElement);
			}

			if (!$elClosesIt.size()){
				return;
			}

			$elClosesIt.on('click.toggleTarget', function(e) {
				e.preventDefault();

				$overlay.stop().fadeToggle(opt.transitionSpeed);

				switch (effect) {
					case 'fade':
						$target.stop().fadeOut(opt.transitionSpeed, function() {
							$target.css(cssReset).appendTo($originalParent);
							opt.onClose(e, $target);
						});
						break;
					case 'slide':
						$target.stop().slideUp(opt.transitionSpeed, function() {
							$target.css(cssReset).appendTo($originalParent);
							opt.onClose(e, $target);
						});
						break;
					default:
						$target.hide();
						$target.css(cssReset).appendTo($originalParent);
						opt.onClose(e, $target);
				}

				$(window).off('resize.toggleTarget');
			});
		}
	});
};


/**
 * MomentCron
 *
 * Dependencies: Moment.js
 *
 * @namespace MomentCron
 * @public
 */
(function(factory) { // ~UMD :: Uses AMD or browser globals to create the module.
	'use strict';

	var module = {
		name: 'MomentCron',
		dependencies: [
			'moment'
		]
	};

	if (typeof define === 'function' && define.amd) define(module.dependencies, factory);
	else window[module.name] = factory(jQuery);

}(function($) {
	'use strict';

	/**
	 * Module info
	 *
	 * @property {object}	_info - Module info
	 * @property {string}	_info.name - Module Name
	 * @property {string}	_info.version - Module Version
	 * @public
	 * @memberOf MomentCron
	 */
	var _info = {
		name: 'MomentCron',
		version: '1.0'
	};

	/**
	 * Module's default configuration
	 *
	 * @property {object}	defaults - Module's default configuration
	 * @property {boolean}	defaults.moduleLog - Defines when the module should log some processing steps at console
	 * @private
	 * @memberOf MomentCron
	 */
	var defaults = {
		moduleLog: false,
		localStorageKey: _info.name,
		timeGap: 'minutes',
		timeUnits: [
			'year',
			'month',
			'week',
			'day',
			'hour',
			'minute',
			'second'
		]
	};

	/**
	 * Will store specific configuration defined at module's init
	 *
	 * @type {object}
	 * @memberOf MomentCron
	 */
	var setts = {};

	/**
	 * Logs a message on Console concatenating the app name, to maintain a friendly understanding.
	 * Used by the app to 'Talk' about its status with the developers, through Console.
	 * Needs to set defaults.moduleLog = true to get all messages that is being said
	 * In case of a error, all debug messages will be shown on console as a warning, independently of defaults.moduleLog.
	 *
	 * @param {string} msg - Message to be displayed
	 * @param {string} type - 'warn' as a value, should be used in case of debug
	 * @returns {undefined}
	 * @memberOf MomentCron
	 */
	var moduleMsg = function(msg, type) {
		var logStyles = ['color: #FF0000'].join(';');
		msg = '"' + _info.name + '" says :: ' + msg;
		if (type && type.toLowerCase() === 'warn') {
			if (window.chrome) {
				console.warn('%c' + msg, logStyles);
			} else {
				console.warn(msg);
			}
		}else if (setts.moduleLog){
			console.log(msg);
		}
	};

	var exec = function(jobKey, job){
		job.toExec();
		job.executedAt = (new Date());

		var localStorageCurrent = JSON.parse( localStorage.getItem(setts.localStorageKey) );
		localStorageCurrent[jobKey] = job;
		localStorage.setItem(setts.localStorageKey, JSON.stringify(localStorageCurrent));
	};

	/**
	 * Initial function. The one which initializes the module
	 *
	 * @param {object} newSetts - New configurations to be used on top of the default one
	 * @returns {undefined}
	 * @public
	 * @memberOf MomentCron
	 */
	var init = function(newSetts) {
		setts = $.extend(true, {}, defaults, newSetts);

		for(var jobKey in setts.jobs){
			var job = setts.jobs[jobKey];

			if(!localStorage.getItem(setts.localStorageKey)){
				moduleMsg('Creating Module Object at localStorage: "'+ setts.localStorageKey +'"');
				localStorage.setItem(setts.localStorageKey, JSON.stringify({}));
			}

			if(job.startDate && moment().diff( moment(job.startDate) ) < 0){
				moduleMsg('"'+ jobKey +'" Waiting for its start date ('+ job.startDate +') to execute it');
				continue;
			}

			if(job.endDate && moment().diff( moment(job.endDate) ) >= 0){
				moduleMsg('"'+ jobKey +'" passed its end date ('+ job.endDate +') . Will not be executed anymore');
				continue;
			}

			if(! JSON.parse( localStorage.getItem(setts.localStorageKey) )[jobKey]){
				moduleMsg('"'+ jobKey +'" Running for the first time');

				exec(jobKey, job);
			} else {
				moduleMsg('"'+ jobKey +'" Trying to run again');

				var measurement = job.timeGap.split(' ');
				if(!measurement.length > 2){
					moduleMsg('measurement is not well formed');
					continue;
				}

				var time = measurement[0];
				var unit = measurement[1];
				unit = unit.replace(/s$/, '');

				if(setts.timeUnits.indexOf(unit) < 0){
					moduleMsg(unit + ' is not a valid time unit', 'warn');
					continue;
				}

				var executedAt = moment( JSON.parse( localStorage.getItem(setts.localStorageKey) )[jobKey].executedAt );
				var diff = moment().diff(executedAt, unit);

				if(diff < time){
					moduleMsg('"'+ jobKey +'" is not in time to execute. Still on its "' + time + ' ' + unit + '" gap');
					continue;
				}

				moduleMsg('"'+ jobKey +'" Running again');
				exec(jobKey, job);
			}
		}
	};

	return {
		_info: _info,
		init: init
	};
}));

/**
 * googleAnalyticsEventTracking
 * Define in a simple, and easy, way elements and events to be tracked by Google Analytics
 *
 * Author: Diego ZoracKy | @DiegoZoracKy
 * GitHub: http://github.com/DiegoZoracKy/google-analytics-event-tracking
 */
!function(){"use strict";$.googleAnalyticsEventTracking=function(e){function t(e,t){return function(o){var a={hitType:"event"};for(var c in e){var g=r[c.toLowerCase()]||c;a[g]=e[c],e[c].constructor==Function&&(a[g]=e[c]($(o.currentTarget),$(t)))}n?console.log("send",a):ga("send",a)}}var n=!1,o=[],r={category:"eventCategory",action:"eventAction",label:"eventLabel",value:"eventValue",nonInteraction:"nonInteraction"};e.constructor!=Array&&(e=Array.prototype.slice.call(arguments)),e.forEach(function(e){o.push(e),e.events.constructor!=Array&&(e.events=[e.events]),e.events.forEach(function(n){e.delegateTo?$(e.delegateTo).on(n.eventType,e.targetSelector,t(n.gaEventData,e.delegateTo)):$(e.targetSelector).on(n.eventType,t(n.gaEventData,e.delegateTo))})}),$.googleAnalyticsEventTracking.setDebugMode=function(e){n=e},$.googleAnalyticsEventTracking.getRegisteredEvents=function(){return o}}}(jQuery);