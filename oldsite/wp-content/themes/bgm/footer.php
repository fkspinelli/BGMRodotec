<footer>

	<div class="trava">

		<div class="social">

			<ul>

				<li><a href="<?php the_field('link_facebook', 'option'); ?>" title="Facebook" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/bot_facebook.jpg" alt="ícone facebook"></a></li>

				<li><a href="<?php the_field('link_twitter', 'option'); ?>" title="Twitter" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/bot_twitter.jpg" alt="ícone twitter"></a></li>

				<li><a href="<?php the_field('link_youtube', 'option'); ?>" title="Youtube" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/bot_youtube.jpg" alt="ícone youtube"></a></li>

			</ul>

		</div>

		<hr>

		<article class="sitemap">

			<nav>

				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>

			</nav>

			<nav class="adm">

				<ul>

					<?php /*?><li><a href="<?php the_field('link_area_do_cliente', 'option'); ?>" target="_blank" title="Área do cliente">área do cliente</a></li><?php */?>

	                    		<!-- <li><a href="#" title="Área do cliente">área do cliente</a></li> -->

					<li><a href="<?php the_field('link_globus_parts', 'option'); ?>" target="_blank" title="Globus Parts">globus parts</a></li>

				</ul>

			</nav>

		</article>

		<hr>

		<div class="copy">

			<p class="ligue">Ligue: <span><?php the_field('telefone', 'option'); ?></span></p>

            <p class="ligue">Unidades: <span><?php the_field('telefone_unidades', 'option'); ?></span></p>

			<p>Copyright © 2013 BgmRodotec Ltda. Todos os direitos reservados. Design e Desenvolvimento &#160;<a style="display: inline-block;" href="http://orkestra.com.br" target="_blank" alt="Orkestra"><img src="<?php echo get_template_directory_uri(); ?>/images/logo-orkestra-assinatura.png"></a></p>

		</div>

		<?php // wp_signature(); ?>

	</div>

</footer>

<?php if( strtolower( get_field('pop_up_enabled', 'option') ) == 'sim'): ?>

	<a class="tgl-target" href="#" style="display: none;" data-tglt-target=".pop-vaga"></a>
	<div class="pop-vaga" style="display: none; position: relative; width: 320px; height: 410px; left: 50%; margin-left: -120px;">
	    <a href="http://bgmrodotec.com.br/seja-um-membro-da-equipe-da-bgmrodotec-confira-as-oportunidades-abertas/" target="_blank" style="border: 1px solid red; border: 1px solid transparent;width: 215px;position: absolute;bottom: 56px;left: 48px;height: 67px;display: block;">&#160;</a>
	    <img src="<?php echo get_template_directory_uri(); ?>/images/pop-up-vagas.png" style="width: 320px; height: 410px;">
	</div>
	<script>
		$(function() {
			'use strict';

			$('.tgl-target').toggleTarget({
				overlay: true,
				overlayBackground: 'url(<?php echo get_template_directory_uri(); ?>/images/pop-up-overlay.png) repeat',
				overlayOpacity: 1
			});

			MomentCron.init({
				jobs:{
					'popVagas':{
						timeGap: '1 day',
						toExec: function(){
							setTimeout(function(){
								$('.tgl-target').trigger('click');
							}, 800);
						}
					}
				}
			});
		});
	</script>

<?php endif ?>

<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//v2.zopim.com/?2p9iMivJMTWrOoQetHo8vjh61TB5dWWL';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->

</body>

</html>