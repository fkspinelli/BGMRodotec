<?php get_header(); ?>
<section id="busca">
	<div id="headerTxt">
		<article>
			<header>
				<h1>Resultado da <span>busca</span></h1>
				<?php wp_pagenavi(); ?>
			</header>
			<ul>

				<?php query_posts( 'post_type[]=post' ); ?>

				<?php while ( have_posts() ) : the_post(); ?>
					<li>
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
							<?php the_title(); ?><br>
							<span><?php the_field('subtitulo'); ?></span>
						</a>
						<p><?php echo texto_limite(get_field('texto'),300); ?></p>
					</li>
				<?php endwhile; ?>
			</ul>
			<?php wp_pagenavi(); ?>
		</article>
		<div class="clear"></div>
	</div>	
</section>
<?php get_footer(); ?>