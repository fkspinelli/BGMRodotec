<section id="depoimentos">
	<header>
		<h1>DEPOIMENTOS</h1>
	</header>
	<div class="news">
		<?php
			$blc = 1;
			$i = 1;
			$contador_geral = 1;
			$num_registros = 5;
			$depoimentos = get_field('depoimentos');
			$result = array_reverse($depoimentos);
			foreach($result as $depoimentos){
				if ($i == 1) {
					echo '<div class="paginacao" data-bloco="'.$blc.'">';
				}
		?>
		<article>
			<div class="trava">
				<div class="content">
					<figure>
                        <img src="<?php echo $depoimentos['logo_da_empresa']; ?>" alt="">
                    </figure>
                    <div class="text">
						<h2><?php echo $depoimentos['subtitulo']; ?></h2>
						<p><?php echo $depoimentos['texto']; ?></p>
						<p class="assina">
							<span><?php echo $depoimentos['titulo']; ?></span>
							<?php echo $depoimentos['cargo_profissional']; ?>
						</p>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</article>
		<?php
				if ($i == $num_registros) {
					echo "</div>";
					$i = 1;
					$blc++;
				}
				elseif  ($total == $contador_geral && $i < $num_registros) {
					echo "</div>";
				}
				elseif ($total != $contador_geral && $i < $num_registros) {
					$i++;
				}

				$contador_geral++;
			}
		?>
	</div>

</section>
<section id="contato">
	<article>
		<p class="tel">
			tire suas dúvidas pelo telefone
			<a href="tel:<?php the_field('telefone', 'option'); ?>" title="Ligar">
				<?php the_field('telefone', 'option'); ?>
			</a>
		</p>
		<p class="mail">
			Se preferir, envie email por aqui
			<a href="mailto:<?php the_field('email_de_contato', 'option'); ?>" title="ENVIAR CONTATO">
				ENVIAR CONTATO
			</a>
		</p>
	</article>
	<hr>
	<ul class="social">
		<li><a href="<?php the_field('link_facebook', 'option'); ?>" title="Facebook" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/bot_facebook.jpg" alt="ícone facebook"></a></li>
		<li><a href="<?php the_field('link_twitter', 'option'); ?>" title="Twitter" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/bot_twitter.jpg" alt="ícone twitter"></a></li>
		<li><a href="<?php the_field('link_youtube', 'option'); ?>" title="Youtube" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/bot_youtube.jpg" alt="ícone youtube"></a></li>
	</ul>
	<hr>
	<div class="copy">

		<p class="ligue">Ligue: <span><?php the_field('telefone', 'option'); ?></span></p>

        <p class="ligue">Unidades: <span><?php the_field('telefone_unidades', 'option'); ?></span></p>

		<p>Copyright © 2013 BgmRodotec Ltda. Todos os direitos reservados.</p>
	</div>
</section>