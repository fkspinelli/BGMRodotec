<?php get_header(); ?>
<?php 
	while ( have_posts() ) : the_post();
		if(urlPage2() != "globus"){
			switch(urlPage()){
				default:
					get_template_part( 'content', 'home' );
				break;
				case "empresa":
					get_template_part( 'content', 'empresa' );
				break;
				case "globus":
					get_template_part( 'content', 'globus' );
				break;
				case "passageiros":
					get_template_part( 'content', 'globus_children' );
				break;
				case "logistica":
					get_template_part( 'content', 'globus_children' );
				break;
				case "globus-inteligence":
					get_template_part( 'content', 'intelligence' );
				break;
				case "consultoria":
					get_template_part( 'content', 'consultoria' );
				break;
				case "clientes":
					get_template_part( 'content', 'clientes' );
				break;
				case "depoimentos":
					get_template_part( 'content', 'depoimentos' );
				break;
				case "noticias":
					get_template_part( 'content', 'noticias' );
				break;
				case "contato":
					get_template_part( 'content', 'contato' );
				break;
			}
		}
	endwhile;
?>
<?php get_footer(); ?>