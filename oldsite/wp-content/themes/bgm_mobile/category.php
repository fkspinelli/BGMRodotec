<?php get_header(); ?>
<?php
	if(urlPage() == 1 || urlPage() == "noticias"){
		$section = "noticias";
	} else {
		$section = "noticias";
	}
?>
<section id="<?php echo $section; ?>">
	<header>
		<h1><?php printf( __( '%s', 'twentythirteen' ), single_cat_title( '', false ) ); ?></h1>
	</header>
	<div class="news">
		<?php $i = 1; ?>
		<?php while ( have_posts() ) : the_post(); ?>
			<?php
				if ($i == 2) {
					echo '<div class="cinza"><div class="wrap">';
				}

				get_template_part( 'content', 'noticias' );

				if ($i == 3) {
					echo '</div><div class="clear"></div></div>';
				}
				$i++;
			?>
		<?php endwhile; ?>
	</div>
	<div class="more">
		<?php twentythirteen_paging_nav(); ?>
	</div>
</section>
<section id="contato">
	<article>
		<p class="tel">
			tire suas dúvidas pelo telefone
			<a href="tel:<?php the_field('telefone', 'option'); ?>" title="Ligar">
				<?php the_field('telefone', 'option'); ?>
			</a>
		</p>
		<p class="mail">
			Se preferir, envie email por aqui
			<a href="mailto:<?php the_field('email_de_contato', 'option'); ?>" title="ENVIAR CONTATO">
				ENVIAR CONTATO
			</a>
		</p>
	</article>
	<hr>
	<ul class="social">
		<li><a href="<?php the_field('link_facebook', 'option'); ?>" title="Facebook" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/bot_facebook.jpg" alt="ícone facebook"></a></li>
		<li><a href="<?php the_field('link_twitter', 'option'); ?>" title="Twitter" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/bot_twitter.jpg" alt="ícone twitter"></a></li>
		<li><a href="<?php the_field('link_youtube', 'option'); ?>" title="Youtube" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/bot_youtube.jpg" alt="ícone youtube"></a></li>
	</ul>
	<hr>
	<div class="copy">

		<p class="ligue">Ligue: <span><?php the_field('telefone', 'option'); ?></span></p>

        <p class="ligue">Unidades: <span><?php the_field('telefone_unidades', 'option'); ?></span></p>

		<p>Copyright © 2013 BgmRodotec Ltda. Todos os direitos reservados.</p>
	</div>
</section>
<?php get_footer(); ?>