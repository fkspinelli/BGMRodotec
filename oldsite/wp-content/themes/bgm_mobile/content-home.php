<section id="home">
	<figure>
		<img src="<?php the_field('imagem_topo'); ?>" alt="<?php the_field('titulo_mobile'); ?>">
	</figure>
	<article>
		<h1><?php the_field('titulo_mobile'); ?></h1>
		<p><?php the_field('texto_mobile'); ?></p>
		<a href="<?php echo esc_url( home_url( '/empresa' ) ); ?>" title="saiba mais sobre a empresa">saiba mais sobre a empresa</a>
	</article>
</section>
<hr>
<section id="softwareGlobus">
	<article>
		<a href="<?php echo esc_url( home_url( '/passageiros' ) ); ?>" title="Passageiros">
			<img src="<?php echo get_template_directory_uri(); ?>/images/img_passageiros.jpg" alt="#">
		</a>
		<p>conheça as soluções e os benefícios para <a href="<?php echo esc_url( home_url( '/passageiros' ) ); ?>" title="Passageiros">passageiros</a></p>
	</article>
	<article>
		<a href="<?php echo esc_url( home_url( '/logistica' ) ); ?>" title="Passageiros">
			<img src="<?php echo get_template_directory_uri(); ?>/images/img_logistica.jpg" alt="#">
		</a>
		<p>conheça as soluções e os benefícios para <a href="<?php echo esc_url( home_url( '/logistica' ) ); ?>" title="logística">logística</a></p>
	</article>
</section>
<hr>
<section id="contato">
	<article>
		<p class="tel">
			tire suas dúvidas pelo telefone
			<a href="tel:<?php the_field('telefone', 'option'); ?>" title="Ligar">
				<?php the_field('telefone', 'option'); ?>
			</a>
		</p>
		<p class="mail">
			Se preferir, envie email por aqui
			<a href="mailto:<?php the_field('email_de_contato', 'option'); ?>" title="ENVIAR CONTATO">
				ENVIAR CONTATO
			</a>
		</p>
	</article>
	<hr>
	<ul class="social">
		<li><a href="<?php the_field('link_facebook', 'option'); ?>" title="Facebook" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/bot_facebook.jpg" alt="ícone facebook"></a></li>
		<li><a href="<?php the_field('link_twitter', 'option'); ?>" title="Twitter" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/bot_twitter.jpg" alt="ícone twitter"></a></li>
		<li><a href="<?php the_field('link_youtube', 'option'); ?>" title="Youtube" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/bot_youtube.jpg" alt="ícone youtube"></a></li>
	</ul>
	<hr>
	<div class="copy">

		<p class="ligue">Ligue: <span><?php the_field('telefone', 'option'); ?></span></p>
            
        <p class="ligue">Unidades: <span><?php the_field('telefone_unidades', 'option'); ?></span></p>

		<p>Copyright © 2013 BgmRodotec Ltda. Todos os direitos reservados.<br /></p>
        
	</div>
</section>