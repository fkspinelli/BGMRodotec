hc.menu = function(){
	
	// MENU
	
	var menuOpen = $('body > header nav.menu h3 a');
	var menuLink = $('body > header nav.menu > div > ul > li.menu-item-191 > a');
	var menu = $('body > header nav.menu > div');
	var subMenu = $('body > header nav.menu > div > ul > li > ul');
	var statusMenu = 0;
	var statusSubMenu = 0;
	
	var altura = menu.find('> ul').height();
	
	menuLink.addClass('plus');
	
	menuOpen.click(function(){
		if(statusMenu == 0){
			$(this).removeClass('down').addClass('up');
			menu.stop().animate({height:altura, opacity:1},300);
			statusMenu = 1;
		} else {
			$(this).removeClass('up').addClass('down');
			menu.stop().animate({height:0, opacity:0},300);
			subMenu.stop().animate({height:0, opacity:0},300);
			menuLink.removeClass('less').addClass('plus');
			statusSubMenu = 0;
			statusMenu = 0;
		}
		return false;
	});
	
	menuLink.click(function(){
		if(statusSubMenu == 0){
			$(this).removeClass('plus').addClass('less');
			menu.stop().animate({height:'+='+210, opacity:1},300);
			subMenu.stop().animate({height:210, opacity:1},300);
			statusSubMenu = 1;
		} else {
			$(this).removeClass('less').addClass('plus');
			subMenu.stop().animate({height:0, opacity:0},300);
			menu.stop().animate({height:'-='+210, opacity:1},300);
			statusSubMenu = 0;
		}
		return false;
	});
	
}