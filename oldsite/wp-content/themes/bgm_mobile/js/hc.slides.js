hc.webdoor = function(){
	
	// WEBDOOR HOME
	
	function webdoor(objeto,tipo){
		
		var posicao = 1;
		
		var ul = objeto.find('ul');
		var li = ul.find('li');
		var li_first_child = ul.find('li:first-child');
		var count_atual = objeto.find('div.controles span.atual');
		var count_total = objeto.find('div.controles span.total');
		var controles = objeto.find('div.controles');
		
		li_first_child.addClass('ativo');
		li.each(function(){
			$(this).attr('data-posicao',posicao);
			posicao++;	
		});
		
		var atual = 1;
		
		count_total.text(posicao - 1);
		count_atual.text('1');
		
		function slide(controle){
			atual = ul.find('li.ativo').attr('data-posicao');
			if(controle == 'proximo'){
				var proximo = parseInt(atual) + 1;
				count_atual.text(proximo);
				if(atual == posicao - 1){
					ul.find('li[data-posicao="'+atual+'"]').stop().animate({opacity:0},1000,function(){
						$(this).css({'z-index':0,});
						$(this).removeClass();
					});
					ul.find('li[data-posicao="1"]').stop().animate({opacity:1},1000,function(){
						$(this).css({'z-index':1,});
						$(this).addClass('ativo');
						if(tipo == 1){
							tempo();
						}
					});
					count_atual.text('1');
				} else {
					ul.find('li[data-posicao="'+atual+'"]').stop().animate({opacity:0},1000,function(){
						$(this).css({'z-index':0,});
						$(this).removeClass();
					});
					ul.find('li[data-posicao="'+proximo+'"]').stop().animate({opacity:1},1000,function(){
						$(this).css({'z-index':1,});
						$(this).addClass('ativo')
						if(tipo == 1){
							tempo();
						}
					});
				}
			} else if(controle == 'anterior') {
				if(atual == 1){
					var proximo = parseInt(posicao) - 1;
					count_atual.text(proximo);
					ul.find('li[data-posicao="1"]').stop().animate({opacity:0},1000,function(){
						$(this).css({'z-index':0,});
						$(this).removeClass();
					});
					ul.find('li[data-posicao="'+proximo+'"]').stop().animate({opacity:1},1000,function(){
						$(this).css({'z-index':1,});
						$(this).addClass('ativo');
						if(tipo == 1){
							tempo();
						}
					});
				} else {
					var proximo = parseInt(atual) - 1;
					count_atual.text(proximo);
					ul.find('li[data-posicao="'+atual+'"]').stop().animate({opacity:0},1000,function(){
						$(this).css({'z-index':0,});
						$(this).removeClass();
					});
					ul.find('li[data-posicao="'+proximo+'"]').stop().animate({opacity:1},1000,function(){
						$(this).css({'z-index':1,});
						$(this).addClass('ativo')
						if(tipo == 1){
							tempo();
						}
					});
				}
			} else {
				var proximo = parseInt(atual) + 1;
				count_atual.text(proximo);
				if(atual == posicao - 1){
					count_atual.text('1');
					ul.find('li[data-posicao="'+atual+'"]').stop().animate({opacity:0},1000,function(){
						$(this).css({'z-index':0,});
						$(this).removeClass();
					});
					ul.find('li[data-posicao="1"]').stop().animate({opacity:1},1000,function(){
						$(this).css({'z-index':1,});
						$(this).addClass('ativo')
						if(tipo == 1){
							tempo();
						}
					});
				} else {
					ul.find('li[data-posicao="'+atual+'"]').stop().animate({opacity:0},1000,function(){
						$(this).css({'z-index':0,});
						$(this).removeClass();
					});
					ul.find('li[data-posicao="'+proximo+'"]').stop().animate({opacity:1},1000,function(){
						$(this).css({'z-index':1,});
						$(this).addClass('ativo')
						if(tipo == 1){
							tempo();
						}
					});
				}	
			}
		}
		
		var timer;
		
		if(tipo == 1){
			function tempo(){
				timer = setTimeout(function() {
					slide();
				},10000)
			}
			
			tempo();
		}
		
		controles.find('a.proximo').click(function(){
			window.clearTimeout(timer);
			slide('proximo');
			return false;
		});
		
		controles.find('a.anterior').click(function(){
			window.clearTimeout(timer);
			slide('anterior');
			return false;
		});
	}
	
	$('section#webdoor').each(function(){
		var tipo = $(this).attr('data-tipo');
		webdoor($(this),tipo);
	});
	
}

hc.slides = function(){
	
	/* SLIDE */
	
	var slide = $('div.slide');
	var slide_scroll = slide.find('div.scroll');
	var slide_scroll_ul = slide_scroll.find('ul');
	var slide_scroll_li = slide_scroll_ul.find('li');
	var slide_scroll_li_a = slide_scroll_li.find('a')
	
	var controles = slide.find('div.controles');
	
	var sizeLi = 0;
	
	 $('div.slide div.scroll ul li').each(function(){
		sizeLi += $(this).width();
	});
	
	slide_scroll_ul.width(sizeLi);
	
	var anda = slide_scroll.width();
	
	controles.find('a.anterior').click(function(){
		slide_scroll.stop().animate({scrollLeft:'-='+anda},1200);
		return false;
	});
	
	controles.find('a.proximo').click(function(){
		slide_scroll.stop().animate({scrollLeft:'+='+anda},1200);
		return false;
	});
	
}