<section id="empresa">
	<figure>
		<img src="<?php the_field('topo_mobile'); ?>" alt="<?php the_field('titulo'); ?>">
	</figure>
	<div class="wrap">
		<article>
			<?php the_field('texto'); ?>
		</article>
		<article class="valores">
			<h1>nossa <span>missão</span></h1>
			<?php the_field('texto_de_missão'); ?>
		</article>
		<article class="valores">
			<h1>nossa <span>visão</span></h1>
			<?php the_field('texto_de_visao'); ?>
		</article>
		<article class="valores">
			<h1>nossos <span>valores</span></h1>
			<?php the_field('texto_de_valores'); ?>
		</article>
	</div>
	<article class="relacionamentos">
		<h1><?php the_field('titulo_de_relacionamentos'); ?></h1>
		<p><?php the_field('texto_de_relacionamentos'); ?></p>
	</article>
	<article class="metodologia">
		<h1><?php the_field('titulo_de_metodologia'); ?></h1>
		<p><?php the_field('texto_de_metodologia'); ?></p>
	</article>
	<article class="solucoes">
		<div class="content">
			<header>
				<h1><?php the_field('titulo_de_solucoes'); ?></h1>
				<h2><?php the_field('subtitulo_de_solucoes'); ?></h2>
			</header>
			<p><?php the_field('texto_de_solucoes'); ?></p>
		</div>
		<figure>
			<img src="<?php the_field('imagem_de_solucoes'); ?>" alt="#">
		</figure>
	</article>
</section>
<section id="contato">
	<article>
		<p class="tel">
			tire suas dúvidas pelo telefone
			<a href="tel:<?php the_field('telefone', 'option'); ?>" title="Ligar">
				<?php the_field('telefone', 'option'); ?>
			</a>
		</p>
		<p class="mail">
			Se preferir, envie email por aqui
			<a href="mailto:<?php the_field('email_de_contato', 'option'); ?>" title="ENVIAR CONTATO">
				ENVIAR CONTATO
			</a>
		</p>
	</article>
	<hr>
	<ul class="social">
		<li><a href="<?php the_field('link_facebook', 'option'); ?>" title="Facebook" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/bot_facebook.jpg" alt="ícone facebook"></a></li>
		<li><a href="<?php the_field('link_twitter', 'option'); ?>" title="Twitter" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/bot_twitter.jpg" alt="ícone twitter"></a></li>
		<li><a href="<?php the_field('link_youtube', 'option'); ?>" title="Youtube" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/bot_youtube.jpg" alt="ícone youtube"></a></li>
	</ul>
	<hr>
	<div class="copy">

		<p class="ligue">Ligue: <span><?php the_field('telefone', 'option'); ?></span></p>
            
        <p class="ligue">Unidades: <span><?php the_field('telefone_unidades', 'option'); ?></span></p>

		<p>Copyright © 2013 BgmRodotec Ltda. Todos os direitos reservados.</p>
	</div>
</section>