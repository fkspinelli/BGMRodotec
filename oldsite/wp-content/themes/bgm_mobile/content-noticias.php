<article>
	<div class="trava">
		<div class="content">
			<figure>
				<img src="<?php the_field('imagem_de_destaque'); ?>" alt="#">
				<figcaption><?php the_field('legenda_da_imagem'); ?></figcaption>
			</figure>
			<header>
				<h2><?php the_title(); ?></h2>
				<time datetime=""><?php echo get_the_date(); ?></time>
			</header>
			<div class="text">
				<p>
					<?php the_field('resumo_do_texto'); ?>
				</p>
			</div>
			<a href="<?php the_permalink(); ?>" title="leia a notícia completa">leia a notícia completa</a>
		</div>
	</div>
</article>
