<?php get_header(); ?>
<section id="noticiaOpen">
	<header>
		<h1>Notícias</h1>
	</header>
	<div class="news">
		<article>
			<div class="trava">
				<div class="content">
					<figure>
						<img src="<?php the_field('imagem_de_destaque'); ?>" alt="#">
						<figcaption><?php the_field('legenda_da_imagem'); ?></figcaption>
					</figure>
					<header>
						<h2><?php the_title(); ?></h2>
						<time datetime="<?php echo get_the_date(); ?>"><?php echo get_the_date(); ?></time>
					</header>
					<div class="text">
						<?php the_field('texto'); ?>
					</div>
					<div class="share">
						<div class="fb-like" data-href="<?php echo get_permalink(); ?>" data-width="200" data-height="The pixel height of the plugin" data-colorscheme="light" data-layout="button_count" data-action="like" data-show-faces="true" data-send="true"></div>
						<div class="addthis_toolbox addthis_default_style addthis_32x32_style">
							<a class="addthis_button_email"><img src="<?php echo get_template_directory_uri(); ?>/images/bot_email.jpg" alt="#"></a>
							<a class="addthis_button_print"><img src="<?php echo get_template_directory_uri(); ?>/images/bot_print.jpg" alt="#"></a>
						</div>
						<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
						<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=herickcorrea"></script>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</article>
		<?php
			$args = array(
				'cat'           => '2',
				'post__not_in' => array(get_the_ID()),
				'posts_per_page'    => '2',
			);
			query_posts($args);
			while ( have_posts() ) : the_post();
		?>
		<article>
			<div class="trava">
				<div class="content">
					<figure>
					<!--	<img src="<?php the_field('imagem_de_destaque'); ?>" alt="#">
						<figcaption><?php the_field('legenda_da_imagem'); ?></figcaption> -->
					</figure>
					<header>
						<h2><?php the_title(); ?></h2>
						<time datetime="<?php echo get_the_date(); ?>"><?php echo get_the_date(); ?></time>
					</header>
					<div class="text">
						<p>
							<?php the_field('resumo_do_texto'); ?>
						</p>
					</div>
					<a href="<?php the_permalink(); ?>" title="leia a notícia completa">leia a notícia completa</a>
				</div>
			</div>
		</article>
		<?php
			endwhile;
			wp_reset_query();
		?>
	</div>
</section>
<section id="contato">
	<article>
		<p class="tel">
			tire suas dúvidas pelo telefone
			<a href="tel:<?php the_field('telefone', 'option'); ?>" title="Ligar">
				<?php the_field('telefone', 'option'); ?>
			</a>
		</p>
		<p class="mail">
			Se preferir, envie email por aqui
			<a href="mailto:<?php the_field('email_de_contato', 'option'); ?>" title="ENVIAR CONTATO">
				ENVIAR CONTATO
			</a>
		</p>
	</article>
	<hr>
	<ul class="social">
		<li><a href="<?php the_field('link_facebook', 'option'); ?>" title="Facebook" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/bot_facebook.jpg" alt="ícone facebook"></a></li>
		<li><a href="<?php the_field('link_twitter', 'option'); ?>" title="Twitter" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/bot_twitter.jpg" alt="ícone twitter"></a></li>
		<li><a href="<?php the_field('link_youtube', 'option'); ?>" title="Youtube" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/bot_youtube.jpg" alt="ícone youtube"></a></li>
	</ul>
	<hr>
	<div class="copy">

		<p class="ligue">Ligue: <span><?php the_field('telefone', 'option'); ?></span></p>

        <p class="ligue">Unidades: <span><?php the_field('telefone_unidades', 'option'); ?></span></p>

		<p>Copyright © 2013 BgmRodotec Ltda. Todos os direitos reservados.</p>
	</div>
</section>
<?php get_footer(); ?>