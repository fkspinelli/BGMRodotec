<?php get_header("internal"); ?>
    <section class="blog">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1>Blog</h1>

                </div>
                <div class="col-sm-8">
                 <div class="scroll"> 
                <?php while(have_posts()): the_post(); ?>
                    <div class="box-blog">
                        <div class="box-header" style="background-image:url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(),'normal')[0]; ?>);">
                            <div class="date">
                                <p><?php echo get_the_date('d'); ?></p>
                                <p><?php echo get_the_date('M'); ?></p>
                            </div>
                        </div>
                        <div class="box-body">
                            <h2><?php the_title(); ?></h2>
                            <p><?php echo mb_strtoupper(get_the_date()); ?> | assunto <a href="<?php echo getMainTerm('true'); ?>"><?php echo getMainTerm(); ?></a></p>
                        </div>
                        <div class="box-footer">
                            <p><?php the_excerpt(); ?></p>
                            <div class="row">
                                <div class="col-sm-12">
                                    <a href="<?php the_permalink(); ?>" class="btn btn-danger btn-radios-none text-uppercase text-semi-bold pull-left">ler mais</a>
                                    <div class="social-networks pull-right">
                                        <div>
                                               <a onclick="javascript:window.open(this.href,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=500,height=300'); return false;" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_the_permalink(); ?>"><i class="fa fa-facebook"></i></a>
                                            <a onclick="javascript:window.open(this.href,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=500,height=300'); return false;" href="https://twitter.com/home?status=<?php echo get_the_title().' - '.get_the_permalink(); ?>"><i class="fa fa-twitter"></i></a>
                                            <!--<a onclick="javascript:window.open(this.href,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=500,height=300'); return false;" data-notes="<?php echo get_the_permalink(); ?>" href="https://embed.tumblr.com/share"><i class="fa fa-tumblr"></i></a>-->
                                            <a onclick="javascript:window.open(this.href,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=500,height=300'); return false;" href="https://plus.google.com/share?url=<?php echo get_the_permalink(); ?>"><i class="fa fa-google-plus"></i></a>
                                            <a onclick="javascript:window.open(this.href,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=500,height=300'); return false;" href="whatsapp://send?text=<?php echo get_the_title().' - '.get_the_permalink(); ?>"><i class="fa fa-whatsapp"></i></a>
                                            <!--<a href="#" class="share"><i class="fa fa-share-alt"></i></a>-->
                                            <script>!function(d,s,id){var js,ajs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://assets.tumblr.com/share-button.js";ajs.parentNode.insertBefore(js,ajs);}}(document, "script", "tumblr-js");</script>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; ?>
                    <div class="row">
                            <?php $next_link = get_next_posts_link(); if($next_link): ?>

                            <div class="col-sm-12 pagination">
                                <a href="<?php echo get_next_posts_page_link() ?>" class="btn btn-danger btn-block text-uppercase ver-mais next-page">Ver mais</a>
                            </div>
                        <?php endif; ?>
                        </div>
                    </div>
                </div>
                <?php get_template_part('includes/content', 'sidebar'); // blog sidebar ?>
            </div>
        </div>
    </section>
<?php get_footer(); ?>