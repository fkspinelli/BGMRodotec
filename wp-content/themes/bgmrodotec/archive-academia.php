<?php get_header(); ?>
<section class="banner banner-interna" style="background-image: url(<?php bloginfo('template_url'); ?>/images/banner/academiaglobus.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-sm-11">
                <div class="text" data-scroll-reveal="move 20px">
                    <h1>CURSOS PRESENCIAIS</h1>
                    <h2>
                        A BgmRodotec promove treinamentos para capacitar os usuários na utilização dos recursos do software Globus.
                    </h2>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="academia-globus">
    <div class="container">
      <!--  <form>
            <div class="row">
                <div class="col-sm-2">
                    <div class="form-group">
                        <select class="form-control" id="sel1">
                            <option selected disabled>LOCAL</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6"></div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <input type="search" class="form-control" placeholder="BUSCAR">
                    </div>
                </div>
            </div>
        </form> -->
        <div class="row">
          <div class="scroll-academia">
            <?php while(have_posts()): the_post(); ?>
            <div class="col-sm-4">
                <div class="modulo">
                    <div class="modulo-header">
                        <div class="banner" style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(),'thumb')[0]; ?>);"></div>
                        <div class="filter"></div>
                    </div>
                    <div class="modulo-body">
                        <h4><?php the_title(); ?></h4>
                        <p><?php the_excerpt(); ?></p>
                        <a href="<?php the_permalink(); ?>" class="btn btn-success text-uppercase">saiba mais</a>
                        
                    </div>
                </div>
            </div>
            <?php endwhile; ?>


            <div class="row">
                <div class="col-sm-8 col-sm-push-2 pagination">
                    <?php
                        $next_link = get_next_posts_link(__('Newer Entries &raquo;'));
                        if ($next_link) {
                    ?>
                    <a href="<?php echo get_next_posts_page_link() ?>" class="btn btn-success btn-block text-uppercase ver-mais next-page-academia">Ver mais</a>
                    <?php } ?>
                </div>
            </div>
          </div>

        </div>
    </div>
</section>
<?php get_template_part('includes/content','newsletter'); //NEWSLETTER ?>
<?php get_footer(); ?>
