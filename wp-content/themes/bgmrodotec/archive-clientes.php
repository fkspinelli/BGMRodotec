<?php get_header(); ?>
<section class="banner banner-interna" style="background-image: url(<?php bloginfo('template_url'); ?>/images/banner/clientes.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-sm-11">
                    <div class="text" data-scroll-reveal="move 20px">
                        <h1>CLIENTES</h1>
                        <h2>
                            Faça parte do nosso time de clientes
                        </h2>
                        <a href="<?php bloginfo('url'); ?>/contato/" class="btn btn-default btn-outline">AGENDE UMA DEMOSTRAÇÃO</a> 
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="clientes">
        <div class="container"> 

            <div class="row">
                <?php $cont = 0; while(have_posts()): the_post(); $cont++; ?>
                <div class="col-sm-4">
                    <div class="box-cliente">
                        <div class="box-image">
                            <p>
                                <?php $img = wp_get_attachment_image_src(get_post_thumbnail_id(),'normal'); ?>

                                <img draggable="false" alt="<?php echo get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true); ?>" src="<?php echo $img[0]; ?>">
                            </p>
                        </div>

                        <a href="#item_<?php echo get_field('testemunho') != '' ? get_field('testemunho')[0] :null; ?>">depoimento</a>
                       

                    </div>
                </div>
                <?php if($cont%3 ==0): echo '</div><div class="row">'; endif; endwhile; wp_reset_query(); ?>
            </div>
          
        </div>
    </section>

    <section class="depoimentos-clientes">
        <div id="depoimentos-clientes" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">

            <?php query_posts(['post_type'=>'testemunhos']); $cont_test = 0; while(have_posts()): the_post(); $cont_test++; ?>
                <div id="item_<?php echo $post->ID; ?>" class="item <?php echo $cont_test == '1' ? 'active' : null; ?>">
 
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4><?php the_title(); ?></h4>
                               <?php the_content(); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                
                                <img draggable="false" src="<?php echo get_field('imagem_empresa')['sizes']['thumbnail']; ?>">
                            </div>
                            <div class="col-xs-9">
                                <h5><?php echo get_field('nome'); ?></h5>
                                <h6><?php echo get_field('info'); ?></h6>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endwhile; wp_reset_query(); ?>
            </div>
            <a class="left carousel-control" href="#depoimentos-clientes" role="button" data-slide="prev">
                <i class="fa fa-angle-left" aria-hidden="true"></i>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#depoimentos-clientes" role="button" data-slide="next">
                <i class="fa fa-angle-right" aria-hidden="true"></i>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </section>

    <?php get_template_part('includes/content','newsletter'); //NEWSLETTER ?>
<?php get_footer(); ?>