<?php get_header(); ?>
    <section class="blog">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1>Blog</h1>

                </div>
                <div class="col-sm-8">
                    <div class="scroll">
                    <?php while(have_posts()): the_post(); ?>
                        <div class="box-blog materia">
                            <div class="box-header" style="background-image:url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(),'normal')[0]; ?>);" title="<?php echo get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true); ?>">
                                <div class="date">
                                    <p><?php echo get_the_date('d'); ?></p>
                                    <p><?php echo get_the_date('M'); ?></p>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="blog-title"><?php the_title(); ?></div>
                                <p><?php echo mb_strtoupper(get_the_date()); ?> 
                                <?php if(getMainTerm() !== false): ?>
                                    | assunto 
                                    <a href="<?php echo getMainTerm('true'); ?>"><?php echo getMainTerm(); ?></a>
                                <?php endif; ?>
                                </p>
                            </div>
                            <div class="box-footer">
                                <p><?php the_excerpt(); ?></p>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <a href="<?php the_permalink(); ?>" class="btn btn-danger btn-radios-none text-uppercase text-semi-bold pull-left">ler mais</a>
                                        <div class="social-networks pull-right">
                                            <div>
                                                <a onclick="javascript:window.open(this.href,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=500,height=300'); return false;" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_the_permalink(); ?>"><i class="fa fa-facebook"></i></a>
                                                <a onclick="javascript:window.open(this.href,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=500,height=300'); return false;" href="https://twitter.com/home?status=<?php echo get_the_title().' - '.get_the_permalink(); ?>"><i class="fa fa-twitter"></i></a>
                                                <!--<a onclick="javascript:window.open(this.href,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=500,height=300'); return false;" data-notes="<?php echo get_the_permalink(); ?>" href="https://embed.tumblr.com/share"><i class="fa fa-tumblr"></i></a>-->
                                                <a onclick="javascript:window.open(this.href,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=500,height=300'); return false;" href="https://plus.google.com/share?url=<?php echo get_the_permalink(); ?>"><i class="fa fa-google-plus"></i></a>
                                                <a onclick="javascript:window.open(this.href,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=500,height=300'); return false;" href="https://www.linkedin.com/cws/share?url=<?php echo get_the_permalink(); ?>"><i class="fa fa-linkedin"></i></a>
                                                <a onclick="javascript:window.open(this.href,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=500,height=300'); return false;" href="whatsapp://send?text=<?php echo get_the_title().' - '.get_the_permalink(); ?>"><i class="fa fa-whatsapp"></i></a>
                                                <!--<a href="#" class="share"><i class="fa fa-share-alt"></i></a>-->
                                                
                                                <script>!function(d,s,id){var js,ajs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://assets.tumblr.com/share-button.js";ajs.parentNode.insertBefore(js,ajs);}}(document, "script", "tumblr-js");</script>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; wp_reset_query(); ?>
                        <div class="row">
                            <div class="col-sm-12 pagination">
                                <a href="<?php echo get_next_posts_page_link() ?>" class="btn btn-danger btn-block text-uppercase ver-mais next-page">Ver mais</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php get_template_part('includes/content', 'sidebar'); // blog sidebar ?>
            </div>
        </div>
    </section>
    <style type="text/css">
    .navbar {
        background-image: url(http://bgmrodotec.com.br/wp-content/themes/bgmrodotec/css/../images/background/header-blog.png);
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
        position: relative;
    }
    </style>
<?php get_footer(); ?>