<?php  
/*
* Template Name: Globus
*
*/
get_header();
the_post();
?>
    <section class="banner banner-interna" style="background-image: url(<?php bloginfo('template_url'); ?>/images/banner/globus.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="text"   data-scroll-reveal="move 20px">
                        <h1><b><?php the_title(); ?></b></h1>
                        <h2>
                            <?php echo get_field('subtitulo'); ?>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="agendar-demonstracao bg-f2f2f2">
        <div class="container">
            <div class="row">
                <div class="row-height">
                    <div class="col-sm-9 col-sm-height col-sm-middle" data-scroll-reveal="enter left and move 20px over 1s wait 0s">
                        <?php the_content(); ?>
                    </div>
                    <div class="col-sm-3 col-sm-height col-sm-middle" data-scroll-reveal="enter right and move 20px over 1s wait 0s">
                        <a href="http://materiais.bgmrodotec.com.br/agendar-demonstracao" target="_blank" class="btn btn-danger btn-block bt-agendar-demonstracao">AGENDAR <br class="hidden-xs"> DEMONSTRAÇÃO</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
<!--     <section class="modulos-poligon-gradient">
        <div class="container">
            <div class="row" data-scroll-reveal="enter">
                <div class="col-xs-4">
                    <a href="<?php bloginfo('url'); ?>/globus/passageiros" class="poligon-gradient">PASSAGEIROS</a>
                </div>
                <div class="col-xs-4">
                    <a href="<?php bloginfo('url'); ?>/globus/carga" class="poligon-gradient">CARGA</a>
                </div>
                <div class="col-xs-4">
                    <a href="<?php bloginfo('url'); ?>/globus/trr" class="poligon-gradient">TRR</a>
                </div>
            </div>
        </div>
    </section> -->
    <section class="modulos-poligon-gradient">
        <div class="container">
            <div class="row" data-scroll-reveal="enter">
                <div class="col-sm-5">
                    <h3>MAIS PRODUTIVIDADE</h3>
                    <div class="check-list">
                        <?php echo get_field('lista_1'); ?>
                    </div>
                </div>
                <div class="col-sm-6 col-sm-push-1">
                    <h3>IMPLANTAÇÃO E SUPORTE</h3>
                    <div class="check-list">
                       <?php echo get_field('lista_2'); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="portal-do-cliente">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <div class="title">portal do <b>cliente</b></div>
                    <div class="subtitle">O seu canal de Relacionamento com a BgmRodotec</div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <img src="<?php bloginfo('template_url'); ?>/images/icon/iMac.png" class="img-responsive" alt="Portal do Cliente">
                </div>
                <div class="col-sm-6">
                    <div class="text">
                        O Portal do Cliente nasceu com o objetivo de relacionamento com os clientes Globus. Nele, é possível fazer solicitações das mais variadas naturezas aos nossos especialistas, sanar dúvidas, manter-se atualizado quanto as atualizações disponíveis do software, participar de pesquisas de opinião e enquetes, ter acesso a biblioteca de publicações BgmRodotec e não para por aí.
                        <br><br>
                        Estamos sempre trabalhando de forma que a sua experiência Globus seja a mais simples e produtiva.
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="solucoes-integradas-detalhes" class="solucoes-integradas-detalhes">
        <div class="container">
            <div class="row">
                <div class="col-sm-12" data-scroll-reveal="enter bottom and move 20px over 1s wait 0s">
                    <div class="title">soluções <b>integradas</b></div>
                    <div class="subtitle">O sistema possui um módulo específico para cada área de atividade da empresa, integrado aos demais. Eles atendem amplamente às necessidades operacionais, logísticas, financeiras, fiscais, comerciais e administrativas das empresas de transportes</div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row btns">
                <div class="col-sm-4">
                    <a href="#" class="operacinal"><span>operacional</span> <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
                <div class="col-sm-4">
                    <a href="#" class="manutencao-de-frota"><span>manutenção de frota</span> <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
                <div class="col-sm-4">
                    <a href="#" class="administrativo-e-financeiro"><span>administrativo <br> e financeiro</span> <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
        <div class="box-content">
            <div class="box operacinal passageiros" data-group="operacinal">
                <div class="text">
                    <span class="box-icon"></span>
                    <span><a href="<?php bloginfo('url'); ?>/globus/passageiros/">passageiros</a></span>
                </div>
                <div class="hover">
                    <p>
                        <a href="<?php bloginfo('url'); ?>/globus/passageiros#urbano">Urbano</a> | 
                        <a href="<?php bloginfo('url'); ?>/globus/passageiros#rodoviario">Rodoviário</a> | 
                        <a href="<?php bloginfo('url'); ?>/globus/passageiros#fretamento">Fretamento</a>
                    </p>
                </div>
            </div>
            <div class="box operacinal cargas-e-encomendas" data-group="operacinal">
                <div class="text">
                    <span class="box-icon"></span>
                    <span><a href="<?php bloginfo('url'); ?>/globus/carga/">cargas e encomendas</a></span>
                </div>
                <div class="hover">
                    <p>
                        <a href="<?php bloginfo('url'); ?>/globus/carga#carga">Operacional</a> 
                    </p>
                </div>
            </div>
            <div class="box operacinal trr" data-group="operacinal">
                <div class="text">
                    <span class="box-icon"></span>
                    <span><a href="<?php bloginfo('url'); ?>/globus/trr/">trr</a></span>
                </div>
                <div class="hover">
                    <p>
                        <a href="<?php bloginfo('url'); ?>/globus/trr#trr">Operacional</a>
                    </p>
                </div>
            </div>
            <div class="box administrativo-e-financeiro controle" data-group="administrativo-e-financeiro">
                <div class="text">
                    <span class="box-icon"></span>
                    <span><a href="<?php bloginfo('url'); ?>/globus/controle/">Controle</a></span>
                </div>
                <div class="hover">
                    <p>
                    <a href="<?php bloginfo('url'); ?>/globus/controle/">Funcionalidades</a>
                    </p>
                </div>
            </div>
            <div class="box manutencao-de-frota oficinas-e-materiais" data-group="manutencao-de-frota">
                <div class="text">
                    <span class="box-icon"></span>
                    <span><a href="<?php bloginfo('url'); ?>/globus/oficinas-e-materiais/">oficinas e materiais</a></span>
                </div>
                <div class="hover">
                    <p>
                        <a href="<?php bloginfo('url'); ?>/globus/oficinas-e-materiais/">Frota</a> | 
                        <a href="<?php bloginfo('url'); ?>/globus/oficinas-e-materiais/">Abastecimento</a> | 
                        <a href="<?php bloginfo('url'); ?>/globus/oficinas-e-materiais/">Componentes</a> | 
                        <a href="<?php bloginfo('url'); ?>/globus/oficinas-e-materiais/">Motor</a> | 
                        <a href="<?php bloginfo('url'); ?>/globus/oficinas-e-materiais/">Manutenção</a> | 
                        <a href="<?php bloginfo('url'); ?>/globus/oficinas-e-materiais/">Pneus</a> | 
                        <a href="<?php bloginfo('url'); ?>/globus/oficinas-e-materiais/">Compras</a> | 
                        <a href="<?php bloginfo('url'); ?>/globus/oficinas-e-materiais/">Estoque</a>
                    </p>
                </div>
            </div>
            <div class="box manutencao-de-frota apoio" data-group="manutencao-de-frota">
                <div class="text">
                    <span class="box-icon"></span>
                    <span><a href="<?php bloginfo('url'); ?>/globus/apoio/">apoio</a></span>
                </div>
                <div class="hover">
                    <p>
                        <a href="<?php bloginfo('url'); ?>/globus/apoio/">Doc. de Veículos e Seguros</a> |
                        <a href="<?php bloginfo('url'); ?>/globus/apoio/">Acidentes</a> |
                        <a href="<?php bloginfo('url'); ?>/globus/apoio/">Terminal de Consulta</a> |
                        <a href="<?php bloginfo('url'); ?>/globus/apoio/">GED – Gestão Eletrônica de Documentos</a>
                    </p>
                </div>
            </div>
            <div class="box administrativo-e-financeiro pessoal" data-group="administrativo-e-financeiro">
                <div class="text">
                    <span class="box-icon"></span>
                    <span><a href="<?php bloginfo('url'); ?>/globus/pessoal/">pessoal</a></span>
                </div>
                <div class="hover">
                    <p>
                        <a href="<?php bloginfo('url'); ?>/globus/pessoal/">Folha de pagamento</a> |
                        <a href="<?php bloginfo('url'); ?>/globus/pessoal/">Frequência</a> |
                        <a href="<?php bloginfo('url'); ?>/globus/pessoal/">Recursos Humanos</a>
                    </p>
                </div>
            </div>
            <div class="box administrativo-e-financeiro financeiro" data-group="administrativo-e-financeiro">
                <div class="text">
                    <span class="box-icon"></span>
                    <span><a href="<?php bloginfo('url'); ?>/globus/financeiro/">financeiro</a></span>
                </div>
                <div class="hover">
                    <p>
                        <a href="<?php bloginfo('url'); ?>/globus/financeiro/">Contas a pagar</a> |
                        <a href="<?php bloginfo('url'); ?>/globus/financeiro/">Contas a receber</a> |
                        <a href="<?php bloginfo('url'); ?>/globus/financeiro/">Controle Bancário</a>
                    </p>
                </div>
            </div>
            <div class="box administrativo-e-financeiro contabil-fiscal" data-group="administrativo-e-financeiro">
                <div class="text">
                    <span class="box-icon"></span>
                    <span><a href="<?php bloginfo('url'); ?>/globus/contabil/">contabil fiscal</a></span>
                </div>
                <div class="hover">
                    <p>
                        <a href="<?php bloginfo('url'); ?>/globus/contabil/">Ativo Imobilizado</a> |
                        <a href="<?php bloginfo('url'); ?>/globus/contabil/">Contabilidade</a> |
                        <a href="<?php bloginfo('url'); ?>/globus/contabil/">Escrituração fiscal </a>|
                        <a href="<?php bloginfo('url'); ?>/globus/contabil/">Sped fiscal e contábil</a>
                    </p>
                </div>
            </div>
        </div>
    </section>
<?php 
    get_template_part('includes/content', 'mosaico'); // MOSAICO
    
    get_template_part('includes/content','newsletter'); //NEWSLETTER 
    
    get_footer(); 
?>