<?php  
/*
* Template Name: Empresa
*
*/
get_header();
the_post();
?>
 <section class="banner banner-interna" style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(),'full')[0] ?>);">
        <div class="container">
            <div class="row">
                <div class="col-sm-11">
                    <div class="text" data-scroll-reveal="move 20px">
                        <h1><b>A Empresa</b></h2>
                        <h2><?php the_content(); ?></h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="numeros">
        <div class="container">
            <div class="row">
                <div class="col-xs-3">
                    <!-- <div class="box-numero box-numero-a" data-scroll-reveal="move 25px wait 0.4s">
                        <h4><?php echo get_field('bloco_numero_1'); ?></h4>
                        <h5>anos no mercado</h5>
                    </div> -->
                    <div class="box-numero box-numero-b gray" data-scroll-reveal="move 25px wait 0.4s">
                        <h5>mais de</h5>
                        <h4><?php echo get_field('bloco_numero_1'); ?></h4>
                        <h5>de mercado</h5>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="box-numero box-numero-b red" data-scroll-reveal="move 25px wait 0.6s">
                        <h5>equipe de</h5>
                        <h4><?php echo get_field('bloco_numero_2'); ?></h4>
                        <h5>colaboradores</h5>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="box-numero box-numero-b gray" data-scroll-reveal="move 25px wait 0.8s">
                        <h5>mais de</h5>
                        <h4><?php echo get_field('bloco_numero_3'); ?></h4>
                        <h5>clientes</h5>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="box-numero box-numero-b red" data-scroll-reveal="move 25px wait 1s">
                        <h5>comunidade de</h5>
                        <h4><?php echo get_field('bloco_numero_4'); ?></h4>
                        <h5>USUÁRIOS</h5>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="presenca-territorio-nacional">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2>presença em <strong>todo</strong> território nacional</h2>
                </div>
            </div>

             <div class="row">
                <div class="col-sm-12">
                    <div class="pull-right">
                        <div class="box-numero box-numero-a" data-scroll-reveal="enter right and move 40px over 1s">
                            <h4><?php echo get_field('bloco_numero_5'); ?></h4>
                            <h5>unidades <br>&nbsp;</h5>
                        </div>
                        <div class="box-numero box-numero-b" data-scroll-reveal="enter right and move 30px over 1s wait 0.3s">
                            <h4><?php echo get_field('bloco_numero_6'); ?></h4>
                            <h5>representantes <br>&nbsp;</h5>
                        </div>
                        <div class="box-numero box-numero-a" data-scroll-reveal="enter right and move 40px over 1s">
                            <h4>1</h4>
                            <h5>escritório <br> virtual</h5>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <section class="missao-visao-valores">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <ul>
                        <li class="active" data-scroll-reveal="enter left and move 30px over 1s ">
                            <a data-toggle="pill" href="#home">
                                <p>Missão</p>
                            </a>
                        </li>
                        <li data-scroll-reveal="enter left and move 30px over 1s wait 0.3s">
                            <a data-toggle="pill" href="#menu1">
                                <p>Visão</p>
                            </a>
                        </li>
                        <li data-scroll-reveal="enter left and move 30px over 1s wait 0.6s">
                            <a data-toggle="pill" href="#menu2">
                                <p>Valores</p>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-7 col-sm-push-1">
                    <div class="tab-content" data-scroll-reveal="enter right and move 30px over 1s">
                      <div id="home" class="tab-pane fade in active">
                        <div class="text"><p><?php echo get_field('missao'); ?></p></div>
                      </div>
                      <div id="menu1" class="tab-pane fade">
                        <div class="text"><p><?php echo get_field('visao'); ?></p></div>
                      </div>
                      <div id="menu2" class="tab-pane fade">
                        <div class="text"><p><?php echo get_field('valores'); ?></p></div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="solucoes-integradas">
        <div class="container">
            <div class="row">
                <div class="col-sm-5">
                    <img draggable="false" src="<?php bloginfo('template_url'); ?>/images/icon/globus.png" class="img-responsive" data-scroll-reveal="enter">
                </div>
                <div class="col-sm-7" data-scroll-reveal="enter">
                    <!-- <h4>Soluções</h4> -->
                    <h3>software <br> de gestão
                        <span>com integração total<br>entre as áreas</span>
                    </h3>
                    <a href="<?php bloginfo('url'); ?>/globus/" class="btn btn-default text-uppercase">conheça o globus</a>
                </div>
            </div>
        </div>
    </section>
    <section class="premiacoes-e-certificacoes">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h3><b>premiações e</b> certificações</h3>
                    <div class="check-list">
                        <?php echo get_field('premiacoes_e_certificacoes_1'); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="check-list cl2">
                        <?php echo get_field('premiacoes_e_certificacoes_2'); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="nosso-time">
        <div class="container">
            <div class="row" data-scroll-reveal="enter">
                <div class="col-sm-12">
                    <div class="title">nosso <strong>time</strong></div>
                    <p>Empresas excelentes são formadas por pessoas excelentes. Por acreditar nisso, buscamos os melhores profissionais do mercado para integrar nosso time.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <a href="mailto:diretoriacomercial@bgmrodotec.com.br" class="membro" data-scroll-reveal="enter bottom and move 30px over 1s">
                        <img draggable="false" src="<?php bloginfo('template_url'); ?>/images/photo/lauro-feire.png" alt="Lauro Freire">
                        <h4>Lauro Freire</h4>
                        <p>Diretor</p>
                    </a>
                </div>
                <div class="col-sm-4">
                    <a href="mailto:diretoriacomercial@bgmrodotec.com.br" class="membro" data-scroll-reveal="enter bottom and move 30px over 1s">
                        <img draggable="false" src="<?php bloginfo('template_url'); ?>/images/photo/valmir-colodrao.png" alt="Valmir Colodrão">
                        <h4>Valmir Colodrão</h4>
                        <p>Diretor</p>
                    </a>
                </div>
                <div class="col-sm-4" data-scroll-reveal="enter bottom and move 30px over 1s wait 0.8s">
                    <a href="mailto:diretoriacomercial@bgmrodotec.com.br" class="membro" data-scroll-reveal="enter bottom and move 30px over 1s">
                        <img draggable="false" src="<?php bloginfo('template_url'); ?>/images/photo/edson-caldeira.png" alt="Edson Caldeira">
                        <h4>Edson Caldeira</h4>
                        <p>Diretor</p>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3 col-num" data-scroll-reveal="enter right and move 300px over 1s wait 0.1s">
                    <div class="numero">
                        <img draggable="false" src="<?php bloginfo('template_url'); ?>/images/background/box-gray-3.png">
                        <div class="content">
                            <img draggable="false" src="<?php bloginfo('template_url'); ?>/images/icon/customer-service.png">
                            <h4><?php echo get_field('bloco_numero_7'); ?></h4>
                            <p>colaboradores <br>NO ATENDIMENTO</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 col-num" data-scroll-reveal="enter right and move 300px over 1s wait 0.3s">
                    <div class="numero">
                        <img draggable="false" src="<?php bloginfo('template_url'); ?>/images/background/box-gray-3-outline.png">
                        <div class="content">
                            <img draggable="false" src="<?php bloginfo('template_url'); ?>/images/icon/desktop.png">
                            <h4><?php echo get_field('bloco_numero_8'); ?></h4>
                            <p>colaboradores <br>NA implantação</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 col-num" data-scroll-reveal="enter right and move 300px over 1s wait 0.6s">
                    <div class="numero">
                        <img draggable="false" src="<?php bloginfo('template_url'); ?>/images/background/box-gray-3.png">
                        <div class="content">
                            <img draggable="false" src="<?php bloginfo('template_url'); ?>/images/icon/desktop.png">
                            <h4><?php echo get_field('bloco_numero_9'); ?></h4>
                            <p>colaboradores <br>NO DESENVOLVIMENTO</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 col-num" data-scroll-reveal="enter right and move 300px over 1s wait 0.9s">
                    <div class="numero">
                        <img draggable="false" src="<?php bloginfo('template_url'); ?>/images/background/box-gray-3-outline.png">
                        <div class="content">
                            <img draggable="false" src="<?php bloginfo('template_url'); ?>/images/icon/desktop.png">
                            <h4><?php echo get_field('bloco_numero_10'); ?></h4>
                            <p>colaboradores <br>NO ADMINISTRATIVO</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php get_template_part('includes/content','newsletter'); //NEWSLETTER ?>

<?php get_footer(); ?>