<?php  
/*
* Template Name: Globus Interna
*
*/
get_header();
the_post();
?>
    <section class="banner banner-interna" style="background-image: url(<?php bloginfo('template_url'); ?>/images/banner/globus-passageiros.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="text" data-scroll-reveal="move 20px">
                        <h1><b>GLOBUS</b> <?php echo strtoupper(get_the_title()); ?></h1>
                        <h2>
                            SISTEMA DE GESTÃO COMPLETO PARA <br> EMPRESAS DE TRANSPORTE DE PASSAGEIROS
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="agendar-demonstracao bg-f2f2f2">
        <div class="container">
            <div class="row">
                <div class="row-height">
                    <div class="col-sm-9 col-sm-height col-sm-middle" data-scroll-reveal="enter left and move 20px over 1s wait 0s">
                        <p>Se o seu negócio é transportar pessoas, você precisa conhecer o Globus Passageiros, uma solução especialmente desenvolvida para atender todas as necessidades da sua empresa, seja ele transporte urbano, rodoviário ou fretamento e turismo.  Confira os benefícios desta ferramenta:</p>
                    </div>
                    <div class="col-sm-3 col-sm-height col-sm-middle" data-scroll-reveal="enter right and move 20px over 1s wait 0s">
                        <a href="http://materiais.bgmrodotec.com.br/agendar-demonstracao" target="_blank" class="btn btn-danger btn-block bt-agendar-demonstracao">AGENDAR <br> DEMONSTRAÇÃO</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="box-check-list">
                        <div class="check-list">
                            <h2>fretamento e turismo</h2>
                            <ul>
                                <li>Monitoramento de KM e itinerário;</li>
                                <li>Gestão de clientes e contratos;</li>
                                <li>Controle da escala de serviço dos motoristas;</li>
                                <li>Gerenciamento das vendas de excursão, translados, city tour e viagens;</li>
                                <li>Mantém os cadastros dos clientes com as suas ocorrências e movimentações;</li>
                                <li>Controle das comissões das agências, dos vendedores e dos motoristas.</li>
                            </ul>
                        </div>
                    </div>
                    <div class="box-check-list">
                        <div class="check-list">
                            <h2>urbano</h2>
                            <ul>
                                <li>Controle das escalas dos motoristas e cobradores;</li>
                                <li>Gestão da arrecadação (por linha/ por veículo/ por período);</li>
                                <li>Gestão de Vale Transporte;</li>
                            </ul>
                        </div>
                        <div class="check-list">
                            <hr>
                            <h2>rodoviário</h2>
                            <ul>
                                <li>Integração com o software de bilhetagem eletrônica.</li>
                                <li>Controle das escalas dos motoristas e cobradores;</li>
                                <li>Gestão de vale transporte</li>
                                <li>Gestão de retaguarda.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="solucoes-operacionais s-o-b">
        <div class="container">
            <div class="row">
                <div class="col-sm-12" data-scroll-reveal="enter">
                    <h2>soluções <b>operacionais</b></h2>
                    <p>O sistema possui um módulo específico para cada área de atividade da empresa transportadora.</p>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <h3>urbano</h3>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <ul class="btns">
                            <li class="active"><a data-toggle="pill" href="#tab1">ARRECADAÇÃO</a></li>
                            <li><a data-toggle="pill" href="#tab2">ESCALA URBANA</a></li>
                            <li><a data-toggle="pill" href="#tab3">PLANTÃO</a></li>
                            <li><a data-toggle="pill" href="#tab4">PASSES E VT</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-8">
                        <div class="tab-content">
                            <div id="tab1" class="tab-pane fade in active">
                                <div class="check-list">
                                    <h2>Objetivo do módulo</h2>
                                    <ul>
                                        <li>Controle das escalas dos motoristas e cobradores;</li>
                                        <li>Gestão da arrecadação (por linha/ por veículo/ por período);</li>
                                        <li>Gestão de Vale Transporte;</li>
                                    </ul>
                                    <h2>Benefícios</h2>
                                    <ol>
                                        <li>Controle das escalas dos motoristas e cobradores;</li>
                                        <li>Gestão da arrecadação</li>
                                        <li>Gestão de Vale Transporte;</li>
                                        <li>Gestão da arrecadação</li>
                                        <li>Gestão de Vale Transporte;</li>
                                    </ol>
                                </div>
                            </div>
                            <div id="tab2" class="tab-pane fade">
                                <div class="check-list">
                                    <h2>Tab 2</h2>
                                </div>
                            </div>
                            <div id="tab3" class="tab-pane fade">
                                <div class="check-list">
                                    <h2>Tab 3</h2>
                                </div>
                            </div>
                            <div id="tab4" class="tab-pane fade">
                                <div class="check-list">
                                    <h2>Tab 4</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <h3>freteamento</h3>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <ul class="btns">
                            <li class="active"><a data-toggle="pill" href="#tab5">ARRECADAÇÃO</a></li>
                            <li><a data-toggle="pill" href="#tab6">ESCALA URBANA</a></li>
                            <li><a data-toggle="pill" href="#tab7">PLANTÃO</a></li>
                            <li><a data-toggle="pill" href="#tab8">PASSES E VT</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-8">
                        <div class="tab-content">
                            <div id="tab5" class="tab-pane fade in active">
                                <div class="check-list">
                                    <h2>Objetivo do módulo</h2>
                                    <ul>
                                        <li>Controle das escalas dos motoristas e cobradores;</li>
                                        <li>Gestão da arrecadação (por linha/ por veículo/ por período);</li>
                                        <li>Gestão de Vale Transporte;</li>
                                    </ul>
                                    <h2>Benefícios</h2>
                                    <ol>
                                        <li>Controle das escalas dos motoristas e cobradores;</li>
                                        <li>Gestão da arrecadação</li>
                                        <li>Gestão de Vale Transporte;</li>
                                        <li>Gestão da arrecadação</li>
                                        <li>Gestão de Vale Transporte;</li>
                                    </ol>
                                </div>
                            </div>
                            <div id="tab6" class="tab-pane fade">
                                <div class="check-list">
                                    <h2>Tab 2</h2>
                                </div>
                            </div>
                            <div id="tab7" class="tab-pane fade">
                                <div class="check-list">
                                    <h2>Tab 3</h2>
                                </div>
                            </div>
                            <div id="tab8" class="tab-pane fade">
                                <div class="check-list">
                                    <h2>Tab 4</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-8 col-sm-push-2">
                    <a href="<?php bloginfo('url'); ?>/globus#solucoes-integradas-detalhes" class="btn btn-gradient btn-block text-uppercase">< Voltar para módulos Globus</a>
                </div>
            </div>
        </div>
    </section>

    <section class="mozaico">
        <div class="moz-row">
            <div class="moz" style="background-image: url(<?php bloginfo('template_url'); ?>/images/background/globus_passageiros_moz4.jpg);">
                <div class="moz-text">
                   <div class="content" data-scroll-reveal="move 10px over 1s">
                        <h4>globus</h4>
                        <h5>cloud</h5>
                        <p>QUER ECONOMIZAR COM INFRAESTRUTURA?</p>
                        <p>Conheça a nossa versão na nuvem.</p>
                        <a href="http://materiais.bgmrodotec.com.br/agendar-demonstracao" target="_blank" class="btn btn-default text-uppercase bt-agendar-demonstracao">AGENDAR DEMONSTRAÇÃO</a>
                   </div>
                </div>
            </div>
            <div class="moz moz-text moz-danger">
               <div class="content" data-scroll-reveal="move 10px over 1s">
                    <div class="check-list">
                        <ul>
                            <li>Dispensa estrutura de TI completa, estações potentes e licenciamento de software.</li>
                            <li>Implantação: você pode utilizar todas as funcionalidades do sistema assim que contratar o serviço.</li>
                            <li>Mais segurança e velocidade de processamento.</li>
                        </ul>
                    </div>
               </div>
            </div>
        </div>
        <div class="moz-row">
            <div class="moz moz-text moz-warning">
                <div class="content" data-scroll-reveal="move 10px over 1s">
                    <h4>globus</h4>
                    <h5>mobile</h5>
                    <p>Mais inovação na gestão de sua transportadora. A BgmRodotec dispões de aplicativos para facilitar ainda mais o seu dia.</p>
                    <a href="http://materiais.bgmrodotec.com.br/agendar-demonstracao" target="_blank" class="btn btn-default text-uppercase bt-agendar-demonstracao">AGENDAR DEMONSTRAÇÃO</a>
                    <a href="#" class="btn btn-default btn-outline text-uppercase">saiba mais</a>
                </div>
            </div>
            <div class="moz" style="background-image: url(<?php bloginfo('template_url'); ?>/images/background/globus_passageiros_moz2.jpg);"></div>
        </div>
        <div class="moz-row">
            <div class="moz" style="background-image: url(<?php bloginfo('template_url'); ?>/images/background/globus_passageiros_moz3.jpg);"></div>
            <div class="moz moz-text moz-green">
               <div class="content" data-scroll-reveal="move 10px over 1s">
                    <h4>consultoria</h4>
                    <h5>globus</h5>
                    <p>Estabeleça uma gestão mais ágil e competitiva, em sintonia com a realidade atual.</p>
                    <a href="#" class="btn btn-default text-uppercase">entrar em contato</a>
               </div>
            </div>
        </div>
        <div class="moz-row">
            <div class="moz" style="background-image: url(<?php bloginfo('template_url'); ?>/images/background/globus_parts.jpg);"></div>
            <div class="moz moz-text moz-default">
                <div class="content" data-scroll-reveal="move 10px over 1s">
                    <h4>globus</h4>
                    <h5>parts</h5>
                    <p>O portal de compra e venda pela Internet, para compradores e fornecedores do setor de transportes</p>
                    <a href="#" class="btn btn-purple text-uppercase">acessar plataforma</a>
                </div>
            </div>
        </div>
    </section>
    <?php get_template_part('includes/content','newsletter'); //NEWSLETTER ?>
<?php get_footer(); ?>