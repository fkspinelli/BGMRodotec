<?php  
/*
* Template Name: Home
*
*/
get_header();
?>
<?php get_template_part('includes/content', 'banner'); //BANNER HOME ?>

    <section class="video">
        <section class="bus">
            <div data-stellar-ratio="2" class="bus-left horizontal-parallax-left"></div>
            <div data-stellar-ratio="2" class="bus-right horizontal-parallax-right"></div>
        </section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h3>
                        Como o <br>
                        <b>Globus</b> <br>
                        ajudará a sua empresa
                    </h3>
                    <h4>Com o Globus, diversas rotinas são automatizadas, gerando maior produtividade ao negócio.</h4>
                    <!-- <a href="#" data-toggle="modal" data-target="#videoModal" data-scroll-reveal="move 50px wait 0.3s">
                        <img src="<?php bloginfo('template_url'); ?>/images/video/video.jpg" class="img-responsive">
                    </a> -->
                    <iframe src="https://www.youtube.com/embed/ToRIzkvuFTA" class="iframe" frameborder="0" allowfullscreen style="width:100%; height:515px; margin-bottom: 50px; background: #7b7b7b;"></iframe>
                     <!-- <div id="videoModal" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-lg" style="height:90%;">
                            <iframe src="https://www.youtube.com/embed/ToRIzkvuFTA" frameborder="0" allowfullscreen style="width:100%; height:100%;"></iframe>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </section>
    
    <?php get_template_part('includes/content','noticias'); //noticias ?>
    
    <?php get_template_part('includes/content','depoimentos'); //DEPOIMENTOS ?>

    <?php get_template_part('includes/content','newsletter'); //NEWSLETTER ?>

   
<?php get_footer(); ?>