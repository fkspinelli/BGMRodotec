<?php  
/*
* Template Name: Globus Intelligence
*
*/
get_header();
the_post();
$banner = wp_get_attachment_image_src(get_post_thumbnail_id(),'full');
?>
 <section class="banner banner-interna" style="background-image: url(<?php echo $banner[0]; ?>);">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="text" data-scroll-reveal="move 20px">
                        <h1><b>GLOBUS</b> <?php the_title(); ?></h1>
                        <h2>
                            <?php echo get_field('subtitulo'); ?>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="agendar-demonstracao bg-f2f2f2">
        <div class="container">
            <div class="row">
                <div class="row-height">
                    <div class="col-sm-9 col-sm-height col-sm-middle" data-scroll-reveal="enter left and move 20px over 1s wait 0s">
                      <?php the_content(); ?>
                    </div>
                    <div class="col-sm-3 col-sm-height col-sm-middle" data-scroll-reveal="enter right and move 20px over 1s wait 0s">
                        <a href="http://materiais.bgmrodotec.com.br/agendar-demonstracao" target="_blank" class="btn btn-gold btn-block bt-agendar-demonstracao">AGENDAR <br> DEMONSTRAÇÃO</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php if(get_field('subtexto') != ''): // definindo se há subtexto ?>
    <section class="texto">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                   <?php echo get_field('subtexto'); ?>
                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>
    <section class="texto bg-f2f2f2">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h3>RESULTADOS</h3>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="check-list">
                              <?php echo get_field('lista_1'); ?>
                               
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="check-list">
                                <?php echo get_field('lista_2'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

 <?php 
    get_template_part('includes/content', 'mosaico'); // MOSAICO
    
    get_template_part('includes/content','newsletter'); //NEWSLETTER 
    
    get_footer(); 
?>