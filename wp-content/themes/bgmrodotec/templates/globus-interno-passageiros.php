<?php  
/*
* Template Name: Globus Interna Passageiros
*
*/
get_header();
the_post();
$banner = wp_get_attachment_image_src(get_post_thumbnail_id(),'full');
?>
    <section class="banner banner-interna" style="background-image: url(<?php echo $banner[0]; ?>);">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="text" data-scroll-reveal="move 20px">
                        <h1><b>GLOBUS</b> <?php echo strtoupper(get_the_title()); ?></h1>
                        <h2>
                             <?php echo get_field('subtitulo'); ?>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="agendar-demonstracao bg-f2f2f2">
        <div class="container">
            <div class="row">
                <div class="row-height">
                    <div class="col-sm-9 col-sm-height col-sm-middle" data-scroll-reveal="enter left and move 20px over 1s wait 0s">
                       <?php the_content(); ?>
                    </div>
                    <div class="col-sm-3 col-sm-height col-sm-middle" data-scroll-reveal="enter right and move 20px over 1s wait 0s">
                        <a href="http://materiais.bgmrodotec.com.br/agendar-demonstracao" target="_blank" class="btn btn-danger btn-block bt-agendar-demonstracao">AGENDAR <br> DEMONSTRAÇÃO</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="title">Veja o que o Globus pode fazer para sua empresa:</div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="box-check-list">
                        <div class="check-list">
                            
                             <?php echo get_field('lista_1'); ?>
                        </div>
                    </div>
                    <div class="box-check-list">
                        <div class="check-list">
                           
                            <?php echo get_field('lista_2'); ?>
                        </div>
                        <div class="check-list">
                            <hr>
                           
                            <?php echo get_field('lista_3'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="solucoes-operacionais s-o-b">
        <div class="container">
            <div class="row">
                <div class="col-sm-12" data-scroll-reveal="enter">
                    <div class="title">soluções <b>operacionais</b></div>
                    <div class="subtitle">O sistema possui um módulo específico para cada área de atividade da empresa transportadora.</div>
                </div>
            </div>
              <?php 
                    $terms = get_terms( array(
                        'taxonomy' => 'passageirocat',
                        'hide_empty' => false,
                    ) );
                  foreach ($terms as $term) : 
                   ?>
            <div class="row">
                <div class="col-sm-12">
                    <h3 id="<?php echo $term->slug; ?>"><?php echo $term->name; ?></h3>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <ul class="btns">
                        <?php $args = array('post_type'=>'passageiros', 'passageirocat' => $term->slug, 'order' => 'ASC' );
                                query_posts($args);
                                $c =-1; $j=-1;
                                while(have_posts()): the_post(); $c++; ?>
                            <li class="<?php echo $c == '0' ? 'active' : null; ?>"><a data-toggle="pill" href="#tab<?php echo $post->ID; ?>"><span><?php the_title(); ?></span></a></li>
                        <?php endwhile;  ?>
                            
                        </ul>
                    </div>
                    <div class="col-sm-8">
                        <div class="tab-content">
                            <?php  while(have_posts()): the_post(); $j++; ?>
                            <div id="tab<?php echo $post->ID; ?>" class="tab-pane fade <?php echo $j == '0' ? 'in active' : null; ?>">
                                <div class="check-list">
                                    <?php the_content(); ?>
                                </div>
                            </div>
                            <?php endwhile; wp_reset_query(); ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>


            <div class="row">
                <div class="col-sm-8 col-sm-push-2">
                    <a href="<?php bloginfo('url'); ?>/globus#solucoes-integradas-detalhes" class="btn btn-gradient btn-block text-uppercase">< Voltar para módulos Globus</a>
                </div>
            </div>
        </div>
    </section>

  <?php 
    get_template_part('includes/content', 'mosaico'); // MOSAICO
    
    get_template_part('includes/content','newsletter'); //NEWSLETTER 
    
    get_footer(); 
?>