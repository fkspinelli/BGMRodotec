<?php  
/*
* Template Name: Biblioteca
*
*/
get_header();
the_post();
?>



<section class="banner">
    <div id="carouselBiblioteca" class="carousel carousel-fade slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            <div class="item active" style="background-image: url(<?php bloginfo('template_url'); ?>/images/banner/materiais.jpg);">
                <div class="container">
                    <div class="vcenter">
                        <div class="text">
                            <h1>Biblioteca</h1>
                            <h2><?php the_content(); ?></h2>
                        </div>
                        <div class="links">
                            <div class="row">

                                <?php $banner_e_book = CFS()->get( 'e_book' ); if ( count( $banner_e_book ) ) { ?>
                                <div class="col-sm-4">
                                    <a href="#ebooks"><span class="icon icon-ebook icon-size-50"></span> <span>E-books</span></a>
                                </div>
                                <?php } ?>

                                <?php $banner_videos = CFS()->get( 'video' ); if ( count( $banner_videos ) ) { ?>
                                <div class="col-sm-4">
                                    <a href="#videos"><span class="icon icon-video icon-size-50"></span> <span>Vídeos</span></a>
                                </div>
                                <?php } ?>

                                <?php $banner_webinar = CFS()->get( 'webinar' ); if ( count( $banner_webinar ) ) { ?>
                                <div class="col-sm-4">
                                    <a href="#webinars"><span class="icon icon-webinar icon-size-50"></span> <span>WEBINARS</span></a>
                                </div>
                                <?php } ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div style="padding-bottom:40px;"></div>
            </div>
        </div>
    </div>
</section>

    <section id="ebooks" class="nosso-time">
        <div class="container">
            <div class="title title-2"><strong>Ebooks</strong></div>
            <div class="subtitle">Preencha o formulário e baixe gratuitamente nossos ebooks</div>
            <div class="row">
                <div class="col-sm-9">
                    <div class="row">
                        <?php
                            // E-books
                            $ebooks = CFS()->get( 'e_book' );
                            if ( count( $ebooks ) ) {
                                $i = 0;
                                foreach ( $ebooks as $ebook ) {
                        ?>
                        <div class="col-md-4 col-sm-6">
                            <div class="ebook">
                                <img src="<?php echo $ebook['imagem']; ?>">
                                <a data-href="<?php echo $ebook['arquivo']; ?>" class="btn btn-default btn-gradient btn-block text-uppercase btn-ebook disabled" data-download="<?php echo $ebook['titulo']; ?>"><i class="ion-archive"></i> Download</a>
                            </div>
                        </div>
                        <?php
                                    $i++;
                                }
                            }
                        ?>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-materiais form-materiais-ebooks">
                        <?php echo do_shortcode('[contact-form-7 id="1286" title="Formulário Materiais"]'); ?>
                        <div class="ebook-success">
                            <b><img src="<?php echo bloginfo('template_url'); ?>/images/icon/check-ebook.png" style="vertical-align: bottom;"> Dados Enviados!</b><br>
                            Agora é só baixar seus e-books.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
        // Vídeos
        $videos = CFS()->get( 'video' );
        if ( count( $videos ) ) {
    ?>
    <section id="videos" class="nosso-time bg-f2f2f2 padding-50">
        <div class="container">
            <div class="videos">
                <div class="title title-2"><strong>Vídeos</strong></div>
                <div class="subtitle">Assista na íntegra</div>
                <div class="content-video clearfix">
                    <div class="box-video">
                        <?php
                            $i = 0;
                            foreach ( $videos as $video ) {
                                if ($i === 0) {
                        ?>
                        <iframe src="<?php echo str_replace("https://youtu.be/","https://www.youtube.com/embed/", $video['url']) ?>" frameborder="0" allowfullscreen></iframe>
                        <?php
                                }
                                $i++;
                            }
                        ?>
                    </div>
                    <div class="list-videos">
                        <ul>
                            <?php
                                $i = 0;
                                foreach ( $videos as $video ) {
                                    $video_id = str_replace("https://youtu.be/","", $video['url']);
                            ?>
                            <li>
                                <a href="#" data-youtube-id="<?php echo $video_id ?>">
                                    <div class="row">
                                        <div class="col-xs-5">
                                            <div class="thumb" style="background-image: url(https://img.youtube.com/vi/<?php echo $video_id ?>/0.jpg);"></div>
                                        </div>
                                        <div class="col-xs-7">
                                            <?php echo $video['titulo'] ?>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <?php
                                    $i++;
                                }
                            ?>
                        </ul>
                    </div>
                </div>
                <div class="row bt">
                    <div class="col-sm-4 col-sm-push-4">
                        <a href="https://www.youtube.com/channel/UCx-43tOdhMdDDyE7ceM0KBA" class="btn btn-default btn-gradient btn-block text-uppercase" target="_blank">Acesse o nosso canal</a> 
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php 
        } 
    ?>

    <?php
        // Webinar
        $webinars = CFS()->get( 'webinar' );
        if ( count( $webinars ) ) {
    ?>
    <section id="webinars" class="nosso-time padding-50">
        <div class="container">
            <div class="videos">
                <div class="title title-2"><strong>Webinars</strong></div>
                <div class="subtitle">Confira os materiais que preparamos pra você</div>
                <div class="content-video clearfix">
                    <div class="box-video">
                        <?php
                            $i = 0;
                            foreach ( $webinars as $webinar ) {
                                if ($i === 0) {
                        ?>
                        <iframe src="<?php echo str_replace("https://youtu.be/","https://www.youtube.com/embed/", $webinar['url']) ?>" frameborder="0" allowfullscreen></iframe>
                        <?php
                                }
                                $i++;
                            }
                        ?>
                    </div>
                    <div class="list-videos">
                        <ul>
                            <?php
                                $i = 0;
                                foreach ( $webinars as $webinar ) {
                                    $webinar_id = str_replace("https://youtu.be/","", $webinar['url']);
                            ?>
                            <li>
                                <a href="#" data-youtube-id="<?php echo $webinar_id ?>">
                                    <div class="row">
                                        <div class="col-xs-5">
                                            <div class="thumb" style="background-image: url(https://img.youtube.com/vi/<?php echo $webinar_id ?>/0.jpg);"></div>
                                        </div>
                                        <div class="col-xs-7">
                                            <?php echo $webinar['titulo'] ?>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <?php
                                    $i++;
                                }
                            ?>
                        </ul>
                    </div>
                </div>
                <div class="row bt">
                    <div class="col-sm-4 col-sm-push-4">
                        <a href="http://materiais.bgmrodotec.com.br/calendario-de-webinars-bgmrodotec" class="btn btn-default btn-gradient btn-block text-uppercase" target="_blank">Acesse o calendário de webinars</a> 
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php 
        } 
    ?>
<?php get_template_part('includes/content','newsletter'); //NEWSLETTER ?>

<?php get_footer(); ?>