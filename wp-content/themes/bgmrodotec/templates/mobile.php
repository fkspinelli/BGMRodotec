<?php  
/*
* Template Name: Globus Mobile
*
*/
get_header();
the_post();
$banner = wp_get_attachment_image_src(get_post_thumbnail_id(),'full');
?>
  <section class="banner banner-interna" style="background-image: url(<?php echo $banner[0]; ?>);">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="text" data-scroll-reveal="move 20px">
                        <h1><b>GLOBUS</b> <?php the_title(); ?></h1>
                        <h2>
                            <?php echo get_field('subtitulo'); ?>
                        </h2>
                        <a href="<?php bloginfo('url'); ?>/contato/" class="btn btn-default btn-outline">AGENDE UMA DEMOSTRAÇÃO</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
        <div class="mobile-detalhes">
         <!-- categorias de mobiles -->
                <?php 
                    $terms = get_terms( array(
                        'taxonomy' => 'mobilecat',
                        'hide_empty' => false,
                    ) );
                  foreach ($terms as $term) : 
                   ?>
            <div class="box-mobile">

               
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <?php $img = get_field('imagem_categoria', $term); //print_r($img); ?>
                            <img src="<?php echo $img['sizes']['large']; ?>">
                            
                        </div>
                        <div class="col-sm-8">
                            <h3><span><?php echo $term->name; ?></span></h3>

                            <div class="panel-group" id="accordion<?php echo $term->ID; ?>">
                                
                                <?php $args = array('post_type'=>'mobile', 'mobilecat' => $term->slug, 'order' => 'ASC' );
                                query_posts($args);
                                $c =0;
                                while(have_posts()): the_post(); $c++;
                                 ?>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion<?php echo $term->ID; ?>" href="#collapse<?php echo $post->ID; ?>"><?php the_title(); ?></a>
                                    </h4>
                                    </div>
                                    <div id="collapse<?php echo $post->ID; ?>" class="panel-collapse collapse <?php echo $c == '1' ? 'in' : null; ?>">
                                        <div class="panel-body">
                                            <div class="check-list">
                                                <?php the_content(); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php endwhile; wp_reset_query(); ?>
                      
                               
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
         
        </div>
<?php 
    get_template_part('includes/content', 'mosaico'); // MOSAICO
    
    get_template_part('includes/content','newsletter'); //NEWSLETTER 
    
    get_footer(); 
?>