             <div class="col-sm-4">
                    <div class="panel-list">
                        <div class="panel-header">
                            categorias
                        </div>
                        <div class="panel-body">
                           <ul>
                                <?php wp_list_categories(array('taxonomy'=>'category_blog', 'title_li' => '', 'show_count'=>true)); ?>
                           </ul>
                        </div>
                    </div>
                    <div class="panel-list">
                        <div class="panel-header">
                            MAIS VISTOS
                        </div>
                        <div class="panel-body">
                        <?php 
                            $popularpost = new WP_Query( array( 'posts_per_page' => 5, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC', 'post_type' =>'blog'  ) );
                            
                            ?>
                            <ul>
                            <?php while ( $popularpost->have_posts() ) : $popularpost->the_post();
                             ?>
                                <li>
                                    <a href="<?php the_permalink(); ?>">
                                        <span><?php the_title(); ?></span>
                                        <span><?php the_date(); ?></span>
                                        <p><?php echo wpb_get_post_views(get_the_ID()); ?></p>
                                    </a>
                                </li>
                               <?php endwhile; wp_reset_postdata(); ?>
                            </ul>
                        </div>
                    </div>
                    <div class="panel-list">
                        <div class="panel-header">
                            tags
                        </div>
                        <div class="panel-body">
                            <div class="tags">
                               <?php $tags = get_terms(['taxonomy' => 'blog_tag','hide_empty' => true]); foreach ($tags as $tag): ?>
                                <a href="<?php bloginfo('url'); ?>/tag/<?php echo $tag->slug; ?>"><?php echo $tag->name; ?></a>
                               <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>