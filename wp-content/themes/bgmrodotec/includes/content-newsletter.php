<section class="news">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <div class="title" data-scroll-reveal="move 50px wait 0.3s">assine <b>nossa newsletter</b></div>
               

                    <?php echo do_shortcode('[contact-form-7 id="5" html_class="form-inline" html_data-scroll-reveal="move 25px wait 0.4s" title="Formulário de Newsletter"]'); ?>
            </div>
        </div>
    </div>
</section>
<style type="text/css">
.news .screen-reader-response {
	display: none;
}

.news .form-group {
    padding-bottom: 30px;	
}

.news .form-group > span {
    display: block;
    position: relative;
}

.news .form-inline .form-group > span .form-control {
	display: block;
}

.news .form-group > span input + span {
    position: absolute;
    left: 0;
    color: #d40000;
    margin-top: 5px;
}
.news [type="submit"] {
    margin-bottom: 30px;
}

.news .wpcf7-validation-errors {
    display: none !important;
}
</style>