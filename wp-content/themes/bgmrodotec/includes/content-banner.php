<section class="banner">
    <div id="carouselHome" class="carousel carousel-fade slide" data-ride="carousel">
        <div class="bottom">
            <div class="container">
                <ol class="carousel-indicators">
                 <?php $c = -1; $args_banner = array('post_type'=>'banners'); query_posts($args_banner); while(have_posts()): the_post(); $c++; ?>
                    <li data-target="#carouselHome" data-slide-to="<?php echo $c; ?>" class="<?php echo $c == '0' ? 'active' : null;  ?>"></li>
                <?php endwhile; ?>
                </ol>
            </div>
        </div>
        <div class="carousel-inner" role="listbox">
            <?php $j = -1; while(have_posts()): the_post(); $img = wp_get_attachment_image_src(get_post_thumbnail_id(),'large'); $j++; ?>
            <div class="item <?php echo $j == '0' ? 'active' : null;  ?> <?php if (get_field('banner_principal') != 1) {echo 'new-banner';} ?>" style="background-image: url(<?php echo $img[0]; ?>);">
                <?php if (get_field('banner_principal') == 1) { ?>
                <div class="container">
                    <div class="vcenter">
                        <div class="text">
                            <?php the_content(); ?>
                        </div>
                        <div class="links">
                            <div class="row">
                                <div class="col-sm-4">
                                    <a href="<?php bloginfo('url'); ?>/globus/passageiros"><span class="icon icon-passageiros icon-size-50"></span> <span>passageiros</span></a>
                                </div>
                                <div class="col-sm-4">
                                    <a href="<?php bloginfo('url'); ?>/globus/carga"><span class="icon icon-cargas icon-size-50"></span> <span>carga</span></a>
                                </div>
                                <div class="col-sm-4">
                                    <a href="<?php bloginfo('url'); ?>/globus/trr"><span class="icon icon-trr icon-size-50"></span> <span>trr</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <a href="http://materiais.bgmrodotec.com.br/agendar-demonstracao" target="_blank" class="btn btn-default text-uppercase bt-agendar-demonstracao">AGENDAR DEMONSTRAÇÃO</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="padding-bottom:40px;"></div>
                <?php } else { ?>
                <div class="container">
                    <div class="vcenter">
                        <div class="text" style="background-color: <?php echo hex2rgba(get_field('cor_do_box'), $opacity = 0.7); ?>">
                            <h2><?php the_content(); ?></h2>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <a href="<?php echo get_field('link_botao'); ?>" class="bt" id="bt_<?php echo $j; ?>"><?php echo get_field('texto_botao'); ?></a>
                                <style>
                                    #bt_<?php echo $j; ?> {
                                        background-color: <?php echo hex2rgba(get_field('cor_do_box'), $opacity = 0.7); ?>;
                                    }

                                    #bt_<?php echo $j; ?>:hover {
                                        background-color: <?php echo hex2rgba(get_field('cor_do_box'), $opacity = 0.9); ?>;
                                    }
                                </style>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="padding-bottom:40px;"></div>
                <?php } ?>
            </div>
            <?php endwhile; wp_reset_query(); ?>
        </div>
    </div>
</section>