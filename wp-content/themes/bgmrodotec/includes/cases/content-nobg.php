<section class="case-text">
    <div class="container"> 
        <div class="row">
            <div class="col-sm-12" data-scroll-reveal="move 20px">
                <h3><?php the_title(); ?></h4>
                <h4><?php echo get_field('subtitulo'); ?></h5>
                <?php the_content(); ?>
                <!-- <a href="<?php echo get_field('link'); ?>" class="btn btn-gradient text-uppercase"><?php echo get_field('link_texto'); ?></a> -->
            </div>
        </div>
    </div>
</section>
