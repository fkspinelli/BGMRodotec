<section class="background-text bg-<?php echo str_replace('#', '', get_field('custom_bg')); ?>">
    <div class="container">
        <div class="row">
            <div class="row-height">
                <div class="col-sm-5 col-xs-height col-xs-middle">
                <?php $img = wp_get_attachment_image_src(get_post_thumbnail_id(),'large'); ?>
                    <div class="background-left" style="background-image: url(<?php echo $img[0]; ?>); height: 100%;"></div>
                    <div class="text">
                        <h3 data-scroll-reveal="move 20px"><?php the_title(); ?></h3>
                        <h4 data-scroll-reveal="move 20px"><?php echo get_field('subtitulo'); ?></h4>
                      
                          <!-- <a href="<?php echo get_field('link'); ?>" class="btn btn-default text-uppercase" data-scroll-reveal="move 20px"><?php echo get_field('link_texto'); ?></a> -->
                    </div>
                </div>
                <div class="col-sm-7 col-xs-height col-xs-middle" data-scroll-reveal="move 20px">
                   <?php the_content(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
// $(document).ready(function(){
//     $(window).on('load resize', function(){
//         $('.background-left').css({'width': $('.background-left').parents('[class*="col-"]').offset().left + $('.background-left').parents('[class*="col-"]').width() +15 });
//     });
// });
</script>