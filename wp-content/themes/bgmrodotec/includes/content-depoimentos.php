<section class="depoimento">
    <div class="container">
        <div id="depoimentoCarousel" class="carousel carousel-fade slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                <?php $i=0; query_posts(['post_type'=>'testemunhos']); while(have_posts()): the_post(); $i++; ?>
                <div class="item <?php echo $i==1 ? 'active' : null; ?>">
                    <div class="row">
<!--                         <div class="col-sm-4">
                            <ul class="list-progress">
                                <li>
                                    <div class="pull-left">
                                        <div class="cicular-progress">
                                            <div data-name="" data-percent="<?php echo get_field('economia_manutencao'); ?>%">
                                                <svg viewBox="-10 -10 220 220">
                                                    <g fill="none" stroke-width="20" transform="translate(100,100)">
                                                        <path d="M 0,-100 A 100,100 0 0,1 86.6,-50" stroke="url(#cl1)" />
                                                        <path d="M 86.6,-50 A 100,100 0 0,1 86.6,50" stroke="url(#cl2)" />
                                                        <path d="M 86.6,50 A 100,100 0 0,1 0,100" stroke="url(#cl3)" />
                                                        <path d="M 0,100 A 100,100 0 0,1 -86.6,50" stroke="url(#cl4)" />
                                                        <path d="M -86.6,50 A 100,100 0 0,1 -86.6,-50" stroke="url(#cl5)" />
                                                        <path d="M -86.6,-50 A 100,100 0 0,1 0,-100" stroke="url(#cl6)" /> </g>
                                                </svg>
                                                <svg viewBox="-10 -10 220 220">
                                                    <path d="M200,100 C200,44.771525 155.228475,0 100,0 C44.771525,0 0,44.771525 0,100 C0,155.228475 44.771525,200 100,200 C155.228475,200 200,155.228475 200,100 Z" stroke-dashoffset="<?php echo (629 * (get_field('economia_manutencao')/100)); ?>"></path>
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pull-left">
                                        <p>Economia em manutenção</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="pull-left">
                                        <div class="cicular-progress">
                                            <div data-name="" data-percent="<?php echo get_field('economia_combustivel'); ?>%">
                                                <svg viewBox="-10 -10 220 220">
                                                    <g fill="none" stroke-width="20" transform="translate(100,100)">
                                                        <path d="M 0,-100 A 100,100 0 0,1 86.6,-50" stroke="url(#cl1)" />
                                                        <path d="M 86.6,-50 A 100,100 0 0,1 86.6,50" stroke="url(#cl2)" />
                                                        <path d="M 86.6,50 A 100,100 0 0,1 0,100" stroke="url(#cl3)" />
                                                        <path d="M 0,100 A 100,100 0 0,1 -86.6,50" stroke="url(#cl4)" />
                                                        <path d="M -86.6,50 A 100,100 0 0,1 -86.6,-50" stroke="url(#cl5)" />
                                                        <path d="M -86.6,-50 A 100,100 0 0,1 0,-100" stroke="url(#cl6)" /> </g>
                                                </svg>
                                                <svg viewBox="-10 -10 220 220">
                                                    <path d="M200,100 C200,44.771525 155.228475,0 100,0 C44.771525,0 0,44.771525 0,100 C0,155.228475 44.771525,200 100,200 C155.228475,200 200,155.228475 200,100 Z" stroke-dashoffset="<?php echo (629 * (get_field('economia_combustivel')/100)); ?>"></path>
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pull-left">
                                        <p>Economia de combustível</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="pull-left">
                                        <div class="cicular-progress">
                                            <div data-name="" data-percent="<?php echo get_field('economia_hora_extra'); ?>%">
                                                <svg viewBox="-10 -10 220 220">
                                                    <g fill="none" stroke-width="20" transform="translate(100,100)">
                                                        <path d="M 0,-100 A 100,100 0 0,1 86.6,-50" stroke="url(#cl1)" />
                                                        <path d="M 86.6,-50 A 100,100 0 0,1 86.6,50" stroke="url(#cl2)" />
                                                        <path d="M 86.6,50 A 100,100 0 0,1 0,100" stroke="url(#cl3)" />
                                                        <path d="M 0,100 A 100,100 0 0,1 -86.6,50" stroke="url(#cl4)" />
                                                        <path d="M -86.6,50 A 100,100 0 0,1 -86.6,-50" stroke="url(#cl5)" />
                                                        <path d="M -86.6,-50 A 100,100 0 0,1 0,-100" stroke="url(#cl6)" /> </g>
                                                </svg>
                                                <svg viewBox="-10 -10 220 220">
                                                    <path d="M200,100 C200,44.771525 155.228475,0 100,0 C44.771525,0 0,44.771525 0,100 C0,155.228475 44.771525,200 100,200 C155.228475,200 200,155.228475 200,100 Z" stroke-dashoffset="<?php echo (629 * (get_field('economia_hora_extra')/100)); ?>"></path>
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pull-left">
                                        <p>Economia de hora extra</p>
                                    </div>
                                </li>
                            </ul>
                        </div> -->
                        <div class="col-sm-8">
                            <h4><?php the_title(); ?></h4>
                            <h6><i><?php the_excerpt(); ?></i></h6>
                            <p><b><?php echo mb_strtoupper(get_field('nome')); ?></b></p>
                            <p>
                                <?php echo get_field('info'); ?>
                            </p>
                        </div>
                    </div>
                </div>
                <?php endwhile; wp_reset_query(); ?>
                <!--  Defining Angle Gradient Colors  -->
                <svg width="0" height="0">
                    <defs>
                        <linearGradient id="cl1" gradientUnits="objectBoundingBox" x1="0" y1="0" x2="1" y2="1">
                            <stop stop-color="#eb1e32" />
                            <stop offset="100%" stop-color="#bb283d" />
                        </linearGradient>
                        <linearGradient id="cl2" gradientUnits="objectBoundingBox" x1="0" y1="0" x2="0" y2="1">
                            <stop stop-color="#bb283d" />
                            <stop offset="100%" stop-color="#aa3040" />
                        </linearGradient>
                        <linearGradient id="cl3" gradientUnits="objectBoundingBox" x1="1" y1="0" x2="0" y2="1">
                            <stop stop-color="#aa3040" />
                            <stop offset="100%" stop-color="#eb1e32" />
                        </linearGradient>
                        <linearGradient id="cl4" gradientUnits="objectBoundingBox" x1="1" y1="1" x2="0" y2="0">
                            <stop stop-color="#eb1e32" />
                            <stop offset="100%" stop-color="#f05926" />
                        </linearGradient>
                        <linearGradient id="cl5" gradientUnits="objectBoundingBox" x1="0" y1="1" x2="0" y2="0">
                            <stop stop-color="#f05926" />
                            <stop offset="100%" stop-color="#f06b23" />
                        </linearGradient>
                        <linearGradient id="cl6" gradientUnits="objectBoundingBox" x1="0" y1="1" x2="1" y2="0">
                            <stop stop-color="#f06b23" />
                            <stop offset="100%" stop-color="#f0a523" />
                        </linearGradient>
                    </defs>
                </svg>
            </div>
            <div class="controls">
                <a class="left carousel-control" href="#depoimentoCarousel" role="button" data-slide="prev">
                    <img src="<?php bloginfo('template_url'); ?>/images/icon/btn-prev.png">
                </a>
                <a class="right carousel-control" href="#depoimentoCarousel" role="button" data-slide="next">
                    <img src="<?php bloginfo('template_url'); ?>/images/icon/btn-next.png">
                </a>
            </div>
        </div>
    </div>
</section>
