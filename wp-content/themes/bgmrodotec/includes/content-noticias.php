<section class="box-blog">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-sm-push-2 text-center">
                <div class="title">siga <b>nosso Blog</b></div>
                <div class="subtitle">Conteúdos exclusívos, últimas noticías sobre produtos e <br> serviços, gestão de custos e obrigações ficais e legais.</div>
            </div>
        </div>
        <div class="row">
        <?php query_posts(['post_type'=>'blog', 'posts_per_page'=>'3']); while(have_posts()): the_post(); ?>
            <div class="col-sm-4">
                <a href="<?php the_permalink(); ?>" data-scroll-reveal="move 50px wait 0.3s">
                    <?php $img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumbnail'); ?>
                    <img class="img-responsive" src="<?php echo $img[0]; ?>" width="<?php echo $img[1]; ?>" height="<?php echo $img[2]; ?>" alt="<?php echo get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true); ?>">
                    <div class="text">
                        <p><?php the_title(); ?></p>
                        <div class="btn btn-default text-uppercase">ler matéria</div>
                    </div>
                </a>
            </div>
         <?php endwhile; wp_reset_query(); ?>  
        </div>
    </div>
</section>