  <footer>
        <div class="container">
            <div class="row footer-top">
                <div class="col-sm-2">
                    <a href="#">
                        <img src="<?php bloginfo('template_url'); ?>/images/logo/logo-bgm-rodotec.png" class="img-responsive logo" alt="BgmRodotec">
                    </a>
                </div>
                <div class="col-sm-8">
                    <div class="row">
                        <div class="col-sm-4">
                            <a>rj (21) 3525-2929 </a>
                        </div>
                        <div class="col-sm-4">
                            <a>sp (11) 5018-2525 </a>
                        </div>
                        <div class="col-sm-4">
                            <a>mg (31) 3287-1207 </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <a>pe (81) 3224-5750 </a>
                        </div>
                        <div class="col-sm-4">
                            <a>sc (47) 3037-3005 </a>
                        </div>
                        <div class="col-sm-4">
                            <a>pe (81) 4106-4449</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2">
                    <a href="https://www.linkedin.com/company/bgmrodotec" target="_blank" class="redesocial">
                        <img src="<?php bloginfo('template_url'); ?>/images/icon/linkedin.png" class="img-responsive">
                    </a>
                    <a href="https://www.facebook.com/BgmRodotec" target="_blank" class="redesocial">
                        <img src="<?php bloginfo('template_url'); ?>/images/icon/facebook.png" class="img-responsive">
                    </a>
                    <a href="https://www.youtube.com/user/BgmRodotec1" target="_blank" class="redesocial">
                        <img src="<?php bloginfo('template_url'); ?>/images/icon/youtube.png" class="img-responsive">
                    </a>
                    <a href="https://twitter.com/BgmRodotec" target="_blank" class="redesocial">
                        <img src="<?php bloginfo('template_url'); ?>/images/icon/twitter.png" class="img-responsive">
                    </a>
                </div>
                <div class="line"></div>
            </div>
            <div class="row">
                <div class="col-sm-12">   
                    <nav>
                        <ul>
                            <li>
                                <a>Empresa</a>
                                <ul>
                                    <li>
                                        <a href="<?php bloginfo('url'); ?>/empresa">Quem Somos</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="<?php bloginfo('url'); ?>/globus">Soluções</a>
                                <ul>
                                    <li>
                                        <a href="<?php bloginfo('url'); ?>/globus">Globus</a>
                                    </li>
                                    <li>
                                        <a href="<?php bloginfo('url'); ?>/globus/intelligence">Globus Intelligence</a>
                                    </li>
                                    <li>
                                        <a href="<?php bloginfo('url'); ?>/consultoria/">Consultoria</a>
                                    </li>    
                                </ul>
                            </li>
                            <li>
                                <a href="<?php bloginfo('url'); ?>/clientes">Clientes</a>
                            </li>
                            <li>
                                <a href="<?php bloginfo('url'); ?>/cases">Cases</a>
                            </li>
                            <li>
                                <a href="<?php bloginfo('url'); ?>/blog">Blog</a>
                            </li>
                            <li>
                                <a href="<?php bloginfo('url'); ?>/contato">Contato</a>
<!--                                 <ul>
                                    <li>
                                        <a>Rio de Janeiro</a>
                                    </li>
                                    <li>
                                        <a>São Paulo</a>
                                    </li>
                                    <li>
                                        <a>Vitória</a>
                                    </li>
                                    <li>
                                        <a>Minas Gerais</a>
                                    </li>
                                    <li>
                                        <a>Recife</a>
                                    </li>
                                    <li>
                                        <a>Blumenau</a>
                                    </li>
                                </ul> -->
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <p>© BgmRodotec - 2016</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>


    

    <?php wp_footer(); ?>

    <!-- script type="text/javascript" async src="https://d335luupugsy2.cloudfront.net/js/loader-scripts/92876efe-bfe7-474c-9133-9fc15ecb0add-loader.js" ></script-->

    <script type="text/javascript" async src="https://d335luupugsy2.cloudfront.net/js/loader-scripts/92876efe-bfe7-474c-9133-9fc15ecb0add-loader.js"></script>
  
    <script type="text/javascript" src="https://d335luupugsy2.cloudfront.net/js/integration/stable/rd-js-integration.min.js"></script>
    <script type="text/javascript">
    function sendToRD_Contato(event) {
        event.preventDefault();
        var identificador = 'Formulário de Demonstração';
        var token_rdstation = '8e0f446f254278102d860f72a325abaf';
        var meus_campos = {
            'email'    : 'email',
            'nome'     : 'nome',
            'assunto'  : 'assunto',
            'mensagem' : 'mensagem'
        };
        var form = jQuery(event.target).closest('form');
        var data_array = form.find(':input').serializeArray();
        var select = form.find(':input[name="assunto"] :selected').val();
        var b = jQuery(this);
        jQuery.each(data_array, function() { if (meus_campos[this.name]) { this.name = meus_campos[this.name]; } });
        data_array.push({ name: 'identificador', value: identificador }, { name: 'token_rdstation', value: token_rdstation });
        if (select == 'Demonstração') {
            RdIntegration.post(data_array, function() {
                b.unbind('click', sendToRD_Contato).click();
                b.bind('click', sendToRD_Contato);
                return false;
            });
        } else {
            b.unbind('click', sendToRD_Contato).click();
            b.bind('click', sendToRD_Contato);
        }

    };


    function sendToRD_News(event) {
        event.preventDefault();
        var identificador = 'Formulário de Newsletter';
        var token_rdstation = '8e0f446f254278102d860f72a325abaf';
        var meus_campos = {
            'email'    : 'email',
            'nome'     : 'nome'
        };
        var form = jQuery(event.target).closest('form');
        var data_array = form.find(':input').serializeArray();
        var b = jQuery(this);
        jQuery.each(data_array, function() { if (meus_campos[this.name]) { this.name = meus_campos[this.name]; } });
        data_array.push({ name: 'identificador', value: identificador }, { name: 'token_rdstation', value: token_rdstation });
        RdIntegration.post(data_array, function() {
            b.unbind('click', sendToRD_News).click();
            b.bind('click', sendToRD_News);
            return false;
        });
    };

    jQuery('section.contato :submit').bind('click', sendToRD_Contato);
    jQuery('section.news :submit').bind('click', sendToRD_News);
    </script>




    <!-- Código do Google para tag de remarketing -->
    <script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 1064836977;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
    <div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1064836977/?guid=ON&amp;script=0"/>
    </div>
    </noscript>
    <style type="text/css">[name="google_conversion_frame"] { display: none;}</style>
    <!-- / Código do Google para tag de remarketing -->



</body>

</html>