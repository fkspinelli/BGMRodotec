<?php

add_image_size('med', 293,194,true);
add_image_size('peq', 204,136,true);


// Adicionando excerpt para paginas
add_action( 'init', 'my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}


the_posts_pagination( array(
    'mid_size'  => 2,
    'prev_text' => __( 'Back', 'textdomain' ),
    'next_text' => __( 'Onward', 'textdomain' ),
) );


add_theme_support( 'post-thumbnails' );

add_action( 'init', 'create_post_type' );

function create_post_type() {

    register_post_type( 'banners',
    array(
      'labels' => array(
        'name' => __( 'Banners' ),
        'singular_name' => __( 'Banner' ),
        'add_new_item'  => __( 'Adicionar novo banner'),
      ),
      'public' => true,
      'has_archive' => true,

      'menu_position'         => 3,
      'rewrite' => array('slug' => 'banners'),
      'supports' => array('title','editor','thumbnail')
    )
  );
  register_post_type( 'banner_menu',
    array(
      'labels' => array(
        'name' => __( 'Banner Menu' ),
        'singular_name' => __( 'Banner Menu' ),
        'add_new_item'  => __( 'Adicionar novo banner menu'),
      ),
      'public' => true,
    //  'has_archive' => true,

      'menu_position'         => 3,
      //'rewrite' => array('slug' => 'banners'),
      'supports' => array('title','editor','thumbnail')
    )
  );
   register_post_type( 'testemunhos',
    array(
      'labels' => array(
        'name' => __( 'Testemunhos' ),
        'singular_name' => __( 'Testemunho' ),
        'add_new_item'  => __( 'Adicionar novo testemunho'),
      ),
      'public' => true,
      'has_archive' => true,

      'menu_position'         => 4,
      'rewrite' => array('slug' => 'testemunhos'),
      'supports' => array('title','excerpt','editor','revisions')
    )
  );

register_post_type( 'servicos',
    array(
      'labels' => array(
        'name' => __( 'Serviços' ),
        'singular_name' => __( 'Serviço' ),
        'add_new_item'  => __( 'Adicionar novo serviço'),
      ),
      'public' => true,
      'has_archive' => true,

      'menu_position'         => 4,
      'rewrite' => array('slug' => 'servicos'),
      'supports' => array('title','excerpt','editor','revisions')
    )
  );
register_post_type( 'blog',
    array(
      'labels' => array(
        'name' => __( 'Blog' ),
        'singular_name' => __( 'Blog' ),
        'add_new_item'  => __( 'Adicionar novo artigo para o blog'),
      ),
      'public' => true,
      'has_archive' => true,
      'taxonomies'            => array( 'category_blog', 'blog_tag' ),
      'menu_position'         => 4,
      'rewrite' => array('slug' => 'blog'),
      'supports' => array('title','excerpt','thumbnail','editor','revisions','tag')
    )
  );
 register_post_type( 'cases',
    array(
      'labels' => array(
        'name' => __( 'Cases' ),
        'singular_name' => __( 'Case' ),
        'add_new_item'  => __( 'Adicionar novo case'),
      ),
      'public' => true,
      'has_archive' => true,

      'menu_position'         => 5,
      'rewrite' => array('slug' => 'cases'),
      'supports' => array('title','editor','thumbnail')
    )
  );
  register_post_type( 'clientes',
    array(
      'labels' => array(
        'name' => __( 'Clientes' ),
        'singular_name' => __( 'Cliente' ),
        'add_new_item'  => __( 'Adicionar novo cliente'),
      ),
      'public' => true,
      'has_archive' => true,

      'menu_position'         => 6,
      'rewrite' => array('slug' => 'clientes'),
      'supports' => array('title','editor','thumbnail')
    )
  );

  register_post_type( 'mobile',
    array(
      'labels' => array(
        'name' => __( 'Globus Mobile' ),
        'singular_name' => __( 'Mobile' ),
        'add_new_item'  => __( 'Adicionar novo mobile'),
      ),
      'public' => true,
      'taxonomies'   => array( 'mobilecat' ),
      'has_archive' => true,
      'menu_position'         => 7,
      //'rewrite' => array('slug' => 'mobile'),
      'supports' => array('title','editor','revisions')
    )
  );

   register_post_type( 'passageiros',
    array(
      'labels' => array(
        'name' => __( 'Globus passageiros' ),
        'singular_name' => __( 'Passageiros' ),
        'add_new_item'  => __( 'Adicionar novo passageiros'),
      ),
      'public' => true,
      'taxonomies'   => array( 'passageirocat' ),
      'has_archive' => true,
      'menu_position'         => 8,
      //'rewrite' => array('slug' => 'mobile'),
      'supports' => array('title','editor','revisions')
    )
  );

   register_post_type( 'cargas',
    array(
      'labels' => array(
        'name' => __( 'Globus cargas' ),
        'singular_name' => __( 'Cargas' ),
        'add_new_item'  => __( 'Adicionar nova carga'),
      ),
      'public' => true,
      'taxonomies'   => array( 'cargacat' ),
      'has_archive' => true,
      'menu_position'         => 9,
      //'rewrite' => array('slug' => 'mobile'),
      'supports' => array('title','editor','revisions')
    )
  );

   register_post_type( 'trr',
    array(
      'labels' => array(
        'name' => __( 'Globus trr' ),
        'singular_name' => __( 'TRR' ),
        'add_new_item'  => __( 'Adicionar nova trr'),
      ),
      'public' => true,
      'taxonomies'   => array( 'trrcat' ),
      'has_archive' => true,
      'menu_position'         => 10,
      //'rewrite' => array('slug' => 'mobile'),
      'supports' => array('title','editor','revisions')
    )
  );

  register_post_type( 'oficinas',
    array(
      'labels' => array(
        'name' => __( 'Globus Oficinas' ),
        'singular_name' => __( 'Oficina' ),
        'add_new_item'  => __( 'Adicionar nova oficina'),
      ),
      'public' => true,
      'taxonomies'   => array( 'oficinacat' ),
      'has_archive' => true,
      'menu_position'         => 11,
      //'rewrite' => array('slug' => 'mobile'),
      'supports' => array('title','editor','revisions')
    )
  );

  register_post_type( 'pessoal',
    array(
      'labels' => array(
        'name' => __( 'Globus Pessoal' ),
        'singular_name' => __( 'Pessoal' ),
        'add_new_item'  => __( 'Adicionar nova pessoal'),
      ),
      'public' => true,
      'taxonomies'   => array( 'pessoalcat' ),
      'has_archive' => true,
      'menu_position'         => 12,
      //'rewrite' => array('slug' => 'mobile'),
      'supports' => array('title','editor','revisions')
    )
  );

   register_post_type( 'apoio',
    array(
      'labels' => array(
        'name' => __( 'Globus Apoio' ),
        'singular_name' => __( 'Apoio' ),
        'add_new_item'  => __( 'Adicionar nova apoio'),
      ),
      'public' => true,
      'taxonomies'   => array( 'apoiocat' ),
      'has_archive' => true,
      'menu_position'         => 13,
      //'rewrite' => array('slug' => 'mobile'),
      'supports' => array('title','editor','revisions')
    )
  );

   register_post_type( 'financeiro',
    array(
      'labels' => array(
        'name' => __( 'Globus financeiro' ),
        'singular_name' => __( 'Financeiro' ),
        'add_new_item'  => __( 'Adicionar nova financeiro'),
      ),
      'public' => true,
      'taxonomies'   => array( 'financeirocat' ),
      'has_archive' => true,
      'menu_position'         => 14,
      //'rewrite' => array('slug' => 'mobile'),
      'supports' => array('title','editor','revisions')
    )
  );

    register_post_type( 'contabil',
    array(
      'labels' => array(
        'name' => __( 'Globus contabil' ),
        'singular_name' => __( 'Contabil' ),
        'add_new_item'  => __( 'Adicionar nova contabil'),
      ),
      'public' => true,
      'taxonomies'   => array( 'contabilcat' ),
      'has_archive' => true,
      'menu_position'         => 15,
      //'rewrite' => array('slug' => 'mobile'),
      'supports' => array('title','editor','revisions')
    )
  );

  register_post_type( 'recursos',
    array(
      'labels' => array(
        'name' => __( 'Controle' ),
        'singular_name' => __( 'Controle' ),
        'add_new_item'  => __( 'Adicionar novo controle'),
      ),
      'public' => true,
      'taxonomies'   => array( 'recursoscat' ),
      'has_archive' => true,
      'menu_position'         => 16,
      //'rewrite' => array('slug' => 'mobile'),
      'supports' => array('title','editor','revisions')
    )
  );

  register_post_type( 'academia',
     array(
       'labels' => array(
         'name' => __( 'Globus Academia' ),
         'singular_name' => __( 'Academia' ),
         'add_new_item'  => __( 'Adicionar novo'),
       ),
       'public' => true,
       'has_archive' => true,
       'menu_position'         => 15,
       'rewrite' => array('slug' => 'cursos-presenciais'),
       'supports' => array('title','excerpt','editor','thumbnail')
     )
   );

}

//Quando der problema no post type acionar esta função
flush_rewrite_rules();


function add_query_vars_filter( $vars ){
  $vars[] = "c";
  return $vars;
}
add_filter( 'query_vars', 'add_query_vars_filter' );

// Adicionando Taxonomias
add_action( 'init', 'custom_taxonomy', 0 );

function custom_taxonomy() {

   $labels_notice = array(
    'name'                       => _x( 'Categoria do blog', 'Taxonomy General Name', 'text_domain' ),
    'singular_name'              => _x( 'Categoria do blog', 'Taxonomy Singular Name', 'text_domain' ),
    'menu_name'                  => __( 'Categoria do blog', 'text_domain' )
    );

  // Categoria de conveniados
   $args_conv = array(
    'labels'                     => $labels_notice,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,

  );

  register_taxonomy( 'category_blog', array( 'blog' ), $args_conv );


  register_taxonomy('blog_tag',array( 'blog' ),array(
    'hierarchical' => false,
   // 'labels' => $labels,
    'show_ui' => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var' => true,
    'rewrite' => array( 'slug' => 'tag' ),
  ));

// categoria mobile
 $labels_mob = array(
    'name'                       => _x( 'Categoria do mobile', 'Taxonomy General Name', 'text_domain' ),
    'singular_name'              => _x( 'Categoria do mobile', 'Taxonomy Singular Name', 'text_domain' ),
    'menu_name'                  => __( 'Categoria do mobile', 'text_domain' )
    );
 $args_mobile = array(
    'hierarchical' => true,
    'labels'                     => $labels_mob,
    'show_ui' => true,
    'public'                     => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,

  );
  register_taxonomy('mobilecat', array('mobile') , $args_mobile);

  // categoria passageiro
 $labels_pas = array(
    'name'                       => _x( 'Categoria do passageiro', 'Taxonomy General Name', 'text_domain' ),
    'singular_name'              => _x( 'Categoria do passageiro', 'Taxonomy Singular Name', 'text_domain' ),
    'menu_name'                  => __( 'Categoria do passageiro', 'text_domain' )
    );
 $args_pas = array(
    'hierarchical' => true,
    'labels'                     => $labels_pas,
    'show_ui' => true,
    'public'                     => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,

  );
  register_taxonomy('passageirocat', array('passageiros') , $args_pas);

  // categoria carga
 $labels_car = array(
    'name'                       => _x( 'Categoria de carga', 'Taxonomy General Name', 'text_domain' ),
    'singular_name'              => _x( 'Categoria de carga', 'Taxonomy Singular Name', 'text_domain' ),
    'menu_name'                  => __( 'Categoria de carga', 'text_domain' )
    );
 $args_car = array(
    'hierarchical' => true,
    'labels'                     => $labels_car,
    'show_ui' => true,
    'public'                     => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,

  );
  register_taxonomy('cargacat', array('cargas') , $args_car);

  // categoria trr
 $labels_trr = array(
    'name'                       => _x( 'Categoria de trr', 'Taxonomy General Name', 'text_domain' ),
    'singular_name'              => _x( 'Categoria de trr', 'Taxonomy Singular Name', 'text_domain' ),
    'menu_name'                  => __( 'Categoria de trr', 'text_domain' )
    );
 $args_trr = array(
    'hierarchical' => true,
    'labels'                     => $labels_trr,
    'show_ui' => true,
    'public'                     => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,

  );
  register_taxonomy('trrcat', array('trr') , $args_trr);

   // categoria oficina
 $labels_ofi = array(
    'name'                       => _x( 'Categoria de oficina', 'Taxonomy General Name', 'text_domain' ),
    'singular_name'              => _x( 'Categoria de oficina', 'Taxonomy Singular Name', 'text_domain' ),
    'menu_name'                  => __( 'Categoria de oficina', 'text_domain' )
    );
 $args_ofi = array(
    'hierarchical' => true,
    'labels'                     => $labels_ofi,
    'show_ui' => true,
    'public'                     => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,

  );
  register_taxonomy('oficinacat', array('oficinas') , $args_ofi);

   // categoria pessoal
 $labels_pes = array(
    'name'                       => _x( 'Categoria de pessoal', 'Taxonomy General Name', 'text_domain' ),
    'singular_name'              => _x( 'Categoria de pessoal', 'Taxonomy Singular Name', 'text_domain' ),
    'menu_name'                  => __( 'Categoria de pessoal', 'text_domain' )
    );
 $args_pes = array(
    'hierarchical' => true,
    'labels'                     => $labels_pes,
    'show_ui' => true,
    'public'                     => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,

  );
  register_taxonomy('pessoalcat', array('pessoal') , $args_pes);

    // categoria apoio
 $labels_apo = array(
    'name'                       => _x( 'Categoria de apoio', 'Taxonomy General Name', 'text_domain' ),
    'singular_name'              => _x( 'Categoria de apoio', 'Taxonomy Singular Name', 'text_domain' ),
    'menu_name'                  => __( 'Categoria de apoio', 'text_domain' )
    );
 $args_apo = array(
    'hierarchical' => true,
    'labels'                     => $labels_apo,
    'show_ui' => true,
    'public'                     => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,

  );
  register_taxonomy('apoiocat', array('apoio') , $args_apo);

    // categoria financeiro
 $labels_fin = array(
    'name'                       => _x( 'Categoria de financeiro', 'Taxonomy General Name', 'text_domain' ),
    'singular_name'              => _x( 'Categoria de financeiro', 'Taxonomy Singular Name', 'text_domain' ),
    'menu_name'                  => __( 'Categoria de financeiro', 'text_domain' )
    );
 $args_fin = array(
    'hierarchical' => true,
    'labels'                     => $labels_fin,
    'show_ui' => true,
    'public'                     => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,

  );
  register_taxonomy('financeirocat', array('financeiro') , $args_fin);

     // categoria contabil
 $labels_cont = array(
    'name'                       => _x( 'Categoria de contabil', 'Taxonomy General Name', 'text_domain' ),
    'singular_name'              => _x( 'Categoria de contabil', 'Taxonomy Singular Name', 'text_domain' ),
    'menu_name'                  => __( 'Categoria de contabil', 'text_domain' )
    );
 $args_cont = array(
    'hierarchical' => true,
    'labels'                     => $labels_cont,
    'show_ui' => true,
    'public'                     => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,

  );
  register_taxonomy('contabilcat', array('contabil') , $args_cont);

     // categoria recursos humanos
 $labels_rec = array(
    'name'                       => _x( 'Categoria de controle', 'Taxonomy General Name', 'text_domain' ),
    'singular_name'              => _x( 'Categoria de controle', 'Taxonomy Singular Name', 'text_domain' ),
    'menu_name'                  => __( 'Categoria de controle', 'text_domain' )
    );
 $args_rec = array(
    'hierarchical' => true,
    'labels'                     => $labels_rec,
    'show_ui' => true,
    'public'                     => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,

  );
  register_taxonomy('recursoscat', array('recursos') , $args_rec);

}

/**
  *Registrando Widget
**/

add_action( 'widgets_init', 'eletros_widgets_init' );
function eletros_widgets_init() {

    # Registradno Sidebars
    register_sidebar( array(
        'name' => __( 'Sidebar Principal', 'eletros-main-sidebar' ),
        'id' => 'sidebar-1',
        'description' => __( 'Widgets nesta área serão mostradas na barra lateral', 'eletros-main-sidebar' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
      ) );
}

/*
* Registrando serviços
*/
function globus_cloud_func( $atts ){
  $res = '<div class="moz-row"><div class="moz" style="background-image: url('.get_bloginfo('template_url').'/images/background/globus_passageiros_moz4.jpg);"> <div class="moz-text"> <div class="content" data-scroll-reveal="move 10px over 1s"> <h4>globus</h4> <h5>cloud</h5> <p>QUER ECONOMIZAR COM INFRAESTRUTURA?</p> <p>Conheça a nossa versão na nuvem.</p> <a href="http://materiais.bgmrodotec.com.br/agendar-demonstracao" target="_blank" class="btn btn-default text-uppercase bt-agendar-demonstracao">AGENDAR DEMONSTRAÇÃO</a> </div> </div> </div> <div class="moz moz-text moz-danger"> <div class="content" data-scroll-reveal="move 10px over 1s"> <div class="check-list"> <ul> <li>Dispensa estrutura de TI completa, estações potentes e licenciamento de software.</li> <li>Implantação: você pode utilizar todas as funcionalidades do sistema assim que contratar o serviço.</li> <li>Mais segurança e velocidade de processamento.</li> </ul> </div> </div> </div> </div>';
  return $res;
}
add_shortcode( 'globus_cloud', 'globus_cloud_func' );

function globus_mobile_func()
{
  $res = '<div class="moz-row"> <div class="moz moz-text moz-warning"> <div class="content" data-scroll-reveal="move 10px over 1s"> <h4>globus</h4> <h5>mobile</h5> <p>Mais inovação na gestão de sua transportadora. A BgmRodotec dispões de aplicativos para facilitar ainda mais o seu dia.</p> <!-- <a href="http://materiais.bgmrodotec.com.br/agendar-demonstracao" target="_blank" class="btn btn-default text-uppercase bt-agendar-demonstracao">AGENDAR DEMONSTRAÇÃO</a> --> <a href="'.get_bloginfo('url').'/globus/mobile/" class="btn btn-default btn-outline text-uppercase">saiba mais</a> </div> </div> <div class="moz" style="background-image: url('.get_bloginfo('template_url').'/images/background/globus_passageiros_moz2.jpg);"></div> </div>';
  return $res;
}
add_shortcode( 'globus_mobile', 'globus_mobile_func' );

function globus_consultoria_func()
{
  $res = '<div class="moz-row"> <div class="moz" style="background-image: url('.get_bloginfo('template_url').'/images/background/globus_passageiros_moz3.jpg);"></div> <div class="moz moz-text moz-green"> <div class="content" data-scroll-reveal="move 10px over 1s"> <h4>consultoria</h4> <h5>globus</h5> <p>Estabeleça uma gestão mais ágil e competitiva, em sintonia com a realidade atual.</p> <a href="'.get_bloginfo('url').'/contato/" class="btn btn-default text-uppercase">entrar em contato</a> </div> </div> </div>';
  return $res;
}
add_shortcode( 'globus_consultoria', 'globus_consultoria_func' );

function globus_parts_func()
{
  $res = ' <div class="moz-row"> <div class="moz" style="background-image: url('.get_bloginfo('template_url').'/images/background/tela-99kote.png); background-size: contain; background-color: #f2f2f2;"></div> <div class="moz moz-text moz-default"> <div class="content" data-scroll-reveal="move 10px over 1s"> <h5>99Kote</h5> <h4 style="margin-bottom: 10px;">A evolução do B2B</h4> <p>O ponto de encontro do comércio eletrônico para a compra e venda de peças e serviços para frota de empresas de transporte rodoviário de passageiros e de carga.</p> <a href="https://99kote.com.br/" target="_blank" class="btn btn-info text-uppercase">acessar plataforma</a> </div> </div> </div>';
  return $res;
}
add_shortcode( 'globus_parts', 'globus_parts_func' );

function globus_intelligence_func()
{
  $res = ' <div class="moz-row"> <div class="moz" style="background-image: url('.get_bloginfo('template_url').'/images/background/consultoria_moz1.jpg);"></div> <div class="moz moz-text bg-c7840f"> <div class="content" data-scroll-reveal="move 10px over 1s"> <h4>globus</h4> <h5>intelligence</h5> <p>Imagine um robô que monitorasse tudo na sua empresa. Da receita global ao custo de um parafuso. Tenha controle absoluto de todos os indicadores.</p> <a href="http://materiais.bgmrodotec.com.br/agendar-demonstracao" target="_blank" class="btn btn-default text-uppercase bt-agendar-demonstracao">AGENDAR DEMONSTRAÇÃO</a> </div> </div> </div>';
  return $res;
}
add_shortcode( 'globus_intelligence', 'globus_intelligence_func' );

// Pegando a categoria principal
function getMainCategory($post_id, $tax){

  $post_categories = get_the_terms($post_id, $tax);
  $cats = array();
  foreach($post_categories as $c){
      $cat = get_category( $c );

      if(($cat->slug != 'sem-categoria') && $cat->slug != 'destaque'){
          $cats[] = array( 'name' => $cat->name, 'slug' => $cat->slug, 'id' => $cat->term_id );
      }
  }
  return $cats;
}

// Setando post views
function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

// Retornando os post views
function wpb_get_post_views($postID){
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}

// Removendo o custom post type
add_action('admin_menu','remove_default_post_type');

function remove_default_post_type() {
  remove_menu_page('edit.php');
}

// Breadcrumbs
function bgmrodotec_breadcrumbs() {

    // Settings
    $separator          = '';
    $breadcrums_id      = 'breadcrumbs';
    $breadcrums_class   = 'breadcrumbs';
    $home_title         = 'Página inicial';

    // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
    $custom_taxonomy    = 'product_cat';

    // Get the query & post information
    global $post,$wp_query;

    // Do not display on the homepage
    if ( !is_front_page() ) {

        // Build the breadcrums
        echo ' <div class="breadcrumb"><div class="container"><i class="icon icon-circle-slelected"></i><ul id="' . $breadcrums_id . '" class="' . $breadcrums_class . '">';

        // Home page
        echo '<li class="item-home"><a class="bread-link bread-home" href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a></li>';
        echo '<li class="separator separator-home"> ' . $separator . ' </li>';

        if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {

            echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . post_type_archive_title($prefix, false) . '</strong></li>';

        } else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {

            // If post is a custom post type
            $post_type = get_post_type();

            // If it is a custom post type display name and link
            if($post_type != 'post') {

                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);

                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';

            }

            $custom_tax_name = get_queried_object()->name;
            echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . $custom_tax_name . '</strong></li>';

        } else if ( is_single() ) {

            // If post is a custom post type
            $post_type = get_post_type();

            // If it is a custom post type display name and link
            if($post_type != 'post') {

                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);

                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';

            }

            // Get post category info
            $category = get_the_category();

            if(!empty($category)) {

                // Get last category post is in
                $last_category = end(array_values($category));

                // Get parent any categories and create array
                $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
                $cat_parents = explode(',',$get_cat_parents);

                // Loop through parent categories and store in variable $cat_display
                $cat_display = '';
                foreach($cat_parents as $parents) {
                    $cat_display .= '<li class="item-cat">'.$parents.'</li>';
                    $cat_display .= '<li class="separator"> ' . $separator . ' </li>';
                }

            }

            // If it's a custom post type within a custom taxonomy
            $taxonomy_exists = taxonomy_exists($custom_taxonomy);
            if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {

                $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
                $cat_id         = $taxonomy_terms[0]->term_id;
                $cat_nicename   = $taxonomy_terms[0]->slug;
                $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                $cat_name       = $taxonomy_terms[0]->name;

            }

            // Check if the post is in a category
            if(!empty($last_category)) {
                echo $cat_display;
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';

            // Else if post is in a custom taxonomy
            } else if(!empty($cat_id)) {

                echo '<li class="item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . '"><a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . $cat_link . '" title="' . $cat_name . '">' . $cat_name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';

            } else {

                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';

            }

        } else if ( is_category() ) {

            // Category page
            echo '<li class="item-current item-cat"><strong class="bread-current bread-cat">' . single_cat_title('', false) . '</strong></li>';

        } else if ( is_page() ) {

            // Standard page
            if( $post->post_parent ){

                // If child page, get parents
                $anc = get_post_ancestors( $post->ID );

                // Get parents in the right order
                $anc = array_reverse($anc);

                // Parent page loop
                foreach ( $anc as $ancestor ) {
                    $parents .= '<li class="item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
                    $parents .= '<li class="separator separator-' . $ancestor . '"> ' . $separator . ' </li>';
                }

                // Display parent pages
                echo $parents;

                // Current page
                echo '<li class="item-current item-' . $post->ID . '"><strong title="' . get_the_title() . '"> ' . get_the_title() . '</strong></li>';

            } else {

                // Just display current page if not parents
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '"> ' . get_the_title() . '</strong></li>';

            }

        } else if ( is_tag() ) {

            // Tag page

            // Get tag information
            $term_id        = get_query_var('tag_id');
            $taxonomy       = 'post_tag';
            $args           = 'include=' . $term_id;
            $terms          = get_terms( $taxonomy, $args );
            $get_term_id    = $terms[0]->term_id;
            $get_term_slug  = $terms[0]->slug;
            $get_term_name  = $terms[0]->name;

            // Display the tag name
            echo '<li class="item-current item-tag-' . $get_term_id . ' item-tag-' . $get_term_slug . '"><strong class="bread-current bread-tag-' . $get_term_id . ' bread-tag-' . $get_term_slug . '">' . $get_term_name . '</strong></li>';

        } elseif ( is_day() ) {

            // Day archive

            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';

            // Month link
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><a class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('m') . '"> ' . $separator . ' </li>';

            // Day display
            echo '<li class="item-current item-' . get_the_time('j') . '"><strong class="bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</strong></li>';

        } else if ( is_month() ) {

            // Month Archive

            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';

            // Month display
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><strong class="bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</strong></li>';

        } else if ( is_year() ) {

            // Display year archive
            echo '<li class="item-current item-current-' . get_the_time('Y') . '"><strong class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</strong></li>';

        } else if ( is_author() ) {

            // Auhor archive

            // Get the author information
            global $author;
            $userdata = get_userdata( $author );

            // Display author name
            echo '<li class="item-current item-current-' . $userdata->user_nicename . '"><strong class="bread-current bread-current-' . $userdata->user_nicename . '" title="' . $userdata->display_name . '">' . 'Author: ' . $userdata->display_name . '</strong></li>';

        } else if ( get_query_var('paged') ) {

            // Paginated archives
            echo '<li class="item-current item-current-' . get_query_var('paged') . '"><strong class="bread-current bread-current-' . get_query_var('paged') . '" title="Page ' . get_query_var('paged') . '">'.__('Page') . ' ' . get_query_var('paged') . '</strong></li>';

        } else if ( is_search() ) {

            // Search results page
            echo '<li class="item-current item-current-' . get_search_query() . '"><strong class="bread-current bread-current-' . get_search_query() . '" title="Search results for: ' . get_search_query() . '">Search results for: ' . get_search_query() . '</strong></li>';

        } elseif ( is_404() ) {

            // 404 page
            echo '<li>' . 'Error 404' . '</li>';
        }

        echo '</ul></div></div>';

    }

}

function getMainTerm($link=null)
{
  global $post;
    $terms = get_the_terms($post->ID,'category_blog');

    if(empty($terms)){

      return false;
    }

    return $link == 'true' ? get_term_link($terms[0]->term_id) : $terms[0]->name;
}


function bgmrodotec_init_scripts() {
  // Arquivos CSS
  wp_enqueue_style( 'styles', get_template_directory_uri() . "/css/styles.css" );
  wp_enqueue_style( 'main', get_template_directory_uri() . "/css/main.css" );

  // Javascripts
  wp_deregister_script( 'scripts' );
  wp_enqueue_script( 'scripts', get_template_directory_uri() . "/js/scripts.js", array(), false, true );
  wp_enqueue_script( 'index', get_template_directory_uri() . "/js/index.js", array( 'scripts' ), false, true );
  wp_enqueue_script( 'rdstation', get_template_directory_uri() . "/js/rdstation.js", array( 'scripts' ), false, true );
}
add_action( 'wp_enqueue_scripts', 'bgmrodotec_init_scripts', 11 );


/*// Register Custom Post Type
function custom_post_type() {

  $labels = array(
    'name'                  => _x( 'Post Types', 'Post Type General Name', 'text_domain' ),
    'singular_name'         => _x( 'Post Type', 'Post Type Singular Name', 'text_domain' ),
    'menu_name'             => __( 'Post Types', 'text_domain' ),
    'name_admin_bar'        => __( 'Post Type', 'text_domain' ),
    'archives'              => __( 'Item Archives', 'text_domain' ),
    'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
    'all_items'             => __( 'All Items', 'text_domain' ),
    'add_new_item'          => __( 'Add New Item', 'text_domain' ),
    'add_new'               => __( 'Add New', 'text_domain' ),
    'new_item'              => __( 'New Item', 'text_domain' ),
    'edit_item'             => __( 'Edit Item', 'text_domain' ),
    'update_item'           => __( 'Update Item', 'text_domain' ),
    'view_item'             => __( 'View Item', 'text_domain' ),
    'search_items'          => __( 'Search Item', 'text_domain' ),
    'not_found'             => __( 'Not found', 'text_domain' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
    'featured_image'        => __( 'Featured Image', 'text_domain' ),
    'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
    'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
    'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
    'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
    'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
    'items_list'            => __( 'Items list', 'text_domain' ),
    'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
    'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
  );
  $args = array(
    'label'                 => __( 'Post Type', 'text_domain' ),
    'description'           => __( 'Post Type Description', 'text_domain' ),
    'labels'                => $labels,
    'supports'              => array( ),
    'taxonomies'            => array( 'category', 'post_tag' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
  );
  register_post_type( 'post_type', $args );

}
add_action( 'init', 'custom_post_type', 0 );



// Register Custom Taxonomy
function custom_taxonomy() {

  $labels = array(
    'name'                       => _x( 'Taxonomies', 'Taxonomy General Name', 'text_domain' ),
    'singular_name'              => _x( 'Taxonomy', 'Taxonomy Singular Name', 'text_domain' ),
    'menu_name'                  => __( 'Taxonomy', 'text_domain' ),
    'all_items'                  => __( 'All Items', 'text_domain' ),
    'parent_item'                => __( 'Parent Item', 'text_domain' ),
    'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
    'new_item_name'              => __( 'New Item Name', 'text_domain' ),
    'add_new_item'               => __( 'Add New Item', 'text_domain' ),
    'edit_item'                  => __( 'Edit Item', 'text_domain' ),
    'update_item'                => __( 'Update Item', 'text_domain' ),
    'view_item'                  => __( 'View Item', 'text_domain' ),
    'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
    'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
    'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
    'popular_items'              => __( 'Popular Items', 'text_domain' ),
    'search_items'               => __( 'Search Items', 'text_domain' ),
    'not_found'                  => __( 'Not Found', 'text_domain' ),
    'no_terms'                   => __( 'No items', 'text_domain' ),
    'items_list'                 => __( 'Items list', 'text_domain' ),
    'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => false,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
  );
  register_taxonomy( 'taxonomy', array( 'post' ), $args );

}
add_action( 'init', 'custom_taxonomy', 0 );




*/

?>
