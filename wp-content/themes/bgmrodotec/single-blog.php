<?php get_header("internal"); the_post(); wpb_set_post_views(get_the_ID()); ?>
<section class="blog">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="title_internal_blog">Blog</h2>
            </div>
            <div class="col-sm-8">
                <div class="box-blog">
                    <div class="box-header" style="background-image:url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(),'normal')[0]; ?>);" title="<?php echo get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true); ?>">
                        <div class="date">
                            <p><?php echo get_the_date('d'); ?></p>
                            <p><?php echo get_the_date('M'); ?></p>
                        </div>
                    </div>
                    <div class="box-body">
                        <h1><?php the_title(); ?></h1>
                        <p><?php the_date(); ?> 
                        <?php if(getMainTerm() !== false): ?>
                        | assunto <a href="<?php echo getMainTerm('true'); ?>"><?php echo getMainTerm(); ?></a>
                                <?php endif; ?>
                        </p>
                    </div>
                    <div class="box-footer">
                        <?php the_content(); ?>
                        <div class="row">
                            <div class="col-sm-12">
                            <?php $tags = get_the_terms($post->ID,'blog_tag'); if(!empty($tags)): ?>
                                <div class="tags pull-left" style="position: relative; top: 10px;">
                                    <span>tags:</span> 
                                             <?php $tags = get_the_terms($post->ID,'blog_tag'); 
                                        
                                        foreach ($tags as $tag): ?>
                                        <a href="<?php bloginfo('url'); ?>/tag/<?php echo $tag->slug; ?>"><?php echo $tag->name; ?></a>
                                       <?php endforeach; ?>
                                </div>
                                <?php endif;  ?>
                                <div class="social-networks pull-right">
                                    <div>
                                            <a onclick="javascript:window.open(this.href,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=500,height=300'); return false;" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_the_permalink(); ?>"><i class="fa fa-facebook"></i></a>
                                            <a onclick="javascript:window.open(this.href,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=500,height=300'); return false;" href="https://twitter.com/home?status=<?php echo get_the_title().' - '.get_the_permalink(); ?>"><i class="fa fa-twitter"></i></a>
                                            <!--<a onclick="javascript:window.open(this.href,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=500,height=300'); return false;" data-notes="<?php echo get_the_permalink(); ?>" href="https://embed.tumblr.com/share"><i class="fa fa-tumblr"></i></a>-->
                                            <a onclick="javascript:window.open(this.href,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=500,height=300'); return false;" href="https://plus.google.com/share?url=<?php echo get_the_permalink(); ?>"><i class="fa fa-google-plus"></i></a>
                                            <a onclick="javascript:window.open(this.href,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=500,height=300'); return false;" href="https://www.linkedin.com/cws/share?url=<?php echo get_the_permalink(); ?>"><i class="fa fa-linkedin"></i></a>
                                            <a onclick="javascript:window.open(this.href,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=500,height=300'); return false;" href="whatsapp://send?text=<?php echo get_the_title().' - '.get_the_permalink(); ?>"><i class="fa fa-whatsapp"></i></a>
                                            <!--<a href="#" class="share"><i class="fa fa-share-alt"></i></a>-->
                                            <script>!function(d,s,id){var js,ajs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://assets.tumblr.com/share-button.js";ajs.parentNode.insertBefore(js,ajs);}}(document, "script", "tumblr-js");</script>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <!-- <a href="#" class="btn btn-danger btn-block text-uppercase ver-mais">ver mais matérias</a> -->
                    </div>
                </div>
            </div>
            <?php get_template_part('includes/content', 'sidebar'); // blog sidebar ?>
          <!--  <div class="col-sm-4">
                <div class="panel-list">
                    <div class="panel-header">
                        categorias
                    </div>
                    <div class="panel-body">
                        <ul>
                            <li>GESTÃO DE CUSTOS (28)</li>
                            <li>OBRIGAÇÕES FISCAIS E LEGAIS  (95)</li>
                            <li>PRODUTOS E SERVIÇOS  (65)</li>
                            <li>notícias (32)</li>
                        </ul>
                    </div>
                </div>
                <div class="panel-list">
                    <div class="panel-header">
                        MAIS VISTOS
                    </div>
                    <div class="panel-body">
                        <ul>
                            <li>
                                <h4>CONTROLE PNEUS E REDUZA O CUSTO COM A MANUTENÇÃO DA SUA FROTA</h4>
                                <span>08/04/2016</span>
                            </li>
                            <li>
                                <h4>CONTROLE PNEUS E REDUZA O CUSTO COM A MANUTENÇÃO DA SUA FROTA</h4>
                                <span>08/04/2016</span>
                            </li>
                            <li>
                                <h4>CONTROLE PNEUS E REDUZA O CUSTO COM A MANUTENÇÃO DA SUA FROTA</h4>
                                <span>08/04/2016</span>
                            </li>
                            <li>
                                <h4>CONTROLE PNEUS E REDUZA O CUSTO COM A MANUTENÇÃO DA SUA FROTA</h4>
                                <span>08/04/2016</span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="panel-list">
                    <div class="panel-header">
                        tags
                    </div>
                    <div class="panel-body">
                        <div class="tags">
                            <a href="#">tag</a>, <a href="#">tag</a>, <a href="#">tag</a>, <a href="#">tag</a>, <a href="#">tag</a>, <a href="#">tag</a>, <a href="#">tag</a>, <a href="#">tag</a>, <a href="#">tag</a>, <a href="#">tag</a>, <a href="#">tag</a>, <a href="#">tag</a>, <a href="#">tag</a>, <a href="#">tag</a>, <a href="#">tag</a>, <a href="#">tag</a>
                        </div>
                    </div>
                </div>
            </div>-->
        </div>
    </div>
</section>
<?php get_template_part('includes/content','newsletter'); ?>
<?php get_footer(); ?>