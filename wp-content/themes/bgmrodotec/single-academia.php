<?php get_header(); the_post(); ?>
<section class="banner banner-interna" style="background-image: url(<?php bloginfo('template_url'); ?>/images/banner/academiaglobus.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-sm-11">
                <div class="text" data-scroll-reveal="move 20px">
                    <h1>CURSOS PRESENCIAIS</h1>
                    <h2>
                        A BgmRodotec promove treinamentos para capacitar os usuários na utilização dos recursos do software Globus.
                    </h2>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="academia-globus">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 detalhes">
                <a href="<?php bloginfo('url') ?>/cursos-presenciais" class="btn btn-dark btn-outline text-uppercase top">< voltar para cursos presenciais</a>

                <div>
                    <h3><?php the_title(); ?></h3>

                    <?php the_content(); ?>
                </div>

                <div>
                    <a href="<?php echo get_field('link_botao'); ?>" class="btn btn-success btn-blck text-uppercase" target="_blank">Tenho Interesse</a>
                </div>

                <div>
                    <a href="<?php bloginfo('url') ?>/cursos-presenciais" class="btn btn-dark btn-outline text-uppercase bottom">< voltar para cursos presenciais</a>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_template_part('includes/content','newsletter'); ?>
<?php get_footer(); ?>
