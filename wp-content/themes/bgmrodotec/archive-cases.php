<?php get_header(); ?>
 <section class="banner banner-interna" style="background-image: url(<?php bloginfo('template_url'); ?>/images/banner/cases.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-sm-11">
                    <div class="text" data-scroll-reveal="move 20px">
                        <h1>CASES</h1>
                        <h2>
                            A BgmRodotec coleciona histórias de sucesso com seus parceiros, sempre com soluções inovadoras.  
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
<div class="cases-content">
    <?php 
    while(have_posts()): the_post();
        if( get_field('template') == '2'): 
            get_template_part('includes/cases/content','withbg'); 
        else:
            get_template_part('includes/cases/content','nobg'); 
        endif;
    endwhile;
    ?>
</div>
    <?php get_template_part('includes/content','newsletter'); //NEWSLETTER ?>
<?php get_footer(); ?>