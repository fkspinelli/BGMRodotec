<!DOCTYPE html>
<html lang="pt-br">

<head>
    <title><?php wp_title(); ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="PwdQ0NATk42_BXrcXKnLDDO2tTe8WStWeQ_jwImtfDc" />

    <!--[if lt IE 9]>
        <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/images/icon/favicon.ico">

    <?php wp_head(); ?>

    <style type="text/css">html{margin-top:0!important;}</style>

    <?php if (!isset($_SERVER['HTTP_USER_AGENT']) || stripos($_SERVER['HTTP_USER_AGENT'], 'Speed Insights') === false): ?>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-772002-1', 'auto');
      ga('send', 'pageview');

    </script>

    ﻿<!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '1842201926053217'); // Insert your pixel ID here.
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=1842201926053217&ev=PageView&noscript=1";
    /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->
    <?php endif; ?>



</head>

<body style="margin-top: -20px;">
<?php

/*if(get_post_type() == 'clientes' || get_post_type() == 'cases' || is_archive('academia') ):
    $nav_class = 'navbar-fixed-top';
elseif(is_category('blog') || is_archive('blog') || is_tax('category_blog') || is_search() || is_singular('blog')):
    $nav_class = 'navbar-image';
else:
     $nav_class = 'navbar-fixed-top';
endif;*/
?>
    <nav class="navbar navbar-default navbar-fixed-top <?php //echo $nav_class; ?>">
        <div class="pattern"></div>
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php bloginfo('url'); ?>" title="BgmRodotec">
                    <img src="<?php bloginfo('template_url'); ?>/images/logo/logo-bgm-rodotec.png" class="img-responsive" alt="BgmRodotec">
                </a>
            </div>
            <div class="collapse navbar-collapse" id="navbar">
                <div class="navbar-top">
                    <form action="<?php bloginfo('url'); ?>" method="get" class="form-inline" role="form">
                        <a href="http://99kote.com.br/" target="_blank" class="btn btn-primary btn-sm text-uppercase">Conheça o 99kote!</a>
                        <a href="http://portaldocliente.bgmrodotec.com.br/" target="_blank" class="btn btn-default btn-sm btn-outline text-uppercase" data-toggle="popover" data-trigger="hover" data-placement="bottom" title="Já é cliente Globus?" data-content="Acesse o Portal do cliente">Portal do Cliente</a>
                        <div class="form-group inner-addon inner-addon-sm right-addon">
                            <i class="fa fa-search" aria-hidden="true"></i>
                            <input type="text" value="<?php echo $_GET['s']; ?>" name="s" class="form-control input-sm border-none" />
                        </div>
                    </form>
                </div>
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="<?php bloginfo('url'); ?>/empresa" title="Empresa">Empresa</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" title="Soluções">Soluções</a>
                        <ul class="dropdown-menu clearfix" style="background-color: #fff;">
                            <li>
                                <ul>
                                    <li>
                                        <a href="<?php bloginfo('url'); ?>/globus" title="Globus">Globus</a>
                                    </li>
                                    <li>
                                        <a href="<?php bloginfo('url'); ?>/globus/intelligence" title="Globus Intelligence">Globus Intelligence</a>
                                    </li>
                                    <li>
                                        <a href="<?php bloginfo('url'); ?>/consultoria" title="Consultoria">Consultoria</a>
                                    </li>
                                </ul>
                            </li>
                            <?php $ags = array('post_type'=>'banner_menu'); query_posts($ags); while(have_posts()): the_post(); ?>
                            <li style="background-image:url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(),'normal')[0]; ?>);">
                                <?php the_content(); ?>
                                <a href="<?php echo get_field('link_externo'); ?>" class="btn btn-default btn-sm btn-outline text-uppercase size-12" style="width: 94px;padding: 5px;font-size:12px;">saiba mais</a>
                            </li>
                        <?php endwhile; wp_reset_query(); ?>
                        </ul>
                    </li>
                    <li><a href="<?php bloginfo('url'); ?>/clientes" title="Clientes">Clientes</a></li>
                    <li><a href="<?php bloginfo('url'); ?>/cases" title="Cases">Cases</a></li>
                    <li><a href="<?php bloginfo('url'); ?>/blog" title="Blog">Blog</a></li>
                    <li><a href="<?php bloginfo('url'); ?>/biblioteca" title="Biblioteca">Biblioteca</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" title="Capacitação">Capacitação</a>
                        <ul class="dropdown-menu clearfix clean" style="background-color: #fff;right: 0;">
                            <li style="width: 100%;">
                                <ul>
                                    <li>
                                        <a href="<?php bloginfo('url'); ?>/cursos-presenciais" title="Cursos Presenciais">Cursos Presenciais</a>
                                    </li>
                                    <li>
                                        <a href="https://eadglobus.maestrus.com/home/" title="Cursos a Distância (EAD)" target="_blank">Cursos a Distância (EAD)</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li><a href="<?php bloginfo('url'); ?>/contato" title="Contato">Contato</a></li>
                </ul>
            </div>
        </div>
    </nav>
