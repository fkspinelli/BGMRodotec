<?php get_header(); the_post(); ?>
    <section class="banner banner-interna" style="background-image: url(<?php bloginfo('template_url'); ?>/images/banner/contato.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-sm-11">
                    <div class="text" data-scroll-reveal="move 20px">
                        <h1>CONTATO</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="contato">
        <div class="container"> 
            <div class="row">
                <div class="col-sm-6"> 
                   <?php echo get_field('form_de_contato'); ?>
                </div>
                <div class="col-sm-5 col-sm-push-1">
                 <a href="http://portaldocliente.bgmrodotec.com.br/" target="_blank" class="suporte">
                        <div class="row">
                            <div class="col-xs-4">
                                <img draggable="false" src="<?php bloginfo('template_url'); ?>/images/icon/suporte.png">
                            </div>
                            <div class="col-xs-8">
                                <p>Para <b>suporte técnico</b>, <u>clique aqui</u>.</p>
                            </div>
                        </div>
                    </a>
                     <?php the_content(); ?>
                </div>
            </div>
        </div>
    </section>s

<?php get_template_part('includes/content','newsletter'); //NEWSLETTER ?>
<?php get_footer(); ?>