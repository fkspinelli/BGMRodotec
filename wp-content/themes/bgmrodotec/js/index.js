$(document).ready(function() {

    if(!isMobile()){
        //scrollReveal
        // window.scrollReveal = new scrollReveal(); 

        // parallax
        $.stellar.positionProperty.horizontalParallax = {
            setTop: function($el, newTop, originalTop) {
                if(newTop > 0){
                    $el.addClass('display-block');
                    $el.css({
                        'left': $el.hasClass('horizontal-parallax-left') ? originalTop - newTop : 'initial',
                        'right': $el.hasClass('horizontal-parallax-right') ? originalTop - newTop : 'initial'
                    });
                }
            }
        };
        
        $.stellar({
            horizontalScrolling: false,
            positionProperty: 'horizontalParallax'
        });

    }

    $('[data-toggle="popover"]').popover(); 

    $(window).on('load resize', function(){
        moduloBodyHeight();
    });

    function moduloBodyHeight() {
        var height = 0;
        $('.modulo-body').each(function(){
            if($(this).height() > height){
                height = $(this).height();
            }
        });
        $('.modulo-body').height(height+40);
        // $('.background-left').css({'width': $('.background-left').parents('[class*="col-"]').offset().left + $('.background-left').parents('[class*="col-"]').width() +15 });
    }

    $('.navbar .navbar-nav>li').append('<span></span>');

    var navbarDefault = $('.navbar-default');

    // $(document).scroll(function(){
    //     var top = $('body').scrollTop();
    //     if( top > $('.navbar').height() ){
    //         navbarDefault.addClass('gradient')
    //     }else{
    //         if($('.navbar-toggle').hasClass('collapsed')){
    //             navbarDefault.removeClass('gradient')
    //         }
    //     }
    // });

    // $(".collapse").on('show.bs.collapse', function(){
    //     navbarDefault.addClass('gradient')
    // }).on('hide.bs.collapse', function(){
    //     var top = $('body').scrollTop();
    //     if( top > $('.navbar').height() ){
    //         navbarDefault.addClass('gradient')
    //     }else{
    //         navbarDefault.removeClass('gradient')
    //     }
    // });





    // soluções integradas
    $('.solucoes-integradas-detalhes .btns a').click(function(event){
        event.preventDefault();
        var tipo = $(this).prop('className');
        $('.solucoes-integradas-detalhes .btns a').removeClass('active');
        $(this).addClass('active');
        $('.box').removeClass('active');
        $('.box.'+tipo).addClass('active');
    });

    $('.solucoes-integradas-detalhes .box').click(function(event){
        var tipo = $(this).attr('data-group');
        $('.solucoes-integradas-detalhes .btns a').removeClass('active');
        $('.solucoes-integradas-detalhes .btns a.'+tipo).addClass('active');
        $('.box').removeClass('active');
        $('.box.'+tipo).addClass('active');
    });

    $('.clientes .box-cliente a').click(function(event){

        event.preventDefault();
        var clientId = $(this).attr('href');

        if($(clientId).index() != '-1'){
            $('html, body').animate({
             scrollTop: $('#depoimentos-clientes').offset().top - $('.navbar').height()
            }, 1000);

            $("#depoimentos-clientes").carousel($(clientId).index());
        }
    });

    $('#carouselBiblioteca .links a').click(function(event){

        event.preventDefault();
        var boxId = $(this).attr('href');

        $('html, body').animate({
         scrollTop: $(boxId).offset().top - $('.navbar').height()
        }, 1000);
    });

    $(document).on('click', '.bt-agendar-demonstracao', function(){
        ga('send', 'event', 'link','click', 'Botão agendar demonstração clicado');
    });

    $('.scroll').jscroll({
        autoTrigger: false,
        nextSelector: '.next-page',
        contentSelector: '.scroll',
        loadingHtml: '<img src="/wp-content/themes/bgmrodotec/images/icon/reload.gif">',
        callback: function(){

        }
    });

    $('.scroll').jscroll({
        autoTrigger: false,
        nextSelector: '.next-page',
        contentSelector: '.scroll',
        loadingHtml: '<img src="/wp-content/themes/bgmrodotec/images/icon/reload.gif">',
        callback: function(){

        }
    });

    $('.scroll-academia').jscroll({
        autoTrigger: false,
        nextSelector: '.next-page-academia',
        contentSelector: '.scroll-academia',
        loadingHtml: '<img src="/wp-content/themes/bgmrodotec/images/icon/reload-2.gif">',
        callback: function(){
            $('.jscroll-added > .scroll-academia > .col-sm-4').unwrap();
            $('.jscroll-added > .col-sm-4').unwrap();
            $('.jscroll-inner > .row:eq(0)').remove();
            // $('.jscroll-added > .scroll-academia > .col-sm-4').unwrap();
            moduloBodyHeight();
        }
    });

    $(window).on('load resize', function(){
        if ($('.background-left').size() > 0) {
            $('.background-left').css({'width': $('.background-left').parents('[class*="col-"]').offset().left + $('.background-left').parents('[class*="col-"]').width() +15 });
        }
    });

    $('.submit-ebooks').click(function(){
        if (
                localStorage.ebookNome == undefined
            &&  localStorage.ebookEmail == undefined
            &&  localStorage.ebookEmpresa == undefined
            &&  localStorage.ebookCargo == undefined
            &&  localStorage.ebookTelefone == undefined
            &&  localStorage.ebookCliente == undefined
            &&  localStorage.ebookCaptcha == undefined
            )
        {
            localStorage.ebookNome = $('.form-materiais-ebooks [name="nome"]').val()
            localStorage.ebookEmail = $('.form-materiais-ebooks [name="email"]').val()
            localStorage.ebookEmpresa = $('.form-materiais-ebooks [name="empresa"]').val()
            localStorage.ebookCargo = $('.form-materiais-ebooks [name="cargo"]').val()
            localStorage.ebookTelefone = $('.form-materiais-ebooks [name="telefone"]').val()
            localStorage.ebookCliente = $('.form-materiais-ebooks [name="cliente"]').val()
            localStorage.ebookCaptcha = $('.form-materiais-ebooks [name="captcha"]').val()
        }
    });

    $('.submit-materiais-graficos').click(function(){
        if (
                localStorage.materialGraficoNome == undefined
            &&  localStorage.materialGraficoEmail == undefined
            &&  localStorage.materialGraficoEmpresa == undefined
            &&  localStorage.materialGraficoCargo == undefined
            &&  localStorage.materialGraficoTelefone == undefined
            &&  localStorage.materialGraficoCliente == undefined
            &&  localStorage.materialGraficoCaptcha == undefined
            )
        {
            localStorage.materialGraficoNome = $('.form-materiais-graficos [name="nome"]').val()
            localStorage.materialGraficoEmail = $('.form-materiais-graficos [name="email"]').val()
            localStorage.materialGraficoEmpresa = $('.form-materiais-graficos [name="empresa"]').val()
            localStorage.materialGraficoCargo = $('.form-materiais-graficos [name="cargo"]').val()
            localStorage.materialGraficoTelefone = $('.form-materiais-graficos [name="telefone"]').val()
            localStorage.materialGraficoCliente = $('.form-materiais-graficos [name="cliente"]').val()
            localStorage.materialGraficoCaptcha = $('.form-materiais-graficos [name="captcha"]').val()
        }
    });


    $(document).on('click', '#modalWebinars [type="submit"]', function(){
        if (
                localStorage.webinarNome == undefined
            &&  localStorage.webinarEmail == undefined
            &&  localStorage.webinarEmpresa == undefined
            &&  localStorage.webinarCargo == undefined
            )
        {

            localStorage.webinarNome = $('#modalWebinars [name="nome"]').val()
            localStorage.webinarEmail = $('#modalWebinars [name="email"]').val()
            localStorage.webinarEmpresa = $('#modalWebinars [name="empresa"]').val()
            localStorage.webinarCargo = $('#modalWebinars [name="cargo"]').val()
        }
    });


    $('.btn-ebook').click(function() {   
        $('.form-materiais-ebooks [name="nome"]').val(localStorage.ebookNome)
        $('.form-materiais-ebooks [name="email"]').val(localStorage.ebookEmail)
        $('.form-materiais-ebooks [name="empresa"]').val(localStorage.ebookEmpresa)
        $('.form-materiais-ebooks [name="cargo"]').val(localStorage.ebookCargo)
        $('.form-materiais-ebooks [name="telefone"]').val(localStorage.ebookTelefone)
        $('.form-materiais-ebooks [name="cliente"]').val(localStorage.ebookCliente)
        $('.form-materiais-ebooks [name="captcha"]').val(localStorage.ebookCaptcha)
        $('.form-materiais-ebooks [name="ebook"]').val($(this).attr('data-download'))


        $('.submit-ebooks').click()
        window.open($(this).attr('data-href'),'_blank');
    });

    $('.btn-material-grafico').click(function() {   
        $('.form-materiais-graficos [name="nome"]').val(localStorage.materialGraficoNome)
        $('.form-materiais-graficos [name="email"]').val(localStorage.materialGraficoEmail)
        $('.form-materiais-graficos [name="empresa"]').val(localStorage.materialGraficoEmpresa)
        $('.form-materiais-graficos [name="cargo"]').val(localStorage.materialGraficoCargo)
        $('.form-materiais-graficos [name="telefone"]').val(localStorage.materialGraficoTelefone)
        $('.form-materiais-graficos [name="cliente"]').val(localStorage.materialGraficoCliente)
        $('.form-materiais-graficos [name="captcha"]').val(localStorage.materialGraficoCaptcha)
        $('.form-materiais-graficos [name="ebook"]').val($(this).attr('data-download'))


        $('.submit-materiais-graficos').click()
        window.open($(this).attr('data-href'),'_blank');
    });

});

$(function(){
    
    var randomNr1 = 0; 
    var randomNr2 = 0;
    var totalNr = 0;

    if (localStorage.ebookEmail == undefined) {
        $('.submit-ebooks').parents('form').find('[type=submit]').attr('disabled','disabled');
    }

    randomNr1 = Math.floor(Math.random()*10);
    randomNr2 = Math.floor(Math.random()*10);
    totalNr = randomNr1 + randomNr2;
    var texti = +randomNr1+" + "+randomNr2+" = ?";
    $('.form-materiais-ebooks .captcha label').text(texti);
    
  
    $('.form-materiais-ebooks input[name="captcha"]').keyup(function(){

      var nr = $(this).val();
      if(nr==totalNr)
      {
        $('.form-materiais-ebooks .submit-ebooks').removeAttr('disabled');       
      }
      else{
        if (localStorage.ebookEmail == undefined) {
            $('.form-materiais-ebooks .submit-ebooks').attr('disabled','disabled');
        }
      }
      
    });

});

$(function(){
    
    var randomNr1 = 0; 
    var randomNr2 = 0;
    var totalNr = 0;

    if (localStorage.materialGraficoEmail == undefined) {
        $('.submit-materiais-graficos').parents('form').find('[type=submit]').attr('disabled','disabled');
    }

    randomNr1 = Math.floor(Math.random()*10);
    randomNr2 = Math.floor(Math.random()*10);
    totalNr = randomNr1 + randomNr2;
    var texti = +randomNr1+" + "+randomNr2+" = ?";
    $('.form-materiais-graficos .captcha label').text(texti);
    
  
    $('.form-materiais-graficos input[name="captcha"]').keyup(function(){

      var nr = $(this).val();
      if(nr==totalNr)
      {
        $('.form-materiais-graficos .submit-materiais-graficos').removeAttr('disabled');       
      }
      else{
        if (localStorage.materialGraficoEmail == undefined) {
            $('.form-materiais-graficos .submit-materiais-graficos').attr('disabled','disabled');
        }
      }
      
    });

});

function isMobile() { 
 if( navigator.userAgent.match(/Android/i)
 || navigator.userAgent.match(/webOS/i)
 || navigator.userAgent.match(/iPhone/i)
 || navigator.userAgent.match(/iPad/i)
 || navigator.userAgent.match(/iPod/i)
 || navigator.userAgent.match(/BlackBerry/i)
 || navigator.userAgent.match(/Windows Phone/i)
 ){
    return true;
  }
 else {
    return false;
  }
}

$(document).ready(function(){
    if (
            localStorage.ebookNome != undefined
        &&  localStorage.ebookEmail != undefined
        &&  localStorage.ebookEmpresa != undefined
        &&  localStorage.ebookCargo != undefined
        &&  localStorage.ebookTelefone != undefined
        &&  localStorage.ebookCliente != undefined
        &&  localStorage.ebookCaptcha != undefined
        )
    {
        enebleEbooks();
        $('.form-materiais-ebooks [name="nome"]').val(localStorage.ebookNome)
        $('.form-materiais-ebooks [name="email"]').val(localStorage.ebookEmail)
        $('.form-materiais-ebooks [name="empresa"]').val(localStorage.ebookEmpresa)
        $('.form-materiais-ebooks [name="cargo"]').val(localStorage.ebookCargo)
        $('.form-materiais-ebooks [name="telefone"]').val(localStorage.ebookTelefone)
        $('.form-materiais-ebooks [name="cliente"]').val(localStorage.ebookCliente)
        $('.form-materiais-ebooks [name="captcha"]').val(localStorage.ebookCaptcha)
    }

    if (
            localStorage.materialGraficoNome != undefined
        &&  localStorage.materialGraficoEmail != undefined
        &&  localStorage.materialGraficoEmpresa != undefined
        &&  localStorage.materialGraficoCargo != undefined
        &&  localStorage.materialGraficoTelefone != undefined
        &&  localStorage.materialGraficoCliente != undefined
        &&  localStorage.materialGraficoCaptcha != undefined
        )
    {
        enebleMateriaisGraficos();
        $('.form-materiais-graficos [name="nome"]').val(localStorage.materialGraficoNome)
        $('.form-materiais-graficos [name="email"]').val(localStorage.materialGraficoEmail)
        $('.form-materiais-graficos [name="empresa"]').val(localStorage.materialGraficoEmpresa)
        $('.form-materiais-graficos [name="cargo"]').val(localStorage.materialGraficoCargo)
        $('.form-materiais-graficos [name="telefone"]').val(localStorage.materialGraficoTelefone)
        $('.form-materiais-graficos [name="cliente"]').val(localStorage.materialGraficoCliente)
        $('.form-materiais-graficos [name="captcha"]').val(localStorage.materialGraficoCaptcha)
    }
});

function enebleEbooks() {
    $('.btn-ebook').removeClass('disabled')
    $('.form-materiais-ebooks input').prop('readonly', true);
    $('.box-button-ebook').hide()
    $('.ebook-success.ebook').fadeIn();
    $('.form-materiais-ebooks .form-group.captcha').hide();
}

function enebleMateriaisGraficos() {
    $('.btn-material-grafico').removeClass('disabled')
    $('.form-materiais-graficos input').prop('readonly', true);
    $('.box-button-material-grafico').hide()
    $('.ebook-success.material-grafico').fadeIn();
    $('.form-materiais-graficos .form-group.captcha').hide();
}

$(document).ready(function(){
    $('.content-video:not(.cv2) li a').click(function(e){
        e.preventDefault();
        var youtubeId = $(this).attr('data-youtube-id');
        $(this).parents('.content-video').find('iframe').attr('src', 'https://www.youtube.com/embed/'+youtubeId)
    });
})



$(document).ready(function(){
    $('#webinars .content-video.cv2 li a').click(function(e){
        e.preventDefault();
        var youtubeId = $(this).attr('data-youtube-id');
        var player = $(this).parents('.content-video').find('iframe').prop('id')
        var title = $(this).find('.video-title').text()
        switch (player) {
            case 'player_2':
                player = player_2
                break;
        }
        player.loadVideoById(youtubeId, 0, "large")
        $(this).parents('.content-video').find('.input-video-title').val(title)
    });
})




// video - bibliteca
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var player_1;
var player_2;
function onYouTubeIframeAPIReady() {
    player_1 = new YT.Player('player_1', {
      height: '360',
      width: '640',
      videoId: $('#player_1').attr('data-src')
    });
    player_2 = new YT.Player('player_2', {
      height: '360',
      width: '640',
      videoId: $('#player_2').attr('data-src'),
      events: {
        'onStateChange': onPlayerStateChange
      }
    });
}

function onPlayerStateChange(event) {

    if (event.data == 1) {
        // verifica se usuário já preencheu o formulário
        if (
                localStorage.webinarNome != undefined
            &&  localStorage.webinarEmail != undefined
            &&  localStorage.webinarEmpresa != undefined
            &&  localStorage.webinarCargo != undefined
            )
        {
            $('#modalWebinars [name="nome"]').val(localStorage.webinarNome)
            $('#modalWebinars [name="email"]').val(localStorage.webinarEmail)
            $('#modalWebinars [name="empresa"]').val(localStorage.webinarEmpresa)
            $('#modalWebinars [name="cargo"]').val(localStorage.webinarCargo)
            $('#modalWebinars [name="title"]').val($(player_2.a).parents('.content-video').find('.input-video-title').val())
            $('#modalWebinars [type="submit"]').click()
        }
    }

    var limit = 10
    setInterval(function(){
        if (event.data == 1) {
            if(player_2.getCurrentTime() >= limit && localStorage.webinarEmail == undefined) {
                var videoTitle = $(player_2.a).parents('.content-video').find('.input-video-title').val()
                var list = String(localStorage.player_2).split(',')
                var l = list.indexOf(videoTitle);
                if (l == -1) {
                    player_2.stopVideo()
                    $('#modalWebinars').modal('show').find('[name="title"]').val($(player_2.a).parents('.content-video').find('.input-video-title').val());
                }
            }
        }
    },1000);
}

//chamada pelo plugin contact form 7
function enablePlayer2() {
    var video = $('#webinars .input-video-title').val()
    localStorage.player_2 += ','+video
    $('#modalWebinars').modal('hide')
}

// if ($('#modalVideoHome').size()) {

//     $('#modalVideoHome').modal('show');

//     document.getElementById('modalVideoHomePlay').play();
// }

// $('#modalVideoHome').on('hide.bs.modal', function () {
//     document.getElementById('modalVideoHomePlay').pause();
// });