<?php get_header("internal"); 
    global $query_string;

$query_args = explode("&", $query_string);
$search_query = array();

if( strlen($query_string) > 0 ) {
    foreach($query_args as $key => $string) {
        $query_split = explode("=", $string);
        $search_query[$query_split[0]] = urldecode($query_split[1]);
    } // foreach
} //if

$search = new WP_Query($search_query);

?>
 <section class="busca">
        <div class="container">
            <h1>RESULTADOS PARA: <a><?php echo $_GET['s']; ?></a></h1>
            <div class="row">
                <div class="col-sm-8">
                    <?php while($search->have_posts()): $search->the_post(); ?>
                    <div class="panel panel-default">
                        <div class="panel-header">
                            <h4><?php the_title(); ?></h4>
                        </div>
                        <div class="panel-body">
                           <?php the_excerpt(); ?>
                            <a href="<?php the_permalink(); ?>"><?php the_permalink(); ?></a>
                        </div>
                    </div>

                    <?php endwhile; ?>

                    <?php the_posts_pagination($search_query); ?>
                   <!-- <ul class="breadcrumb">
                        <li><a href="#"><i class="fa fa-chevron-left" aria-hidden="true"></i></a></li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">...</a></li>
                        <li><a href="#">8</a></li>
                        <li><a href="#">9</a></li>
                        <li><a href="#">10</a></li>
                        <li><a href="#"><i class="fa fa-chevron-right" aria-hidden="true"></i></a></li>
                    </ul>-->

                </div>
                <div class="col-sm-4">
                    <div class="panel panel-default">
                        <div class="panel-header">
                            <h4><?php echo $search->found_posts; ?> resultado<?php echo $search->found_posts > 1 ? 's' : null; ?></h4>
                        </div>
                       <!-- <div class="panel-body">
                            <h6>resultados em</h6>
                            <ul>
                                <li><a href="#">globus</a></li>
                                <li><a href="#">globus intelligence</a></li>
                                <li><a href="#">cases</a></li>
                                <li><a href="#">blog</a></li>
                                <li>
                                    <ul>
                                        <li><a href="#">gestões de custo</a></li>
                                        <li><a href="#">produtos e serviços</a></li>
                                        <li><a href="#">obrigações fiscais e legais</a></li>
                                        <li><a href="#">notícias</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>-->
                    </div>
                </div>
            </div>
        </div>
    </section>
     <?php get_template_part('includes/content','newsletter'); //NEWSLETTER ?>
<?php get_footer(); ?>