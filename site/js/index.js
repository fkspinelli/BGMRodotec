$(document).ready(function() {
    $(window).on('load resize', function(){
        var height = 0;
        $('.modulo-body').each(function(){
            if($(this).height() > height){
                height = $(this).height();
            }
        });
        $('.modulo-body').height(height+40);
        $('.background-left').css({'width': $('.background-left').parents('[class*="col-"]').offset().left + $('.background-left').parents('[class*="col-"]').width() +15 });
    });

    $('.navbar .navbar-nav>li').append('<span></span>');

    var navbarDefault = $('.navbar-default');

    // $(document).scroll(function(){
    //     var top = $('body').scrollTop();
    //     if( top > $('.navbar').height() ){
    //         navbarDefault.addClass('gradient')
    //     }else{
    //         if($('.navbar-toggle').hasClass('collapsed')){
    //             navbarDefault.removeClass('gradient')
    //         }
    //     }
    // });

    // $(".collapse").on('show.bs.collapse', function(){
    //     navbarDefault.addClass('gradient')
    // }).on('hide.bs.collapse', function(){
    //     var top = $('body').scrollTop();
    //     if( top > $('.navbar').height() ){
    //         navbarDefault.addClass('gradient')
    //     }else{
    //         navbarDefault.removeClass('gradient')
    //     }
    // });

    //scrollReveal
    window.scrollReveal = new scrollReveal();

    // parallax
    $.stellar.positionProperty.horizontalParallax = {
        setTop: function($el, newTop, originalTop) {
            if(newTop > 0){
                $el.addClass('display-block');
                $el.css({
                    'left': $el.hasClass('horizontal-parallax-left') ? originalTop - newTop : 'initial',
                    'right': $el.hasClass('horizontal-parallax-right') ? originalTop - newTop : 'initial'
                });
            }
        }
    };
    
    $.stellar({
        horizontalScrolling: false,
        positionProperty: 'horizontalParallax'
    });

    // soluções integradas
    $('.solucoes-integradas-detalhes a').click(function(event){
        event.preventDefault();
        var tipo = $(this).prop('className');
        $('.solucoes-integradas-detalhes a').removeClass('active');
        $(this).addClass('active');
        $('.box').removeClass('active');
        $('.box.'+tipo).addClass('active');
    });

});